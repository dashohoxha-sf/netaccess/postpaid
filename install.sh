#!/bin/bash
### install the application

### go to this dir
cd $(dirname $0)

### get the application path
app_path=$(pwd)

### compile the translation files
langs="en sq_AL nl it de tr"
for lng in $langs ; do l10n/msgfmt.sh $lng ; done

### set the superuser password
scripts/su_passwd.sh

### init the database
db/init.sh

