#!/bin/bash

calldetails_dir="d:/Billing/November2009/CallDetails/2008-09-Amdocs-CallDetails"

### go the script directory
cd $(dirname $0)

### get the current directory
dir=$(pwd)

#html_dir=non-payment-responsible-customers
html_dir=A001
pdf_dir=pdf/invoices

let i=100  # debug

ls $html_dir > file_list.txt
while read d
do
  cd $dir
  cd "$html_dir/$d/"
  for fname in $(ls *.html)
  do
	cust_inv_id=${fname%%_*}
	subs_inv_id=${fname##*_}
	subs_inv_id=${subs_inv_id%%.*}
	msisdn=${fname#*_}
	msisdn=${msisdn%%_*}
	#echo "$cust_inv_id $msisdn $subs_inv_id"  # debug

    cdr_file=$(ls $calldetails_dir/${msisdn}_* 2>/dev/null | head -1)
    if [ "$cdr_file" != "" ]
    then
      mv -v $cdr_file \
	     ${cust_inv_id}_${msisdn}_${subs_inv_id}.4.calldetails.pdf
	  #echo ${cust_inv_id}_${msisdn}_${subs_inv_id}.2.calldetails.pdf # debug
    else
      echo $fname
    fi

    # for debuging
    let i=($i+1)
    if [ $i -eq 10 ]; then exit; fi
  done
done < file_list.txt

rm -f file_list.txt

