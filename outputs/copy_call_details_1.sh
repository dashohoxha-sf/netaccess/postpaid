#!/bin/bash
### call like this: 
###   ./copy_call_datails.sh payment-responsible-customers

### check the parameter
if [ "$1" = "" ]
then
  echo "Call it like this:"
  echo "  $0 payment-responsible-customers"
  echo "  where 'payment-responsible-customers' is a subdir of pdf/all-invoices"
  exit
fi

### get the html directory from the first parameter
dir=$1

### go to the script directory
cd $(dirname $0)

invoice_dir='pdf/all-invoices'
calldetails_dir='d:/Billing/November2009/CallDetails/html'

### convert invoices to PDF
ls "$invoice_dir/$dir" > dir_list.txt
while read d
do
  echo "=== $dir/$d ==="
  mv "$calldetails_dir/$dir/$d/"*.pdf "$invoice_dir/$dir/$d/"
done < dir_list.txt

