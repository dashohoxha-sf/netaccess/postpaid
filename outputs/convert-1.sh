#!/bin/bash
### $1=envelopes/B001 or $1=invoices/A001

NitroPDF="c:/Program Files/Nitro PDF/Professional/NitroPDF.exe"

### check the parameter
if [ "$1" = "" ]
then
  echo "Call it like this:"
  echo "  $0 invoices/A001"
  echo "  $0 envelopes/B001"
  exit
fi

### go to the script directory
cd $(dirname $0)

### get the current directory
dir=$(pwd)

html_dir=$1
pdf_dir=pdf/$1

### get the list of files to be converted and split it
rm -rf convert_fnames/
mkdir -p convert_fnames/
ls $html_dir > convert_fnames/file_list.txt
cd convert_fnames/
full_path=$dir/$html_dir/
full_path=${full_path/#\/cygdrive\/c/c:}
sed -i -e "s#^#$full_path#" file_list.txt
split --lines=100 file_list.txt
cd ..

for flist in $(ls convert_fnames/x*)
do
  echo $html_dir : $flist

  cd $dir
  file_list=$(cat $flist)
  last_file=$(tail -n 1 $flist)
  last_pdf_file=${last_file/%.html/.pdf}

  cd $html_dir
  "$NitroPDF" /CV $file_list &
  #nitro_pid=$!
  
  until [ -f $last_pdf_file ]; do sleep 2; done
  sleep 3
  #kill -9 $nitro_pid
  taskkill /IM NitroPDF.exe /F /T

done

### move pdf files to the pdf dir
cd $dir
mkdir -p $pdf_dir
mv $html_dir/*.pdf $pdf_dir/

echo -------------------------------------------
