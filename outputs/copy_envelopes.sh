#!/bin/bash

dir_all_envelopes=z:/0-Invoices/2008-10/pdf/envelopes-old/ALL
dir_html_invoices=z:/0-Invoices/2008-10/html/invoices
dir_pdf_envelopes=k:/Invoices/2008-10/pdf/envelopes

let i=100  # debug
for d in $(ls $dir_html_invoices)
do
  if [ "${d:0:1}" = "A" ]
  then
    mkdir -p $dir_pdf_envelopes/$d
    for fname in $(ls $dir_html_invoices/$d/*.html)
    do
      inv_file=$(basename $fname)
      env_file=${inv_file/%.*/.envelope.C4.pdf}
      if [ -f $dir_all_envelopes/$env_file ]
      then
        #echo cp -v $dir_all_envelopes/$env_file $dir_pdf_envelopes/$d/
        cp -v $dir_all_envelopes/$env_file $dir_pdf_envelopes/$d/
      fi

      # for debuging
      let i=($i+1)
      if [ $i -eq 100 ]; then exit; fi
    done
  fi
done


