#!/bin/bash
### $1=envelopes/B001 or $1=invoices/A001

remote_dir=z:/0-Invoices/2008-10

### check the parameter
if [ "$1" = "" ]
then
  echo "Call it like this:"
  echo "  $0 invoices/A001"
  echo "  $0 envelopes/B001"
  exit
fi

### go to the script directory
cd $(dirname $0)

### get the current directory
dir=$(pwd)

pdf_dir=pdf/$1
dname=$(basename $1)
merged_dir=$(dirname $pdf_dir)/0_merged

remote_dir=$remote_dir/$(dirname $pdf_dir)

### compress and move the merged files
./7z.exe a  $merged_dir/$dname.pdf.7z $merged_dir/$dname*.pdf
mkdir -p $remote_dir/0_merged/
mv -v $merged_dir/$dname.pdf.7z $remote_dir/0_merged/
rm -rf $merged_dir/$dname*.pdf
touch $merged_dir/$dname.pdf

### compress and move the PDF files
./7z.exe a $pdf_dir.7z $pdf_dir/
mv -v $pdf_dir.7z $remote_dir/
rm -rf $pdf_dir