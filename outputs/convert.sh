#!/bin/bash
### 

NitroPDF="c:/Program Files/Nitro PDF/Professional/NitroPDF.exe"

### check the parameter
if [ "$1" = "" ]
then
  echo "Call it like this:"
  echo "  $0 html_dir"
  echo "  where 'html_dir' contains the html files to be converted"
  exit
fi

### get the parameter
html_dir=$1

### go to the script directory
cd $(dirname $0)

### get the current directory
dir=$(pwd)

### get the list of files to be converted and split it
rm -rf convert_fnames/
mkdir -p convert_fnames/
ls "$html_dir" > convert_fnames/file_list.txt
cd convert_fnames/

full_path="$dir/$html_dir/"
full_path=${full_path/#\/cygdrive\/d/d:}
sed -i -e "s#^#\"$full_path#" -e's#$#"#' file_list.txt

split --lines=100 file_list.txt
cd ..

for flist in $(ls convert_fnames/x*)
do
  echo "$html_dir : $flist"

  last_file=$(tail -n 1 $flist)
  last_file=${last_file#\"}
  last_file=${last_file%\"}
  last_pdf_file=${last_file/%.html/.pdf}

  $(cat $flist | xargs "$NitroPDF" /CV ) &

  until [ -f "$last_pdf_file" ]; do sleep 2; done
  sleep 3
  taskkill /IM NitroPDF.exe /F /T
  sleep 1

done
