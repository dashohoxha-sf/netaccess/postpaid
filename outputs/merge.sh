#!/bin/bash
### $1=envelopes/B001 or $1=invoices/A001

### check the parameter
if [ "$1" = "" ]
then
  echo "Call it like this:"
  echo "  $0 invoices/A001"
  echo "  $0 envelopes/B001"
  exit
fi

### go to the script directory
cd $(dirname $0)

### get the current directory
dir=$(pwd)

pdf_dir=pdf/$1
dname=$(basename $1)
merged_dir=$(dirname $pdf_dir)/0_merged

mkdir -p $merged_dir/

### get the list of files to be converted and split it
rm -rf merge_fnames/
mkdir -p merge_fnames/
ls $pdf_dir/ > merge_fnames/file_list.txt
cd merge_fnames/
sed -i -e "s#^#$pdf_dir/#" file_list.txt
split --lines=400 file_list.txt
cd ..

let i=0
for flist in $(ls merge_fnames/x*)
do
  ### get the name of the merged pdf file
  if [ $i -eq 0 ]
  then
    merged_pdf_file=$merged_dir/$dname.pdf
  else
    merged_pdf_file=$merged_dir/${dname}_$i.pdf
  fi
  echo $merged_pdf_file

  ### merge the pdf files
  file_list=$(cat $flist)
  ./pdftk.exe $file_list cat output $merged_pdf_file

  ### increment the chunk counter
  let i=($i+1)
done
