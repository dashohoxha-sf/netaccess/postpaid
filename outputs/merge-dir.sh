#!/bin/bash

### check the parameter
if [ "$1" = "" ]
then
  echo "Call it like this:"
  echo "  $0 dir-name"
  echo "  where 'dir-name' is a subdirectory of 'pdf/' "
  exit
fi

### go to the script directory
cd $(dirname $0)

### get the current directory
dir=$(pwd)
dir=${dir/#\/cygdrive\/d/d:}

pdf_dir=$1
dname=$(basename "$pdf_dir")
merged_dir=pdf-merged/$(dirname "$pdf_dir")

mkdir -p "$merged_dir/"

### get the list of files to be converted and split it
rm -rf merge_fnames/
mkdir -p merge_fnames/
ls "pdf/$pdf_dir/" > merge_fnames/file_list.txt
cd merge_fnames/
split --lines=500 file_list.txt
cd ..

let i=0
for flist in $(ls merge_fnames/x*)
do
  ### go to the original directory
  cd $dir

  ### get the file list
  file_list=$(cat $flist)

  ### get the name of the merged pdf file
  if [ $i -eq 0 ]
  then
    merged_pdf_file="$dir/$merged_dir/${dname}.pdf"
	echo "$merged_dir/${dname}.pdf"
  else
    merged_pdf_file="$dir/$merged_dir/${dname}_$i.pdf"
	echo "$merged_dir/${dname}_$i.pdf"
  fi

  ### merge the pdf files
  cd "pdf/$pdf_dir/"
  $dir/pdftk.exe $file_list cat output "$merged_pdf_file"

  ### increment the chunk counter
  let i=($i+1)
done

