#!/bin/bash
### separate the invoices of albtelecom and burgu from the rest

mkdir only-invoices

cp invoices-1/A*/*.invoice.*  only-invoices-1/

id_list=$(ls only-invoices/ \
          | grep -v 0000000000 | grep -v zzzzzzzzz \
		  | cut -d'_' -f1 | sort -u)

for id in $id_list
do
  cp invoices-1/A*/${id}_*expenses* only-invoices/
done

mv \
only-invoices/{61804,61614,61323,61312,61637,61707,61503,61535,61966,62387
,62964,61493,61335,61350,61697,61524,62245,61618,61476,61448,61339,61456,61719,62951,62777,61437,62516,62030,61330,61305,62116,61319,61329,63152,61979,61593,62176,61533,62140,61304,61559}_* \
  only-invoices-albtelecom/

mv \
only-invoices/{61495,61311,61327,61418,61985,61349,61307,61369,62281,61668
,62262,61299,61314,61298,62983,61393,61358,61446,61348,61366,61717,67276,61940,62371,61557}_* \
  only-invoices-burgu/ 

rm only-invoices/*zzzzz_end.invoice.*

