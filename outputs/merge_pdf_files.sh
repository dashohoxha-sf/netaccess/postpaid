#!/bin/bash
### call like this: 
###   ./merge_pdf_files.sh all-invoices/payment-responsible-customers

### check the parameter
if [ "$1" = "" ]
then
  echo "Call it like this:"
  echo "  $0 all-invoices/payment-responsible-customers"
  echo "  where 'all-invoices/payment-responsible-customers' is a subdir of pdf/"
  exit
fi

### get the html directory from the first parameter
dir=$1

### go to the script directory
cd $(dirname $0)

### create the pdf directory
mkdir -p "pdf-merged/$dir"
mkdir -p "log/$dir"

### merge pdf files of each subdirectory 
ls "pdf/$dir" > dir_list.txt
while read d
do
  if [ -f "log/$dir/$d.txt" ]; then continue; fi

  echo "=== pdf/$dir/$d ==="

  ### merge
  ./merge-dir.sh "$dir/$d"

  ### create a log file
  touch "log/$dir/$d.txt"

done < dir_list.txt
