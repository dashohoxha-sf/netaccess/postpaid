#!/bin/bash
### call like this: 
###   ./create_pdf_files.sh all-invoices/payment-responsible-customers

### check the parameter
if [ "$1" = "" ]
then
  echo "Call it like this:"
  echo "  $0 all-invoices/payment-responsible-customers"
  echo "  where 'all-invoices/payment-responsible-customers' is a subdir of html/"
  exit
fi

### get the html directory from the first parameter
dir=$1

### go to the script directory
cd $(dirname $0)

### create the pdf and log directories
mkdir -p pdf/$dir
mkdir -p log1/$dir

### convert invoices to PDF
ls html/$dir > dir_list.txt
while read d
do
  if [ -f "log/$dir/$d.txt" ]; then continue; fi

  echo "=== html/$dir/$d ==="

  ### convert to PDF
  ./convert.sh "html/$dir/$d"

  ### move pdf files to another directory
  mkdir -p "pdf/$dir/$d"
  mv "html/$dir/$d/"*.pdf "pdf/$dir/$d/"

  ### create a log file
  touch "log1/$dir/$d.txt"

done < dir_list.txt
