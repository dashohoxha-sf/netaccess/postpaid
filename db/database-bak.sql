-- MySQL dump 10.11
--
-- Host: localhost    Database: invoices
-- ------------------------------------------------------
-- Server version	5.0.45

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `domain` varchar(20) default NULL,
  `username` varchar(20) NOT NULL default '',
  `password` varchar(100) default '',
  `firstname` varchar(20) default NULL,
  `lastname` varchar(20) default NULL,
  `email` varchar(50) default NULL,
  `phone1` varchar(20) default NULL,
  `phone2` varchar(20) default NULL,
  `address` varchar(100) default NULL,
  `notes` text,
  `service` varchar(20) default NULL,
  `expiration_time` datetime default NULL,
  `download_limit` int(11) default NULL,
  `upload_limit` int(11) default NULL,
  `online_time` time default NULL,
  `last_modified` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_by` varchar(20) default NULL,
  PRIMARY KEY  (`username`),
  UNIQUE KEY `u` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` (`domain`, `username`, `password`, `firstname`, `lastname`, `email`, `phone1`, `phone2`, `address`, `notes`, `service`, `expiration_time`, `download_limit`, `upload_limit`, `online_time`, `last_modified`, `modified_by`) VALUES ('domain1','test12','test12','Test','12','','','','','','service1','2008-07-31 00:00:00',0,0,'00:00:00','2008-03-01 07:18:38','superuser'),('domain1','test11','test11','Test','11','','','','','','service1','2008-07-31 00:00:00',0,0,'00:00:00','2008-03-01 07:18:12','superuser'),('domain2','test21','test21','Test','21','','','','','','service1','2008-07-31 00:00:00',0,0,'00:00:00','2008-03-01 07:20:15','superuser'),('domain1','test13','test13','Test','13','','','','','','service1','2008-07-31 00:00:00',0,0,'00:00:00','2008-03-01 07:19:30','superuser'),('domain2','test22','test22','Test','22','','','','','','service1','2008-07-31 00:00:00',0,0,'00:00:00','2008-03-01 07:20:39','superuser'),('domain2','test23','test23','Test','23','','','','','','service2','2008-07-31 00:00:00',0,0,'00:00:00','2008-03-01 07:21:05','superuser');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domains`
--

DROP TABLE IF EXISTS `domains`;
CREATE TABLE `domains` (
  `id` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(50) default NULL,
  `services` varchar(250) default NULL,
  `max_nas_nr` int(11) default NULL,
  `max_client_nr` int(11) default NULL,
  `login_page` varchar(100) default NULL,
  `allowed_sites` text,
  `radius_secret` varchar(64) default NULL,
  `notes` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `domains`
--

LOCK TABLES `domains` WRITE;
/*!40000 ALTER TABLE `domains` DISABLE KEYS */;
INSERT INTO `domains` (`id`, `name`, `description`, `services`, `max_nas_nr`, `max_client_nr`, `login_page`, `allowed_sites`, `notes`) VALUES ('domain2','Domain 2','','service1',3,150,'http://www.albaniaonline.net/hs','http://www.google.com','This is a simple domain, with only one service available for its clients, with a default login page (not a customized one) and with a low limit of hotspots and clients.'),('domain1','Domain 1','','service1,service2',5,500,'http://www.albaniaonline.net/hs/domain1/','http://www.google.com\r\nhttp://www.albaniaonline.net/','This domain has more than one service available, and also has a customized login page.');
/*!40000 ALTER TABLE `domains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` varchar(10) NOT NULL default '',
  `name` varchar(100) NOT NULL default '',
  `charset` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` (`id`, `name`, `charset`) VALUES ('en','English','iso-8859-1'),('sq_AL','Albanian','iso-8859-1'),('it','Italian','iso-8859-1'),('de','German','iso-8859-1'),('nl','Dutch','iso-8859-1');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user` varchar(20) NOT NULL default '',
  `domain` varchar(20) NOT NULL default '',
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `event` varchar(100) NOT NULL default '',
  `details` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=560 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `naslist`
--

DROP TABLE IF EXISTS `naslist`;
CREATE TABLE `naslist` (
  `id` varchar(20) NOT NULL default '',
  `name` varchar(20) default NULL,
  `type` varchar(20) default NULL,
  `domain` varchar(20) default NULL,
  `description` varchar(100) default NULL,
  `mac` varchar(20) default NULL,
  `ip` varchar(20) default NULL,
  `gateway` varchar(20) default NULL,
  `dns1` varchar(20) default NULL,
  `dns2` varchar(20) default NULL,
  `radius_secret` varchar(20) default NULL,
  `phone` varchar(20) default NULL,
  `address` varchar(250) default NULL,
  `latitude` varchar(20) default NULL,
  `longitude` varchar(20) default NULL,
  `notes` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `naslist`
--

LOCK TABLES `naslist` WRITE;
/*!40000 ALTER TABLE `naslist` DISABLE KEYS */;
INSERT INTO `naslist` (`id`, `name`, `type`, `domain`, `description`, `mac`, `ip`, `gateway`, `dns1`, `dns2`, `radius_secret`, `phone`, `address`, `latitude`, `longitude`, `notes`) VALUES ('aolsphs3','HotSpot #003','CoovaAP','domain1','WRT54GL WiFi Router','00:1C:10:BF:64:C9','','','','','aolsphotspot','','','','',''),('aolsphs2','HotSpot #002','CoovaAP','domain1','WRT54GL WiFi Router','00:1C:10:BF:64:C8','','','','','aolsphotspot','','','','',''),('aolsphs1','HotSpot #001','CoovaAP','domain1','WRT54GL WiFi Router','00:1C:10:BF:64:C7','','','','','aolsphotspot','','','','',''),('aolsphs4','HotSpot #004','CoovaAP','domain2','WRT54GL WiFi Router','00:1C:10:BF:64:CA','','','','','aolsphotspot','','','','',''),('aolsphs5','HotSpot #005','CoovaAP','domain2','WRT54GL WiFi Router','00:1C:10:BF:64:CB','','','','','aolsphotspot','','','','',''),('aolsphs6','HotSpot #006','CoovaAP','domain2','WRT54GL WiFi Router','00:1C:10:BF:64:CC','','','','','aolsphotspot','','','','','');
/*!40000 ALTER TABLE `naslist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` varchar(20) NOT NULL default '',
  `name` varchar(20) default NULL,
  `description` varchar(100) default NULL,
  `download_rate` int(11) default NULL,
  `upload_rate` int(11) default NULL,
  `online_time` time default NULL,
  `download_limit` int(11) default NULL,
  `upload_limit` int(11) default NULL,
  `notes` varchar(250) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`id`, `name`, `description`, `download_rate`, `upload_rate`, `online_time`, `download_limit`, `upload_limit`, `notes`) VALUES ('service1','Service 1','',512,256,'00:00:00',0,0,''),('service2','Service 2','',1024,512,'00:00:00',0,0,'');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` varchar(255) NOT NULL default '',
  `vars` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` (`id`, `vars`) VALUES ('IP: 192.168.252.45; DATE: 2008-03-01 09:44:50','a:8:{s:8:\"username\";s:5:\"user7\";s:8:\"password\";s:13:\"60B0.8YdWC2nc\";s:4:\"u_id\";s:1:\"9\";s:7:\"modules\";s:62:\"domains,naslist,clients,misc,user_settings,logs,users,services\";s:7:\"domains\";s:3:\"all\";s:12:\"many_domains\";s:4:\"true\";s:13:\"domain_filter\";s:25:\"\"alldomains\"=\"alldomains\"\";s:20:\"user-can-add-domains\";s:4:\"true\";}');
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(20) default NULL,
  `password` varchar(20) default NULL,
  `firstname` varchar(20) default NULL,
  `lastname` varchar(20) default NULL,
  `phone1` varchar(20) default NULL,
  `phone2` varchar(20) default NULL,
  `email` varchar(100) default NULL,
  `address` varchar(100) default NULL,
  `notes` text,
  `modules` varchar(250) default NULL,
  `domains` varchar(250) default NULL,
  PRIMARY KEY  USING BTREE (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `password`, `firstname`, `lastname`, `phone1`, `phone2`, `email`, `address`, `notes`, `modules`, `domains`) VALUES (4,'user2','10DzFyhXAOKFA','User','2','','','','','This is a normal domain admin, which has access only to one domain and can access the modules NASes, Clients and Logs.','naslist,clients,logs','domain1'),(3,'user1','19l0Db1UnJb3Y','User','1','','','','','This user does not have any access rights on the modules or domains.','',''),(5,'user3','105vWTSPMAui2','User','3','','','','','Tis user has more than one domain, but has access only to Domains, NASes, Clients and Logs.','domains,naslist,clients,logs','domain1\r\ndomain2'),(6,'user4','20INyD6SyR4Bk','User','4','','','','','','domains,naslist,clients,misc,user_settings,logs,users,services','domain1'),(7,'user5','33YW3xXO.LIt.','User','5','','','','','Has access to more than 1 domains and to all the modules.','domains,naslist,clients,misc,user_settings,logs,users,services','domain1\r\ndomain2'),(8,'user6','182eY460KbUco','User','6','','','','','Has access to all the domains and not to all the modules.','naslist,clients,misc,logs,services','all'),(9,'user7','60B0.8YdWC2nc','User','7','','','','','Has access to all the domains and all the modules. Almost superuser, only that the superuser cannot be deleted and cannot be restricted access.','domains,naslist,clients,misc,user_settings,logs,users,services','all');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2008-03-01  8:53:45
