-- MySQL dump 10.10
--
-- Host: localhost    Database: postpaid
-- ------------------------------------------------------
-- Server version	5.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `postpaid`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `postpaid` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `postpaid`;

--
-- Table structure for table `em_customer_fees`
--

DROP TABLE IF EXISTS `em_customer_fees`;
CREATE TABLE `em_customer_fees` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `customer_id` int(10) unsigned default NULL,
  `cos_id` int(10) default NULL,
  `product_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `em_customers`
--

DROP TABLE IF EXISTS `em_customers`;
CREATE TABLE `em_customers` (
  `customer_id` int(10) unsigned NOT NULL auto_increment,
  `customer` varchar(200) character set latin1 default NULL,
  `NIPT` varchar(200) character set latin1 default NULL,
  `Address` varchar(200) character set latin1 default NULL,
  `PaymentResponsible` varchar(10) character set latin1 default NULL,
  `contact_person` varchar(20) default NULL,
  `contact_phone` varchar(20) default NULL,
  `contact_email` varchar(50) default NULL,
  PRIMARY KEY  (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `em_subscribers`
--

DROP TABLE IF EXISTS `em_subscribers`;
CREATE TABLE `em_subscribers` (
  `subs_id` int(10) unsigned NOT NULL auto_increment,
  `customer_id` int(10) default NULL,
  `MSISDN` varchar(200) default NULL,
  `SubsId` int(10) NOT NULL,
  `FirstName` varchar(200) default NULL,
  `LastName` varchar(200) default NULL,
  `Address1` varchar(200) default NULL,
  `Address2` varchar(200) default NULL,
  `City` varchar(200) default NULL,
  `COSID` varchar(200) default NULL,
  `PassportNr` varchar(200) default NULL,
  `AccountID` int(10) default NULL,
  `PaymentResponsible` varchar(10) default NULL,
  `postpaid` tinyint(3) unsigned zerofill default NULL,
  `RegistrationDate` datetime default NULL COMMENT 'Date when the subscriber is registered in the system.',
  PRIMARY KEY  (`subs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `em_subscust_matrix`
--

DROP TABLE IF EXISTS `em_subscust_matrix`;
CREATE TABLE `em_subscust_matrix` (
  `MSISDN` varchar(50) default NULL,
  `FirstName` varchar(50) default NULL,
  `LastName` varchar(50) default NULL,
  `customer_id` int(10) unsigned default NULL,
  `customer` varchar(250) default NULL,
  `CosId` int(10) default NULL,
  `CosName` varchar(50) default NULL,
  `postpaid` smallint(5) default NULL,
  `PassportNr` varchar(20) default NULL,
  `Address1` varchar(50) default NULL,
  `Address2` varchar(50) default NULL,
  `City` varchar(50) default NULL,
  `CreationDate` datetime default NULL,
  `ActivationDate` datetime default NULL,
  `NIPT` varchar(100) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `inv_customers`
--

DROP TABLE IF EXISTS `inv_customers`;
CREATE TABLE `inv_customers` (
  `customer_id` int(10) NOT NULL default '0',
  `month` varchar(20) NOT NULL default '',
  `InvoiceID` int(10) default NULL,
  `CreationDate` datetime default NULL,
  `StartDate` datetime default NULL,
  `EndDate` datetime default NULL,
  `GrandTotal` double default NULL,
  `GroupName` varchar(200) default NULL,
  `NIPT` varchar(200) default NULL,
  `Address` varchar(200) default NULL,
  `SerialNumber` varchar(200) default NULL,
  `PaymentResponsible` varchar(10) default NULL,
  PRIMARY KEY  (`customer_id`,`month`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `inv_details`
--

DROP TABLE IF EXISTS `inv_details`;
CREATE TABLE `inv_details` (
  `type` varchar(20) default NULL,
  `id` int(10) default NULL,
  `month` varchar(20) default NULL,
  `InvoiceID` int(10) default NULL,
  `DetailCategory` varchar(20) default NULL,
  `DetailType` varchar(20) default NULL,
  `DetailName` varchar(200) default NULL,
  `DetailOrder` int(10) default NULL,
  `UnitCount` int(10) default NULL,
  `Usage` int(10) default NULL,
  `NetValue` double default NULL,
  `VAT` double default NULL,
  `TotalValue` double default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `inv_serialnumbers`
--

DROP TABLE IF EXISTS `inv_serialnumbers`;
CREATE TABLE `inv_serialnumbers` (
  `id` int(10) NOT NULL auto_increment,
  `InvoiceID` int(10) default NULL,
  `SerialNumber` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `inv_subscribers`
--

DROP TABLE IF EXISTS `inv_subscribers`;
CREATE TABLE `inv_subscribers` (
  `subs_id` int(10) NOT NULL,
  `month` varchar(20) NOT NULL default '',
  `customer_id` int(10) default NULL,
  `InvoiceID` int(10) default NULL,
  `SubsId` int(10) NOT NULL,
  `GrandTotal` double default NULL,
  `CreationDate` datetime NOT NULL,
  `StartDate` datetime default NULL,
  `EndDate` datetime default NULL,
  `MSISDN` varchar(200) default NULL,
  `FirstName` varchar(200) default NULL,
  `LastName` varchar(200) default NULL,
  `Address1` varchar(200) default NULL,
  `Address2` varchar(200) default NULL,
  `City` varchar(200) default NULL,
  `COSID` varchar(200) default NULL,
  `PassportNr` varchar(200) default NULL,
  `GroupName` varchar(200) default NULL,
  `SerialNumber` varchar(200) default NULL,
  `AccountID` int(10) default NULL,
  `PaymentResponsible` varchar(10) default NULL,
  PRIMARY KEY  (`subs_id`,`month`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `s_cos`
--

DROP TABLE IF EXISTS `s_cos`;
CREATE TABLE `s_cos` (
  `cos_id` int(10) NOT NULL,
  `cos_name` varchar(50) NOT NULL,
  `postpaid` smallint(5) default NULL,
  PRIMARY KEY  (`cos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `s_features`
--

DROP TABLE IF EXISTS `s_features`;
CREATE TABLE `s_features` (
  `9400_FeatureId` int(10) NOT NULL,
  `9425_DefaultName` varchar(50) NOT NULL,
  `9402_FeatureValue` varchar(500) default NULL,
  PRIMARY KEY  (`9400_FeatureId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `s_productprices`
--

DROP TABLE IF EXISTS `s_productprices`;
CREATE TABLE `s_productprices` (
  `ProductId` int(10) NOT NULL,
  `ProductName` varchar(50) NOT NULL,
  `Price` int(10) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

