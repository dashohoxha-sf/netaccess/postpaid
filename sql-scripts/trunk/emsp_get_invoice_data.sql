﻿
use postpaid_scratch;

-------------------------------------------------------------------------------------
-- Get the data of the invoices of the last month from the database Invoice at 10.234.252.63
-- into the local Invoice database. The data that are updated are those in the selected
-- intervals (@p_fromInvDate,@p_toInvDate) and (@p_fromDate,@p_toDate).
-- The parameters: @p_fromInvDate and @p_toInvDate are the dates when the invoice
-- records are processed (INV_SumTrans, INV_InvoiceNumbers), and the parameters 
-- @p_fromDate and @p_toDate are the dates of the records (INV_CDR, INV_TDR).
-- The invoice data of the last month are saved in the tables: INV_SumTrans_1,
-- INV_InvoiceNumbers_1, INV_TDR_1, INV_AREA_SubsInvDialed_1, INV_CDR_1, etc.
--
-- Example: 
--   use postpaid_scratch;
--   exec emsp_get_invoice_data '2008-11-03', '2008-12-02', '2008-11-01', '2008-12-01';
-------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_get_invoice_data' AND type = 'P')
DROP PROCEDURE emsp_get_invoice_data;
GO

CREATE PROCEDURE emsp_get_invoice_data
	@p_fromInvDate datetime,
	@p_toInvDate datetime,
	@p_fromDate datetime,
	@p_toDate datetime
AS
BEGIN
SET NOCOUNT ON;

--------------------- INV_SumTrans_1 ------------------------

if exists(select table_name from information_schema.tables where table_name = 'INV_SumTrans_1') 
drop table INV_SumTrans_1;

select * into INV_SumTrans_1
from [10.234.252.63].Invoice.dbo.INV_SumTrans T 
where (T.SumDate between @p_fromInvDate and @p_toInvDate);

--------------------- INV_InvoiceNumbers_1 ------------------------

if exists(select table_name from information_schema.tables where table_name = 'INV_InvoiceNumbers_1') 
drop table INV_InvoiceNumbers_1;

select * into INV_InvoiceNumbers_1
from [10.234.252.63].Invoice.dbo.INV_InvoiceNumbers N
where N.INVDate between @p_fromInvDate and @p_toInvDate;

--------------------- INV_AREA_SubsInvDialed_1 ------------------------

if exists(select table_name from information_schema.tables where table_name = 'INV_AREA_SubsInvDialed_1') 
drop table INV_AREA_SubsInvDialed_1;

select * into INV_AREA_SubsInvDialed_1
from [10.234.252.63].Invoice.dbo.INV_AREA_SubsInvDialed D
where D.INVDate between @p_fromInvDate and @p_toInvDate;

--------------------- INV_TDR_1 ------------------------

if exists(select table_name from information_schema.tables where table_name = 'INV_TDR_1') 
drop table INV_TDR_1;

select * into INV_TDR_1
from [10.234.252.63].Invoice.dbo.INV_TDR T
where T.[10005_Time] between @p_fromDate and @p_toDate;

--------------------- INV_CDR_1 ------------------------

if exists(select table_name from information_schema.tables where table_name = 'INV_CDR_1') 
drop table INV_CDR_1;

select * into INV_CDR_1
from [10.234.252.63].Invoice.dbo.INV_CDR CDR
where CDR.AnswerTime between @p_fromDate and @p_toDate;

--------------------- INV_Features ------------------------

if exists(select table_name from information_schema.tables where table_name = 'INV_Features') 
drop table INV_Features;

select * into INV_Features from [10.234.252.63].Invoice.dbo.INV_Features ;

--------------------- INV_SubsDetails ------------------------

if exists(select table_name from information_schema.tables where table_name = 'INV_SubsDetails_bak') 
drop table INV_SubsDetails_bak;

exec dbo.sp_rename @objname = N'[dbo].[INV_SubsDetails]', @newname = N'INV_SubsDetails_bak', @objtype = N'OBJECT';

select * into INV_SubsDetails from [10.234.252.63].Invoice.dbo.INV_SubsDetails;

drop table INV_SubsDetails_bak;

END
GO
