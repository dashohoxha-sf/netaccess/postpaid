﻿
-------------------------------------------------------------------------
-- Correct the format of some fields of the customer and subscriber data.
-------------------------------------------------------------------------


IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_correct_fields' AND type = 'P')
DROP PROCEDURE emsp_correct_fields;
GO

CREATE PROCEDURE emsp_correct_fields
AS
BEGIN
SET NOCOUNT ON;

-- test
--select dbo.RegExReplace('<span>this shall remain.</span><br/>', '<[^>]*>', '', DEFAULT);

----------- backup the tables em_customers and em_subscribers -------------------

-- drop table em_customers_bak
if exists(select table_name from information_schema.tables where table_name = 'em_customers_bak') 
drop table em_customers_bak;

-- make a backup of the table em_customers
select * into em_customers_bak from em_customers;

-- drop table em_subscribers
if exists(select table_name from information_schema.tables where table_name = 'em_subscribers_bak') 
drop table em_subscribers_bak;

-- make a backup of the table em_subscribers
select * into em_subscribers_bak from em_subscribers;


------------ fix the fields of the table em_customers ------------------------

update em_customers set NIPT = ltrim(rtrim(NIPT)) where NIPT is not null;
update em_customers set NIPT = upper(NIPT) where NIPT is not null;

update em_customers set customer = ltrim(rtrim(customer)) where customer_id > 1;

update em_customers set contact_person = ltrim(rtrim(contact_person))
  where contact_person is not null;
update em_customers set contact_phone = ltrim(rtrim(contact_phone))
  where contact_phone is not null;
update em_customers set contact_email = ltrim(rtrim(contact_email))
  where contact_email is not null;

update em_customers set Address = ltrim(rtrim(Address)) where Address is not null;

update em_customers 
set Address = dbo.RegExReplace(Address, ' +', ' ', DEFAULT) 
where Address is not null;

update em_customers 
set Address = dbo.RegExReplace(Address, '^[\s\r\n]*', '', DEFAULT) 
where Address is not null;

update em_customers 
set Address = dbo.RegExReplace(Address, '[\s\r\n]*$', '', DEFAULT) 
where Address is not null;

update em_customers 
set Address = dbo.RegExReplace(Address, '\s*, *', ', ', DEFAULT) 
where Address is not null;

update em_customers 
set Address = dbo.RegExReplace(Address, '\s*; *', ', ', DEFAULT) 
where Address is not null;

update em_customers 
set Address = dbo.RegExReplace(Address, '\s*Tirane', char(13)+'Tirane', '1') 
where Address is not null;

select * from em_subscribers where FirstName like '%test%' or LastName like '%test%';

select Address from em_customers;

/*
create table tmp_tbl (city varchar(100));
insert into tmp_tbl values ('Berat');
insert into tmp_tbl values ('Berzhite');
insert into tmp_tbl values ('Bilisht');
insert into tmp_tbl values ('Bulqize');
insert into tmp_tbl values ('Burrel');
insert into tmp_tbl values ('Delvine');
insert into tmp_tbl values ('Diber');
insert into tmp_tbl values ('Durres');
insert into tmp_tbl values ('Elbasan');
insert into tmp_tbl values ('Erinel');
insert into tmp_tbl values ('Erseke');
insert into tmp_tbl values ('Fier');
insert into tmp_tbl values ('Fushe-Kruje');
insert into tmp_tbl values ('Gjirokaster');
insert into tmp_tbl values ('Gramsh');
insert into tmp_tbl values ('Has');
insert into tmp_tbl values ('Kamez');
insert into tmp_tbl values ('Kamez-Tirane');
insert into tmp_tbl values ('Kashar');
insert into tmp_tbl values ('Kavaje');
insert into tmp_tbl values ('Koplik');
insert into tmp_tbl values ('Korce');
insert into tmp_tbl values ('Kruje');
insert into tmp_tbl values ('Kucove');
insert into tmp_tbl values ('Kukes');
insert into tmp_tbl values ('Lac');
insert into tmp_tbl values ('Lezhe');
insert into tmp_tbl values ('Librazhd');
insert into tmp_tbl values ('Lushnje');
insert into tmp_tbl values ('Mat');
insert into tmp_tbl values ('Mirdite');
insert into tmp_tbl values ('Peqin');
insert into tmp_tbl values ('Perrenjas');
insert into tmp_tbl values ('Peshkopi');
insert into tmp_tbl values ('Pogradec');
insert into tmp_tbl values ('Rrogozhine');
insert into tmp_tbl values ('Sarande');
insert into tmp_tbl values ('Shijaku');
insert into tmp_tbl values ('Shkoder');
insert into tmp_tbl values ('Tapize');
insert into tmp_tbl values ('Tepelene');
insert into tmp_tbl values ('Tirana');
insert into tmp_tbl values ('Tirane');
insert into tmp_tbl values ('Tirane(Laprake)');
insert into tmp_tbl values ('Tropoje');
insert into tmp_tbl values ('Vaqarr');
insert into tmp_tbl values ('Vlore');
insert into tmp_tbl values ('Vore');

declare @city varchar(100);
declare city_list cursor read_only for select * from tmp_tbl;
open city_list
fetch next from city_list into @city
while @@fetch_status = 0
  begin
  update em_customers 
  set Address = dbo.RegExReplace(Address, '\s*'+@city, char(13)+@city, '1') 
  where Address is not null;
  fetch next from city_list into @city
  end
close city_list
deallocate city_list
*/


------------ fix the fields of the table em_subscribers ------------------------

update em_subscribers set PassportNr = ltrim(rtrim(PassportNr)) where PassportNr is not null;
update em_subscribers set PassportNr = upper(PassportNr) where PassportNr is not null;

update em_subscribers set City = ltrim(rtrim(City)) where City is not null;
update em_subscribers set City = upper(substring(City,1,1)) + lower(substring(City,2,len(City)));

update em_subscribers 
set Address1 = dbo.RegExReplace(Address1, ' +', ' ', DEFAULT) 
where Address1 is not null;

update em_subscribers 
set Address1 = dbo.RegExReplace(Address1, '^[\s\r\n]*', '', DEFAULT) 
where Address1 is not null;

update em_subscribers 
set Address1 = dbo.RegExReplace(Address1, '[\s\r\n]*$', '', DEFAULT) 
where Address1 is not null;

update em_subscribers 
set Address1 = dbo.RegExReplace(Address1, '\s*, *', ', ', DEFAULT) 
where Address1 is not null;

update em_subscribers 
set Address1 = dbo.RegExReplace(Address1, '\s*; *', ', ', DEFAULT) 
where Address1 is not null;

select Address1 from em_subscribers;

/*
-- get a list of cities
select distinct City from em_subscribers order by City;

select distinct dbo.RegExReplace(substring(Address, len(Address)-15, 16), '.*[\n\r, \.]+', '', '1')
from em_customers;
*/

END
GO
