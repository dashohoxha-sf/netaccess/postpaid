
use postpaid_scratch;

-------------------------------------------------------------------------------------
-- Add discounts on the invoices.
-------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_add_subs_discounts' AND type = 'P')
DROP PROCEDURE emsp_add_subs_discounts;
GO

CREATE PROCEDURE emsp_add_subs_discounts
  @p_month varchar(10)
AS
BEGIN
SET NOCOUNT ON;

-- drop the table inv_details_1_discounts, if it exists
if exists(select table_name from information_schema.tables where table_name = 'inv_details_1_discounts') 
drop table inv_details_1_discounts;

-- create the table inv_details_1_discounts with the same fields as inv_details_1
select top(1) * into inv_details_1_discounts from inv_details_1;
truncate table inv_details_1_discounts;

-- insert the discounts into inv_details_1_discounts
insert into inv_details_1_discounts
select 'subscriber' as InvoiceType, I.InvoiceID, 'feature' as DetailCategory, 
       'discount' as DetailType, D.discount_name as DetailName,
       30 as DetailOrder, 1 as UnitCount, 1 as Usage,
       (-1)*D.discount_value as NetValue, 0.0 as VAT, (-1)*D.discount_value as TotalValue
from inv_subscribers_1 I 
     inner join postpaid..em_discount_subs D on (I.subs_id=D.subs_id)
where D.month=@p_month;

-----------------------------------------------------------------------
-- check that the discount does not make the InvoiceTotal negative

alter table inv_details_1_discounts add InvoiceTotal float;

-- get the InvoiceTotal
update inv_details_1_discounts 
set InvoiceTotal = (select sum(TotalValue) from inv_details_1 D 
                    where D.InvoiceType='subscriber'
										  and D.InvoiceID = inv_details_1_discounts.InvoiceID);
update inv_details_1_discounts set InvoiceTotal=0 where InvoiceTotal is null;

-- adjust the discount where it is neccessary
update inv_details_1_discounts 
set TotalValue = (-1)*InvoiceTotal, NetValue = (-1)*InvoiceTotal
where InvoiceTotal+TotalValue < 0

-- remove any positive or zero discounts
delete from inv_details_1_discounts where TotalValue >= 0;

-- drop the auxiliary column InvoiceTotal
alter table inv_details_1_discounts drop column InvoiceTotal;

-----------------------------------------------------------------------
-- insert discounts into invoice details

insert into inv_details_1 select * from inv_details_1_discounts;

END
GO

