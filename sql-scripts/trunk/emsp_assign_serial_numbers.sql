
-----------------------------------------------------------------
-- Assign a serial number to each payable invoice of the month.
-----------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_assign_serialnumbers' AND type = 'P')
DROP PROCEDURE emsp_assign_serialnumbers;
GO

CREATE PROCEDURE emsp_assign_serialnumbers
  @p_month varchar(20),
	@p_FirstSerialNumber integer
AS
BEGIN
SET NOCOUNT ON;

-- drop the table, if it exists
if exists(select table_name from information_schema.tables where table_name = 'inv_serialnumbers') 
drop table inv_serialnumbers;

create table inv_serialnumbers
(
	id int identity(1,1),
  InvoiceType varchar(20),
	InvoiceID int,
	SerialNumber nvarchar(50)
);

-- add in the table all customer invoices that are payable
insert into inv_serialnumbers (InvoiceType, InvoiceID)
select 'customer', InvoiceID from inv_customers 
where [month] = @p_month and payable = 'true';

-- add in the table all subscriber invoices that are payable
insert into inv_serialnumbers (InvoiceType, InvoiceID)
select 'subscriber', InvoiceID from inv_subscribers 
where [month] = @p_month and payable = 'true';

-- assign an invoice number corresponding to each InvoiceID
update inv_serialnumbers 
set SerialNumber = cast((@p_FirstSerialNumber + id) as nvarchar(50));

-- set the serial number to the corresponding customer invoice
update inv_customers
set SerialNumber = (select SerialNumber from inv_serialnumbers S 
                    where InvoiceType='customer' and S.InvoiceID=inv_customers.InvoiceID)
where [month] = @p_month;

-- set the serial number to the corresponding subscriber invoice
update inv_subscribers 
set SerialNumber = (select SerialNumber from inv_serialnumbers S
                    where InvoiceType='subscriber' and S.InvoiceID=inv_subscribers.InvoiceID)
where [month] = @p_month;

END
GO
