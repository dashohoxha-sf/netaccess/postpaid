﻿
use postpaid_scratch;

-------------------------------------------------------------------------------------------
-- Get the latest subscribers from the tables Magnolia.dbo.SubsPPS and CRM.dbo.Subscribers 
-- and save them into the tables: subscribers_pps, subscribers_crm and subscribers.
-- The latest subscribers are those that are registered recently (according to the 
-- registration date), and are not updated yet into the EagleCC application.
-------------------------------------------------------------------------------------------

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_get_latest_subscribers' AND type = 'P')
DROP PROCEDURE emsp_get_latest_subscribers;
GO

CREATE PROCEDURE emsp_get_latest_subscribers
AS
BEGIN
SET NOCOUNT ON;

-- clean the tables if they exist
exec emsp_update_subscribers_clean;

------ get the latest subscribers into the tables subscribers_pps and subscribers_crm -------

declare @last_SubsId int, @latest_SubsId int, @latestTime datetime;

-- get the last imported SubsId from the table postpaid..em_subscribers
select @last_SubsId = max(SubsId) from postpaid..em_subscribers;

-- get data from [10.234.252.53].Magnolia.dbo.SubsPPS into subscribers_pps
select [9060_SubsId] as SubsId, [9061_SubsNumber] as MSISDN, [9050_SubsIMSI] as IMSI,
   [9180_COSId] as COSID, [1011_StatusId] as StatusId, [9260_AcountId] as AccountId,
   [9062_CreationDate] as CreationDate, [9063_ActivationDate] as ActivationDate
into subscribers_pps
from [10.234.252.53].Magnolia.dbo.SubsPPS
where [9060_SubsId] > @last_SubsId;

-- get the latest time from the table subscribers_pps
select @latestTime = max(CreationDate) from subscribers_pps;

-- move 5 minutes earlier
select @latestTime = dateadd(mi, -5, @latestTime);

-- do not get the subscribers that are registered during the last 5 minutes
delete from subscribers_pps where CreationDate > @latestTime;

-- get the latest SubsId in the table subscribers_pps
select @latest_SubsId = max(SubsId) from subscribers_pps;

-- get data from [10.234.252.61].CRM.dbo.Subscribers into subscribers_crm
select SubsId as CC_SubsId, PrepaidSubsID as SubsId, 
   SubscriberNumber as MSISDN, FirstName, LastName, LastName as CC_LastName, Address, City, State, 
   BirthDate, Gender, Comments, PassportNumber as PassportNr, 
   cast('false' as varchar(10)) as PaymentResponsible, CreationDate
into subscribers_crm
from [10.234.252.61].CRM.dbo.Subscribers
where PrepaidSubsID > @last_SubsId and PrepaidSubsID <= @latest_SubsId;


-------- check the consistency of the tables subscribers_pps and subscribers_crm --------

-- get the subscribers that are in subscribers_pps but are not in subscribers_crm
select * into subscribers_pps_x
from subscribers_pps where SubsId not in (select SubsId from subscribers_crm);

-- remove from subscribers_pps those that are in subscribers_pps_x
delete from subscribers_pps where SubsId in (select SubsId from subscribers_pps_x);

-- get the subscribers that are in subscribers_crm but are not in subscribers_pps
select * into subscribers_crm_x
from subscribers_crm where SubsId not in (select SubsId from subscribers_pps);

-- remove from subscribers_crm those that are in subscribers_crm_x
delete from subscribers_crm where SubsId in (select SubsId from subscribers_crm_x);


---------------------- join the data from PPS and CRM -----------------------------

-- join the data from both tables into subscribers
select P.SubsId, CC_SubsId, P.MSISDN, IMSI, COSID, StatusId, AccountId, 
       P.CreationDate, ActivationDate, FirstName, LastName, CC_LastName, Address, City, State,
       BirthDate, Gender, Comments, PassportNr, cos_name, postpaid, PaymentResponsible
into subscribers
from subscribers_pps P 
     left join subscribers_crm C on (P.SubsId = C.SubsId)
     left join postpaid..s_cos on (P.COSID = s_cos.cos_id);


------- check the consistency of the tables subscribers and postapaid..em_subscribers -------

-- select into subscribers_em_x the subscribers that already exist in the postpaid application
select * into subscribers_em_x from subscribers
where SubsId in (select SubsId from postpaid..em_subscribers);

-- remove from subscribers the ones that are in subscribers_em_x
delete from subscribers where SubsId in (select SubsId from subscribers_em_x);


-- get the latest modifications of subscriber properties (COS, IMSI, MSISDN)
exec emsp_get_latest_modifications;

END
GO


-------------------------------------------------------------------------------------------
-- Get also the latest modifications of subscriber properties (COS, IMSI, MSISDN).
-------------------------------------------------------------------------------------------

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_get_latest_modifications' AND type = 'P')
DROP PROCEDURE emsp_get_latest_modifications;
GO

CREATE PROCEDURE emsp_get_latest_modifications
AS
BEGIN
SET NOCOUNT ON;

declare @from_time datetime, @to_time datetime;

-- get the time interval of the latest modifications
select @from_time = max(RegistrationDate) from postpaid..em_subscribers;
select @to_time = max(CreationDate) from subscribers;

-- drop the table SDR
if exists(select table_name from information_schema.tables where table_name = 'SDR') 
drop table SDR;

-- get the latest modifications into SDR
select PropertyKey, [9060_SubsId] as SubsId, OldProperty, NewProperty, Time, Info, Comment
into SDR
from [10.234.252.65].CDR.dbo.SDR 
where PropertyKey in (3,15,25) and Time between @from_time and @to_time
order by Time;

END
GO



-------------------------------------------------------------------------------------
-- Update the latest subscribers (which are in the table subscribers) 
-- into the table postpaid..em_subscribers.
-------------------------------------------------------------------------------------

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_update_subscribers' AND type = 'P')
DROP PROCEDURE emsp_update_subscribers;
GO

CREATE PROCEDURE emsp_update_subscribers
AS
BEGIN
SET NOCOUNT ON;

-- update the field PaymentResponsible
update subscribers set PaymentResponsible = 'true' where postpaid='1';
update subscribers set PaymentResponsible = 'false' where postpaid='0';

-- correct the fields FirstName and LastName of subscribers
exec emsp_fix_names 'subscribers';

-- append to postpaid..em_subscribers the ones that are in subscribers
insert into postpaid..em_subscribers
  (customer_id, SubsId, MSISDN, IMSI, FirstName, LastName, CC_LastName, PassportNr, AccountID,
   Address1, Address2, City, COSID, postpaid, PaymentResponsible, RegistrationDate, StatusId)
select '0' as customer_id, SubsId, MSISDN, IMSI, FirstName, LastName, CC_LastName, PassportNr,
       AccountID, Address as Address1, '' as Address2, City, COSID, postpaid, 
       PaymentResponsible, CreationDate as RegistrationDate, StatusId
from subscribers;

-- drop the table subscribers
drop table subscribers;


declare @SubsId int, @oldProperty varchar(50), @newProperty varchar(50);

-- apply to postpaid..em_subscribers the COS changes in SDR
declare cos_changes cursor READ_ONLY for
  select SubsId, OldProperty, NewProperty from SDR 
  where PropertyKey=3 order by Time;
  
open cos_changes
fetch next from cos_changes into @SubsId, @oldProperty, @newProperty;
while @@fetch_status = 0
  begin
  update postpaid..em_subscribers set COSID=@newProperty 
  where SubsId=@SubsId and COSID=@oldProperty;
  fetch next from cos_changes into @SubsId, @oldProperty, @newProperty;
  end
close cos_changes
deallocate cos_changes

-- apply to postpaid..em_subscribers the IMSI changes in SDR
declare imsi_changes cursor READ_ONLY for
  select SubsId, OldProperty, NewProperty from SDR 
  where PropertyKey=15 order by Time;
  
open imsi_changes
fetch next from imsi_changes into @SubsId, @oldProperty, @newProperty;
while @@fetch_status = 0
  begin
  update postpaid..em_subscribers set IMSI=@newProperty 
  where SubsId=@SubsId and IMSI=@oldProperty;
  fetch next from imsi_changes into @SubsId, @oldProperty, @newProperty;
  end
close imsi_changes
deallocate imsi_changes
  
-- apply to postpaid..em_subscribers the MSISDN changes in SDR
declare msisdn_changes cursor READ_ONLY for
  select SubsId, OldProperty, NewProperty from SDR 
  where PropertyKey=25 order by Time;

open msisdn_changes
fetch next from msisdn_changes into @SubsId, @oldProperty, @newProperty;
while @@fetch_status = 0
  begin
  update postpaid..em_subscribers set MSISDN=@newProperty 
  where SubsId=@SubsId and MSISDN=@oldProperty;
  fetch next from msisdn_changes into @SubsId, @oldProperty, @newProperty;
  end
close msisdn_changes
deallocate msisdn_changes

-- uncategorize the Riniring and EM Prepaid CoS subscribers whose CoS has already been changed
update postpaid..em_subscribers set customer_id='0'  --Riniring
where customer_id='809' and COSID!=58 and COSID!=59; 
update postpaid..em_subscribers set customer_id='0'  --EM Prepaid CoS
where customer_id='810' and COSID!=27;

-- categorize the eagle numbers
update postpaid..em_subscribers set customer_id='253' where customer_id='0' and COSID='28';

-- categorize the unclassified subscribers whose CC_LastName matches an existing customer
update postpaid..em_subscribers 
set customer_id = (select customer_id from postpaid..em_customers C 
                   where postpaid..em_subscribers.CC_LastName = C.customer)
where customer_id='0' and CC_LastName in (select customer from postpaid..em_customers);

-- (re)categorize the Riniring and EM Prepaid CoS subscribers
update postpaid..em_subscribers set customer_id='809'  --Riniring
where customer_id='0' and COSID in (58,59); 
update postpaid..em_subscribers set customer_id='810'  --EM Prepaid CoS
where customer_id='0' and COSID=27;

-- categorize the amdocs dummy customers
update em_subscribers set customer_id='939' where customer_id=0 and MSISDN like 'C%';


-- clean the tables that we used
exec emsp_update_subscribers_clean;

END
GO


-------------------------------------------------------------------------------------
-- Clean the tables created by emsp_update_subscribers.
-------------------------------------------------------------------------------------

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_update_subscribers_clean' AND type = 'P')
DROP PROCEDURE emsp_update_subscribers_clean;
GO

CREATE PROCEDURE emsp_update_subscribers_clean
AS
BEGIN
SET NOCOUNT ON;

-- drop the table subscribers_crm_x
if exists(select table_name from information_schema.tables where table_name = 'subscribers_crm_x') 
drop table subscribers_crm_x;

-- drop the table subscribers_em_x
if exists(select table_name from information_schema.tables where table_name = 'subscribers_em_x') 
drop table subscribers_em_x;

-- drop the table subscribers_pps
if exists(select table_name from information_schema.tables where table_name = 'subscribers_pps') 
drop table subscribers_pps;

-- drop the table subscribers_crm
if exists(select table_name from information_schema.tables where table_name = 'subscribers_crm') 
drop table subscribers_crm;

-- drop the table subscribers
if exists(select table_name from information_schema.tables where table_name = 'subscribers') 
drop table subscribers;

-- drop the table SDR
if exists(select table_name from information_schema.tables where table_name = 'SDR') 
drop table SDR;

-- copy to only_in_SubsPPS the content of subscribers_pps_x
if exists(select table_name from information_schema.tables where table_name = 'subscribers_pps_x') 
insert into only_in_SubsPPS select * from subscribers_pps_x;

-- drop the table subscribers_pps_x
if exists(select table_name from information_schema.tables where table_name = 'subscribers_pps_x') 
drop table subscribers_pps_x;


END
GO
