﻿
use postpaid_scratch;

-------------------------------------------------------------------------------------
-- Import from CRM and PPS the sybscribers with SubId in the given list.
-------------------------------------------------------------------------------------

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_get_subscribers_by_subsid' AND type = 'P')
DROP PROCEDURE emsp_get_subscribers_by_subsid;
GO

CREATE PROCEDURE emsp_get_subscribers_by_subsid
	@p_SubId_list varchar(4000)
AS
BEGIN
SET NOCOUNT ON;

-- drop the table subscribers_pps_l
if exists(select table_name from information_schema.tables where table_name = 'subscribers_pps_l') 
drop table subscribers_pps_l;

-- get data from [10.234.252.63].Magnolia.dbo.SubsPPS into subscribers_pps_l
exec('select * into subscribers_pps_l from [10.234.252.63].Magnolia.dbo.SubsPPS '
     + ' where [9060_SubsId] in (' + @p_SubId_list + ');');

-- drop the table subscribers_crm_l
if exists(select table_name from information_schema.tables where table_name = 'subscribers_crm_l') 
drop table subscribers_crm_l;

-- get data from [10.234.252.61].CRM.dbo.Subscribers into subscribers_crm_l
exec('select * into subscribers_crm_l from [10.234.252.61].CRM.dbo.Subscribers '
     + ' where PrepaidSubsID in (' + @p_SubId_list + ');');

-- drop the table subscribers_l
if exists(select table_name from information_schema.tables where table_name = 'subscribers_l') 
drop table subscribers_l;

-- join the data from both tables into subscribers_l
select P.[9060_SubsId] as SubsId, C.SubsId as CC_SubsId, P.[9061_SubsNumber] as MSISDN, 
       P.[9050_SubsIMSI] as IMSI, P.[9180_COSId] as COSID, P.[1011_StatusId] as StatusId,
       P.[9260_AcountId] as AccountId, P.[9062_CreationDate] as CreationDate,
       P.[9063_ActivationDate] as ActivationDate, C.FirstName, C.LastName, C.Address, C.City,
       C.State, C.BirthDate, C.Gender, C.Comments, C.PassportNumber as PassportNr,
       COS.cos_name, COS.postpaid
into subscribers_l
from subscribers_pps_l P 
     left join subscribers_crm_l C on (P.[9060_SubsId] = C.PrepaidSubsID)
     left join postpaid..s_cos COS on (P.[9180_COSId] = COS.cos_id);

/*
select [9060_SubsId] as SubsId, [9061_SubsNumber] as MSISDN, [9050_SubsIMSI] as IMSI,
   [9180_COSId] as COSID, [1011_StatusId] as StatusId, [9260_AcountId] as AccountId,
   [9062_CreationDate] as CreationDate, [9063_ActivationDate] as ActivationDate
into subscribers_pps_l
from [10.234.252.63].Magnolia.dbo.SubsPPS
where [9060_SubsId] in (@p_SubId_list);

select SubsId as CC_SubsId, PrepaidSubsID as SubsId, 
   SubscriberNumber as MSISDN, FirstName, LastName, Address, City, State, 
   BirthDate, Gender, Comments, PassportNumber as PassportNr, CreationDate
into subscribers_crm_l
from [10.234.252.61].CRM.dbo.Subscribers
where CreationDate between @p_fromDate and @p_toDate;
*/

-- add the field CC_LastName to subscribers_l
alter table subscribers_l add CC_LastName varchar(200);
update subscribers_l set CC_LastName = LastName;

-- correct the fields FirstName and LastName of subscribers_l
exec emsp_fix_names 'subscribers_l';

-- append to s_subscribers the ones that are in subscribers_l
insert into postpaid..em_subscribers
  (customer_id, SubsId, MSISDN, IMSI, FirstName, LastName, CC_LastName, PassportNr, AccountID,
   Address1, Address2, City, COSID, postpaid, PaymentResponsible, RegistrationDate, StatusId)
select '0' as customer_id, SubsId, MSISDN, IMSI, FirstName, LastName, CC_LastName, PassportNr,
       AccountID, Address as Address1, '' as Address2, City, COSID, postpaid, 
       'false' as PaymentResponsible, CreationDate as RegistrationDate, StatusId
from subscribers_l;

-- remove temporary tables
drop table subscribers_l;
drop table subscribers_pps_l;
drop table subscribers_crm_l;

END
GO
