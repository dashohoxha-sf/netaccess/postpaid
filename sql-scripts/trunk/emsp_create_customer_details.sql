
-------------------------------------------------------------------------------------
-- Add summary details for each customer in the table inv_details_1, by getting 
-- the sum of the details of each subscriber of the customer.
-------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_create_customer_details' AND type = 'P')
DROP PROCEDURE emsp_create_customer_details;
GO

CREATE PROCEDURE emsp_create_customer_details
AS
BEGIN
SET NOCOUNT ON;

-- delete any details that may have been added in any previous attempts
delete from inv_details_1 where InvoiceType = 'customer';

-- append to inv_details_1 the invoice details for the customer invoices
-- which are calculated by summing the details of each invoice of the customer
insert into inv_details_1 
  (InvoiceType, InvoiceID, DetailCategory, DetailType, DetailName,
   DetailOrder, UnitCount, Usage, NetValue, VAT, TotalValue) 
select 'customer' as InvoiceType,
       S.CustomerInvoiceID, DetailCategory, DetailType, DetailName, DetailOrder,
       SUM(UnitCount), SUM(Usage),
			 SUM(cast(NetValue as float)),
			 SUM(cast(VAT as float)),
			 SUM(cast(TotalValue as float)) 
from inv_details_1 D 
     left join inv_subscribers_1 S on (D.InvoiceID = S.InvoiceID)
group by S.CustomerInvoiceID, DetailCategory, DetailType, DetailName, DetailOrder
order by S.CustomerInvoiceID, DetailOrder;

/*
-- update the field id of inv_details_1
update inv_details_1
set id = (select customer_id from inv_customers C 
          where C.InvoiceID = inv_details_1.InvoiceID)
where type = 'customer';
*/

END
GO
