﻿
use postpaid_scratch;

-------------------------------------------------------------------------------------
-- Correct the monthly fees of the suscribers accroding to the values in the table
-- em_customer_fees. This only modifies the existing monthly fee names and amounts.
-- If a subscriber has no monthly fee, then it will not be added for him even if the
-- corresponding customer has a monthly fee.
-------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_monthly_fees_list' AND type = 'P')
DROP PROCEDURE emsp_monthly_fees_list;
GO

CREATE PROCEDURE emsp_monthly_fees_list
AS
BEGIN
SET NOCOUNT ON;

-- drop the table tmp_monthlyfee_list, if it exists
if exists(select table_name from information_schema.tables where table_name = 'tmp_monthlyfee_list') 
drop table tmp_monthlyfee_list;

select S.SubsId, F.product_id 
into tmp_monthlyfee_list
from postpaid..em_subscribers S 
     inner join postpaid..em_customer_fees F on (S.customer_id=F.customer_id and S.COSID=F.cos_id);

/*
-- create the table of monthly fees for each subscriber
create table tmp_monthlyfee_list (SubsId int, ProductId int);

declare @customer_id int, @cos_id int, @product_id int;

declare customer_fees cursor READ_ONLY for
  select customer_id, cos_id, product_id from postpaid..em_customer_fees;

open customer_fees
fetch next from customer_fees into @customer_id, @cos_id, @product_id;
while @@fetch_status = 0
  begin
    -- create a product row for each subscriber of the given customer, 
		-- which has the given class of service
    insert into tmp_monthlyfee_list
    select SubsId, @product_id from postpaid..em_subscribers
    where customer_id = @customer_id and COSID = @cos_id;

    -- fetch the next record of the cursor
    fetch next from customer_fees into @customer_id, @cos_id, @product_id;
  end
close customer_fees
deallocate customer_fees
*/

END
GO
