
update IN_GroupInvoice 
set GrandTotalValue = (select sum(TotalValueValue) from IN_InvoiceDetails 
                  where IN_InvoiceDetails.InvoiceID = IN_GroupInvoice.InvoiceID
									group by IN_InvoiceDetails.InvoiceID);

update IN_Invoice 
set GrandTotalValue = (select sum(TotalValueValue) from IN_InvoiceDetails 
                  where IN_InvoiceDetails.InvoiceID = IN_Invoice.InvoiceID
									group by IN_InvoiceDetails.InvoiceID);


--------------------------------------------------------------------------
-- get a list of customers and the TotalValue amounts that they have to pay
--------------------------------------------------------------------------

select top(10) * 
from IN_GroupInvoice G
left join IN_InvoiceDetails D on (D.InvoiceID = G.InvoiceID);

select GroupName, NIPT, 
       sum(NetValue) as Net, 
			 sum(VAT) as VAT,
			 sum(TotalValueValue) as TotalValue,
			 SerialNumber as InvoiceSerialNumber,
			 G.InvoiceID
from IN_GroupInvoice G
left join IN_InvoiceDetails D on (D.InvoiceID = G.InvoiceID)
group by G.InvoiceID, GroupName, NIPT, SerialNumber
order by GroupName;


----------------------------------------------------------------------------------
-- get a list of subscriber invoices and the TotalValue amounts that they have to pay
----------------------------------------------------------------------------------

select top(10) * 
from IN_Invoice I
     left join IN_InvoiceDetails D on (D.InvoiceID = I.InvoiceID)
where SerialNumber is not null;

select FirstName, LastName, PassportNr, MSISDN,
       sum(NetValue) as Net, 
			 sum(VAT) as VAT,
			 sum(TotalValueValue) as TotalValue,
			 SerialNumber as InvoiceSerialNumber,
			 I.InvoiceID
from IN_Invoice I
     left join IN_InvoiceDetails D on (D.InvoiceID = I.InvoiceID)
where SerialNumber is not null
group by I.InvoiceID, I.SubsId, SerialNumber, MSISDN, FirstName, LastName, PassportNr
order by LastName, FirstName, MSISDN;


----------------------------------------------------------------------------------
-- get a list of subscriber expenses and the TotalValue amounts that they have spent
----------------------------------------------------------------------------------

select top(10) * 
from IN_Invoice I
     left join IN_InvoiceDetails D on (D.InvoiceID = I.InvoiceID)
where SerialNumber is null;

select GroupName, LastName, FirstName, PassportNr, MSISDN,
       sum(NetValue) as Net, 
			 sum(VAT) as VAT,
			 sum(TotalValueValue) as TotalValue,
			 I.InvoiceID as InvID
from IN_Invoice I
     left join IN_InvoiceDetails D on (D.InvoiceID = I.InvoiceID)
where SerialNumber is null
group by I.InvoiceID, I.SubsId, MSISDN, FirstName, LastName, GroupName, PassportNr
order by GroupName, LastName, FirstName, MSISDN;


----------------------------------------------------------------------------------
-- get a full list of customers and subscribers and invoice details
----------------------------------------------------------------------------------


select top(100) * 
from IN_GroupInvoice G
     full join IN_Invoice I on (G.InvoiceID = I.GroupInvoiceID)
     left join IN_InvoiceDetails D on (D.InvoiceID = I.InvoiceID)
;
		 
select top(1000) I.GroupName, MSISDN, FirstName, LastName, DetailName, NetValue, VAT, TotalValueValue
from IN_GroupInvoice G
     full join IN_Invoice I on (G.InvoiceID = I.GroupInvoiceID)
     left join IN_InvoiceDetails D on (D.InvoiceID = I.InvoiceID)
order by GroupName, MSISDN, FirstName, LastName, DetailOrder;

select I.GroupName, MSISDN, FirstName, LastName, DetailName, NetValue, VAT, TotalValueValue
from IN_GroupInvoice G
     full join IN_Invoice I on (G.InvoiceID = I.GroupInvoiceID)
     left join IN_InvoiceDetails D on (D.InvoiceID = I.InvoiceID)
order by GroupName, MSISDN, FirstName, LastName, DetailOrder;


