
use postpaid;

----------------------------------------------------------------------------------
-- Get a summary report for the subscribers of the given customer.
----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_customer_summary_report' AND type = 'P')
DROP PROCEDURE emsp_customer_summary_report;
GO

CREATE PROCEDURE emsp_customer_summary_report
	@p_customer_id integer,
  @p_month varchar(20)
AS
BEGIN
SET NOCOUNT ON;

-- drop the table em_customer_summary_table, if it exists
if exists(select table_name from information_schema.tables where table_name = 'em_customer_summary_table') 
drop table em_customer_summary_table;

-- get the subscribers that belong to the given customer
select FirstName, LastName, MSISDN, COSID, InvoiceID, SubsId, TotalCharge
into em_customer_summary_table
from inv_subscribers 
where customer_id = @p_customer_id and [month] = @p_month;

-- drop the table inv_customer_details, if it exists
if exists(select table_name from information_schema.tables where table_name = 'inv_customer_details') 
drop table inv_customer_details;

-- get all the details of the customer for the given month
select * into inv_customer_details
from inv_details 
where [month] = @p_month and InvoiceType = 'subscriber'
  and InvoiceID in (select InvoiceID from em_customer_summary_table);


-- add columns for storing the summary data
alter table em_customer_summary_table add 
  class_of_service nvarchar(250),
  group_calltime integer,
  group_callnr integer,
  group_callvalue float,
  group_smsnr integer,
  group_smsvalue float,
  eagle_calltime integer,
  eagle_callnr integer,
  eagle_callvalue float,
  eagle_smsnr integer,
  eagle_smsvalue float,
  othergsm_calltime integer,
  othergsm_callnr integer,
  othergsm_callvalue float,
  albtelecom_calltime integer,
  albtelecom_callnr integer,
  albtelecom_callvalue float,
  international_calltime integer,
  international_callnr integer,
  international_callvalue float,
  roaming_callnr integer,
  roaming_callvalue float,
  roaming_smsnr integer,
  roaming_smsvalue float,
  other_smsnr integer,
  other_smsvalue float,
  discount float,
  other_fees float;

-- fill the summary fields with data, getting them from the table inv_customer_details
update em_customer_summary_table 
set
  class_of_service        = (select cos_name from s_cos C 
                                        where (em_customer_summary_table.CosID = C.cos_id)),
	
  group_calltime          = (select Usage from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory = 'call' and D.DetailType = 'group'),
  group_callnr            = (select UnitCount from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'group'),
  group_callvalue         = (select TotalValue from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'group'),
  group_smsnr             = (select UnitCount from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='sms' and D.DetailType = 'group'),
  group_smsvalue          = (select TotalValue from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='sms' and D.DetailType = 'group'),
  eagle_calltime          = (select Usage from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'eagle'),
  eagle_callnr            = (select UnitCount from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'eagle'),
  eagle_callvalue         = (select TotalValue from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'eagle'),
  eagle_smsnr             = (select UnitCount from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='sms' and D.DetailType = 'eagle'),
  eagle_smsvalue          = (select TotalValue from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='sms' and D.DetailType = 'eagle'),
  othergsm_calltime       = (select Usage from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'other_gsm'),
  othergsm_callnr         = (select UnitCount from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'other_gsm'),
  othergsm_callvalue      = (select TotalValue from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'other_gsm'),
  albtelecom_calltime     = (select Usage from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'albtelecom'),
  albtelecom_callnr       = (select UnitCount from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'albtelecom'),
  albtelecom_callvalue    = (select TotalValue from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'albtelecom'),
  international_calltime  = (select Usage from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'international'),
  international_callnr    = (select UnitCount from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'international'),
  international_callvalue = (select TotalValue from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'international'),
  roaming_callnr          = (select UnitCount from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'roaming'),
  roaming_callvalue       = (select TotalValue from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='call' and D.DetailType = 'roaming'),
  roaming_smsnr           = (select UnitCount from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='sms' and D.DetailType = 'roaming'),
  roaming_smsvalue        = (select TotalValue from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='sms' and D.DetailType = 'roaming'),
  other_smsnr             = (select UnitCount from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='sms' and D.DetailType = 'other'),
  other_smsvalue          = (select TotalValue from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailCategory='sms' and D.DetailType = 'other'),
  discount                = (select sum(TotalValue) from inv_customer_details D 
                                        where D.InvoiceID = em_customer_summary_table.InvoiceID
	                                      and D.DetailType = 'discount');

-- add also any other fees
update em_customer_summary_table
set other_fees = 
     (select sum(TotalValue) from inv_customer_details D 
		  where D.InvoiceID = em_customer_summary_table.InvoiceID 
			  and D.DetailCategory not in ('call', 'sms')
				and D.DetailType != 'discount'
			group by D.InvoiceID);

-- output the data
select * from em_customer_summary_table
order by LastName, FirstName, MSISDN;

-- drop the auxiliary tables
drop table em_customer_summary_table;
drop table inv_customer_details;

END
GO
