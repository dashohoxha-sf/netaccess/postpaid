﻿
-------------------------------------------------------------------------------------
-- Correct the monthly fees of the suscribers accroding to the values in the table
-- subs_monthly_fees. 
-------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_correct_monthly_fees_1' AND type = 'P')
DROP PROCEDURE emsp_correct_monthly_fees_1;
GO

CREATE PROCEDURE emsp_correct_monthly_fees_1
AS
BEGIN
SET NOCOUNT ON;

-- drop the table inv_details_1_new_monthlyfees, if it exists
if exists(select table_name from information_schema.tables where table_name = 'inv_details_1_new_monthlyfees') 
drop table inv_details_1_new_monthlyfees;

-- create the table inv_details_1_new_monthlyfees with the same fields as inv_details_1
select top(1) * into inv_details_1_new_monthlyfees from inv_details_1;
truncate table inv_details_1_new_monthlyfees;

-- add some auxiliary fields
alter table inv_details_1_new_monthlyfees 
add monthly_fee float, month_length int, invoice_period int;

declare @msisdn varchar(50), @product_name varchar(100), @product_id int, @product_price int;

declare subscriber_fees cursor READ_ONLY for
  select msisdn, mf from subs_monthly_fees where mf!='no-monthly-fee';

open subscriber_fees
fetch next from subscriber_fees into @msisdn, @product_name;
while @@fetch_status = 0
  begin
    -- get @product_id and @product_price from the table s_productprices
    select @product_id=ProductId, @product_price=Price from s_productprices where ProductName=@product_name;
    
    -- create a monthly fee detail row for the subscriber with the given @msisdb
    insert into inv_details_1_new_monthlyfees
    select 'subscriber' as InvoiceType, InvoiceID, 'feature' as DetailCategory, 
           @product_id as DetailType, @product_name as DetailName,
           20 as DetailOrder, 1 as UnitCount, 1 as Usage,
           0.0 as NetValue, 0.0 as VAT, 0.0 as TotalValue,
           @product_price as monthly_fee,
           day(ToDate) as month_length,
           datediff(day,FromDate,ToDate)+1 as invoice_period
    from inv_subscribers_1
    where MSISDN = @msisdn;

    -- fetch the next record of the cursor
    fetch next from subscriber_fees into @msisdn, @product_name;
  end
close subscriber_fees
deallocate subscriber_fees

-- recalculate the monthly_fee for the registrations that happened in the middle of the month
--select * from inv_details_1_new_monthlyfees where month_length < 28;
delete from inv_details_1_new_monthlyfees where month_length < 28;
update inv_details_1_new_monthlyfees
set monthly_fee = (invoice_period * monthly_fee / month_length);

-- update NetValue, VAT, TotalValue according to monthly_fee
update inv_details_1_new_monthlyfees
set NetValue = 5.0 * monthly_fee / 6.0 / 100.0, 
    VAT = 1.0 * monthly_fee / 6.0 / 100.0, 
    TotalValue = monthly_fee / 100.0;

-- delete the auxiliary tables of inv_details_1_new_monthlyfees
alter table inv_details_1_new_monthlyfees 
drop column monthly_fee, column month_length, column invoice_period;



-- drop the table inv_details_1_old_monthlyfees, if it exists
if exists(select table_name from information_schema.tables where table_name = 'inv_details_1_old_monthlyfees') 
drop table inv_details_1_old_monthlyfees;

-- save the original monthly fee rows for later inspection
select * into inv_details_1_old_monthlyfees
from inv_details_1
where InvoiceType = 'subscriber' and DetailCategory = 'feature'
  and DetailType in (select cast(ProductId as varchar(20)) from s_productprices);
  
-- replace the original monthly fee rows with the modified ones
delete from inv_details_1
where InvoiceType = 'subscriber' and DetailCategory = 'feature'
  and DetailType in (select cast(ProductId as varchar(20)) from s_productprices);

insert into inv_details_1 select * from inv_details_1_new_monthlyfees;

-- do not drop the new and old monthlyfee tables
--drop table inv_details_1_new_monthlyfees;
--drop table inv_details_1_old_monthlyfees;


/*** queries to revert back the monthly fee changes (in case that needed):

-- delete the new monthly fees that were generated
delete from inv_details_1
where InvoiceType = 'subscriber' and DetailCategory = 'feature'
  and DetailType in (select cast(ProductId as varchar(20)) from s_productprices);

-- restore back the original monthly fees that were deleted
insert into inv_details_1 select * from inv_details_1_old_monthlyfees;
*/

END
GO
