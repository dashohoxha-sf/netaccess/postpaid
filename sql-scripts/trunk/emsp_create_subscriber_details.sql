
-------------------------------------------------------------------------------------
-- Create the table inv_details_1 and fill it with invoice details of the last month.
-- The details for each invoice are received from INV_AREA_SubsInvDialed_1,
-- which has the call and SMS details for each subscriber invoice.
-- They are also received from INV_TDR_1, which has the transactions for each
-- subscriber (monthly fees, features, etc).
-------------------------------------------------------------------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_create_subscriber_details' AND type = 'P')
DROP PROCEDURE emsp_create_subscriber_details;
GO

CREATE PROCEDURE emsp_create_subscriber_details
AS
BEGIN
SET NOCOUNT ON;

-- drop the table inv_details_1, if it exists
if exists(select table_name from information_schema.tables where table_name = 'inv_details_1') 
drop table inv_details_1;

create table inv_details_1
(
  InvoiceType     nvarchar(20),
  InvoiceID       int,
  DetailCategory  nvarchar(20),
  DetailType      nvarchar(20),
  DetailName      nvarchar(200),
  DetailOrder     int,
  UnitCount       int,
  Usage           int,
  NetValue        float,
  VAT             float,
  TotalValue      float
);

-- get the call and SMS details for each subscriber invoice from INV_AREA_SubsInvDialed
insert into inv_details_1
  (InvoiceType, InvoiceID, DetailCategory, DetailType, DetailName,
   DetailOrder, UnitCount, Usage, NetValue, VAT, TotalValue) 
select 'subscriber' as InvoiceType, invID as InvoiceID, 
       null as DetailCategory, DialedArea as DetailType, null as DetailName, 
       null as DetailOrder, CDRsCount as UnitCount, 
       (case when DialedArea in (2,4,6,11,12) then CDRSCount else TotalDuration end) as Usage,
	     5.0*cast(TotalChargeableAmount as float)/6.0 as NetValue,
	     1.0*cast(TotalChargeableAmount as float)/6.0 as VAT,
			 1.0*cast(TotalChargeableAmount as float) as TotalValue
from INV_AREA_SubsInvDialed_1
where invID in (select InvoiceID from inv_subscribers_1)
order by invID;

-- set fields according to the called area; the relation between call areas
-- and detail names can be seen in the table INV_AREA_prefix_others
update inv_details_1 
set DetailCategory = 'call', 
    DetailType     = 'group', 
    DetailName     = 'Thirrjet ne grup', 
    DetailOrder    = '0' 
where DetailType='9';

update inv_details_1 
set DetailCategory = 'call',
    DetailType     = 'eagle',
    DetailName     = 'Eagle - Eagle',
    DetailOrder    = '1'
where DetailType='3';

update inv_details_1
set DetailCategory = 'call',
    DetailType     = 'other_gsm',
    DetailName     = 'Eagle - Other GSM',
    DetailOrder    = '2'  
where DetailType='5';

update inv_details_1
set DetailCategory = 'call',
    DetailType     = 'albtelecom',
    DetailName     = 'Eagle - Albtelecom',
    DetailOrder    = '3'
where DetailType='10';

update inv_details_1
set DetailCategory = 'call',
    DetailType     = 'international',
    DetailName     = 'International',
    DetailOrder    = '4'
where DetailType='1';

update inv_details_1
set DetailCategory = 'call',
    DetailType     = 'roaming',
    DetailName     = 'Thirrjet Roaming',
    DetailOrder    = '5'
where DetailType='11';

update inv_details_1
set DetailCategory = 'sms',
    DetailType     = 'group',
    DetailName     = 'SMS ne Grup',
    DetailOrder    = '6'
where DetailType='2';

update inv_details_1
set DetailCategory = 'sms',
    DetailType     = 'eagle',
    DetailName     = 'Eagle SMS',
    DetailOrder    = '7'
where DetailType='4';

update inv_details_1
set DetailCategory = 'sms',
    DetailType     = 'other',
    DetailName     = 'Other SMS',
    DetailOrder    = '8'
where DetailType='6';

update inv_details_1
set DetailCategory = 'sms',
    DetailType     = 'roaming',
    DetailName     = 'SMS Roaming',
    DetailOrder    = '9'
where DetailType='12';

/*
update inv_details_1
set DetailCategory = 'gprs',
    DetailType     = 'gprs',
    DetailName     = 'GPRS',
    DetailOrder    = '10'
where DetailType='x';
*/

update inv_details_1
set DetailCategory = 'gprs',
    DetailType     = 'roaming',
    DetailName     = 'GPRS Roaming',
    DetailOrder    = '11'
where DetailType='13';

update inv_details_1
set DetailCategory = 'charge',
    DetailType     = 'discount',
    DetailName     = 'Zbritje dhe Rimbursim',
    DetailOrder    = '12'
where DetailType='9999';

-- append transaction details for each subscriber (monthly fees, features, etc.)
insert into inv_details_1
  (InvoiceType, InvoiceID, DetailCategory, DetailType, DetailName,
   DetailOrder, UnitCount, Usage, NetValue, VAT, TotalValue) 
select 'subscriber' as InvoiceType, InvoiceID,
       'feature' as DetailCategory, [9402_FeatureValue] as DetailType,
       null as DetailName, 20 as DetailOrder, 1 as UnitCount, 1 as Usage, 
			 5.0*cast([10004_ChargeableAmount] as float)/600.0 as NetValue,
			 1.0*cast([10004_ChargeableAmount] as float)/600.0 as VAT,
		   1.0*cast([10004_ChargeableAmount] as float)/100.0 as TotalValue
from INV_TDR_1 TDR
     left join inv_subscribers_1 S on (TDR.[9060_SubsId] = S.SubsId)
where [10004_ChargeableAmount] > 0
  and [10001_TransactionKey] = 2    -- and [10001_TransactionKey] not in (1,3,4,6)
order by InvoiceID;

-- get the DetailName from the table INV_Features
update inv_details_1 
set DetailName = (select [9425_DefaultName] from INV_Features F 
                  where F.[9400_FeatureId] = inv_details_1.DetailType)
where DetailCategory = 'feature';

-- there are no annual taxes anymore, delete any details about annual tax

if exists(select table_name from information_schema.tables where table_name = 'inv_details_1_annualtax') 
drop table inv_details_1_annualtax;

select * into inv_details_1_annualtax from inv_details_1 where DetailName like 'Annual%';
delete from inv_details_1 where DetailName like 'Annual%';

/*
-- update the field id of inv_details_1
update inv_details_1
set id = (select subs_id from inv_subscribers S 
          where S.InvoiceID = inv_details_1.InvoiceID)
where type = 'subscriber';
*/

END
GO
