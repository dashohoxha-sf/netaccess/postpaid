
xyz;

-------------------------------------------------------------------------------------
-- Import from the tables Magnolia.dbo.SubsPPS and CRM.dbo.Subscribers 
-- all the subscribers, in order to do some checks.
-------------------------------------------------------------------------------------

use postpaid_scratch;

---------
exec emsp_get_subscribers_all;
---------

-- backup em_subscribers
drop table em_subscribers_bak
select * into em_subscribers_bak from postpaid..em_subscribers;

select top(10) * from subscribers;
select count(*) from subscribers;  --173723
select count(*) from postpaid..em_subscribers;  -- 173712

select top(10) * from subscribers_crm;
select count(*) from subscribers_crm;  -- 173580

select top(10) * from subscribers_pps;
select count(*) from subscribers_pps;  -- 173722

select max(SubsId) from postpaid..em_subscribers;  -- 249140
select max(SubsId) from subscribers;  -- 248755    -- 249153

delete from subscribers where SubsId > 249140;
delete from subscribers_crm where SubsId > 249140;
delete from subscribers_pps where SubsId > 249140;


select top(10) * from customers_crm;
select count(*) from customers_crm;  -- 173839
select count(*) from customers_crm where CustomerName!=CustomerCode;  -- 844
select count(*) from customers_crm where CustomerName=CustomerCode;  -- 172992
select top(10) * from customers_crm where CustomerName!=CustomerCode;
delete from customers_crm where CustomerName=CustomerCode;

-- find doublicated SubsId-s
select SubsId, count(*) from postpaid..em_subscribers 
group by SubsId having count(*) > 1;

-- find doublicated SubsId-s
select SubsId, count(*) from subscribers 
group by SubsId having count(*) > 1;

select * from subscribers where SubsId='167253';
select * from subscribers where CC_SubsId='167253';
delete from subscribers where CC_SubsId='167253';

select top(10) * from subscribers_pps;
select top(10) * from subscribers_crm;
select top(10) * from subscribers;
select top(10) * from subscribers_pps where SubsId is null;
select top(10) * from subscribers_crm where SubsId is null;
select top(10) * from subscribers where SubsId is null;
select count(*) from subscribers_pps;  -- 173722
select count(*) from subscribers_crm;  -- 173580
select count(*) from subscribers;   -- 173709
select * from subscribers_pps where SubsId not in (select SubsId from subscribers_crm);
select * from subscribers_crm where SubsId not in (select SubsId from subscribers_pps);
select SubsId, count(*) from subscribers_pps group by SubsId having count(*) > 1;
select SubsId, count(*) from subscribers_crm group by SubsId having count(*) > 1;
select SubsId, count(*) from subscribers group by SubsId having count(*) > 1;

-- subscribers that are only in postpaid..em_subscribers 
select * from postpaid..em_subscribers 
where SubsId not in (select SubsId from subscribers);

select top(10) * from [10.234.252.53].Magnolia.dbo.SubsPPS;

select * from [10.234.252.53].Magnolia.dbo.SubsPPS
where [9060_SubsId] in (225466,225463,230643,230645,230647,230650,230652,230655,238495);

select * from [10.234.252.53].Magnolia.dbo.SubsPPS
where [9061_SubsNumber] in 
(355672966801,355672966802,355672953400,355672953401,
355672953402,355672953403,355672953404,355672953405,355672966226);

select * from [10.234.252.53].Magnolia.dbo.SubsPPS
where [9050_SubsIMSI] in 
(276032002591801,276032002591802,276032002583400,276032002583401,
276032002583402,276032002583403,276032002583404,276032002583405,276032002591226);

/*
355672953400
355672953401
355672953402
355672953403
355672953404
355672953405

355672966226
355672966801
355672966802

241698
235784
234478
*/

select * from postpaid..em_subscribers 
where MSISDN in ('355672966226','355672966801','355672966802')
order by SubsId, MSISDN;

select top(10) * from [10.234.252.61].CRM.dbo.Subscribers;

select * from [10.234.252.61].CRM.dbo.Subscribers
where PrepaidSubsID in (225466,225463,230643,230645,230647,230650,230652,230655,238495);

select * from [10.234.252.61].CRM.dbo.Subscribers
where SubscriberNumber in 
(355672966801,355672966802,355672953400,355672953401,
355672953402,355672953403,355672953404,355672953405,355672966226);

-- subscribers that are only in subscribers
select * from subscribers
where SubsId not in (select SubsId from postpaid..em_subscribers);

-- insert the missing subscribers
insert into postpaid..em_subscribers
  (customer_id, SubsId, MSISDN, IMSI, FirstName, LastName, CC_LastName, PassportNr, AccountID,
   Address1, Address2, City, COSID, postpaid, PaymentResponsible, RegistrationDate, StatusId)
select '0' as customer_id, SubsId, MSISDN, IMSI, FirstName, LastName, LastName as CC_LastName, 
       PassportNr, AccountID, Address as Address1, '' as Address2, City, COSID, postpaid, 
       '' as PaymentResponsible, CreationDate as RegistrationDate, StatusId
from subscribers
where SubsId not in (select SubsId from postpaid..em_subscribers);

-- update the payment responsible field of the added subscribers
select * from postpaid..em_subscribers where PaymentResponsible='';
update postpaid..em_subscribers set PaymentResponsible='true'
where PaymentResponsible='' and postpaid='1';
update postpaid..em_subscribers set PaymentResponsible='false'
where PaymentResponsible='' and postpaid='0';


/*
select * from only_in_SubsPPS;

select * from [10.234.252.61].CRM.dbo.Subscribers
where SubsID in (select SubsID from only_in_SubsPPS);

select top(10) * from [10.234.252.61].CRM.dbo.Subscribers;

select * from [10.234.252.61].CRM.dbo.Subscribers
where SubscriberNumber in (select MSISDN from only_in_SubsPPS);

select * from [10.234.252.53].Magnolia.dbo.SubsPPS
where [9060_SubsId] in (select SubsID from only_in_SubsPPS);

select * from [10.234.252.53].Magnolia.dbo.SubsPPS
where [9061_SubsNumber] in (select MSISDN from only_in_SubsPPS);

select top(10) * from subscribers_crm;
select top(10) * from subscribers_pps;

select count(*) from [10.234.252.53].Magnolia.dbo.SubsPPS
where [1011_StatusId] = 28;
*/


select * 
from postpaid..em_subscribers S1
     full outer join subscribers S2 on (S1.SubsId=S2.SubsId)
where S2.SubsId is null;


select SubsId, count(*) from postpaid..em_subscribers
group by SubsId having count(*) > 1;

select MSISDN, count(*) from postpaid..em_subscribers
group by MSISDN having count(*) > 1;

select IMSI, count(*) from postpaid..em_subscribers
group by IMSI having count(*) > 1;

select * from postpaid..em_subscribers
where MSISDN in (select MSISDN from postpaid..em_subscribers
                 group by MSISDN having count(*) > 1);

select * from postpaid..em_subscribers
where IMSI in (select IMSI from postpaid..em_subscribers
               group by IMSI having count(*) > 1);

select S1.MSISDN, S2.MSISDN, * 
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where S1.MSISDN!=S2.MSISDN;

select S1.COSID, S2.COSID, * 
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where S1.COSID!=S2.COSID;

select S1.COSID, S2.COSID, count(*)
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where S1.COSID!=S2.COSID
group by S1.COSID, S2.COSID;

select S1.IMSI, S2.IMSI, * 
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where S1.IMSI!=S2.IMSI;

select S1.postpaid, S2.postpaid, * 
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where S1.postpaid!=S2.postpaid;

select S1.StatusId, S2.StatusId, * 
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where S1.StatusId!=S2.StatusId;


update postpaid..em_subscribers
set MSISDN = (select MSISDN from subscribers S where S.SubsId = postpaid..em_subscribers.SubsId);

update postpaid..em_subscribers
set COSID = (select COSID from subscribers S where S.SubsId = postpaid..em_subscribers.SubsId);

select * from postpaid..em_subscribers where COSID is null;
delete from postpaid..em_subscribers where COSID is null;

update postpaid..em_subscribers
set IMSI = (select IMSI from subscribers S where S.SubsId = postpaid..em_subscribers.SubsId)
where SubsId in (110980,91568,136051);

update postpaid..em_subscribers
set AccountId = (select AccountId from subscribers S where S.SubsId = postpaid..em_subscribers.SubsId);

update postpaid..em_subscribers
set postpaid = (select postpaid from subscribers S where S.SubsId = postpaid..em_subscribers.SubsId);

update postpaid..em_subscribers
set RegistrationDate = (select CreationDate from subscribers S where S.SubsId = postpaid..em_subscribers.SubsId);

update postpaid..em_subscribers
set StatusId = (select StatusId from subscribers S where S.SubsId = postpaid..em_subscribers.SubsId);

update postpaid..em_subscribers
set CC_LastName = (select LastName from subscribers S where S.SubsId = postpaid..em_subscribers.SubsId);

select top(10) * from postpaid..em_subscribers;
select top(10) * from subscribers;
select top(10) * from subscribers_pps;
select top(10) * from subscribers_crm;

select top(10) * from [10.234.252.61].CRM.dbo.Subscribers;
select top(10) * from [10.234.252.53].Magnolia.dbo.SubsPPS

drop table tmp_subs;

select S1.* 
into tmp_subs
from postpaid..em_subscribers S1
     left join subscribers_crm S2 on (S1.SubsId=S2.SubsId)
where S2.SubsId is null;

select * from tmp_subs;

select * from [10.234.252.53].Magnolia.dbo.SubsPPS
where [9060_SubsId] in (select SubsId from tmp_subs);

select * from [10.234.252.61].CRM.dbo.Subscribers
where SubscriberNumber in (select MSISDN from subs_not_in_CRM);

select * from subs_not_in_CRM;

select S1.MSISDN, S2.MSISDN, * 
from postpaid..em_subscribers S1
     left join subscribers_crm S2 on (S1.SubsId=S2.SubsId)
where S1.MSISDN!=S2.MSISDN;

--exec emsp_fix_names 'subscribers_crm';

select S1.FirstName, S2.FirstName, * 
from postpaid..em_subscribers S1
     left join subscribers_crm S2 on (S1.SubsId=S2.SubsId)
where S1.FirstName!=S2.FirstName;

select (S1.FirstName + ' ' + S1.LastName), (S2.FirstName + ' ' + S2.LastName), * 
from postpaid..em_subscribers S1
     left join subscribers_crm S2 on (S1.SubsId=S2.SubsId)
where (S1.FirstName+' '+S1.LastName)!=((S2.FirstName+' '+S2.LastName) collate Latin1_General_CI_AS);

select S2.* 
into tmp_subs
from postpaid..em_subscribers S1
     left join subscribers_crm S2 on (S1.SubsId=S2.SubsId)
where (S1.FirstName+' '+S1.LastName)!=((S2.FirstName+' '+S2.LastName) collate Latin1_General_CI_AS);

select * from tmp_subs;
select SubsId, count(*) from tmp_subs group by SubsId having count(*) > 1;

update postpaid..em_subscribers set
  FirstName = (select FirstName from tmp_subs S 
               where S.SubsId = postpaid..em_subscribers.SubsId),
  LastName = (select LastName from tmp_subs S 
               where S.SubsId = postpaid..em_subscribers.SubsId)
where SubsId in (select SubsId from tmp_subs);

select ltrim(rtrim(S1.Address1)), ltrim(rtrim(S2.Address)), * 
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where ltrim(rtrim(S1.Address1))!=(ltrim(rtrim(S2.Address)) collate Latin1_General_CI_AS);

select S2.* 
into tmp_subs
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where ltrim(rtrim(S1.Address1))!=(ltrim(rtrim(S2.Address)) collate Latin1_General_CI_AS);

update postpaid..em_subscribers set
  Address1 = (select ltrim(rtrim(Address)) from tmp_subs S 
              where S.SubsId = postpaid..em_subscribers.SubsId)
where SubsId in (select SubsId from tmp_subs);

select S1.City, S2.City, * 
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where S1.City!=(S2.City collate Latin1_General_CI_AS);

drop table tmp_subs;

select S2.* 
into tmp_subs
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where S1.City!=(S2.City collate Latin1_General_CI_AS);

delete from tmp_subs where City='';

update postpaid..em_subscribers set
  City = (select City from tmp_subs S 
          where S.SubsId = postpaid..em_subscribers.SubsId)
where SubsId in (select SubsId from tmp_subs);


select S1.PassportNr, S2.PassportNr, * 
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where S1.PassportNr!=(S2.PassportNr collate Latin1_General_CI_AS);

select S2.* 
into tmp_subs
from postpaid..em_subscribers S1
     left join subscribers S2 on (S1.SubsId=S2.SubsId)
where S1.PassportNr!=(S2.PassportNr collate Latin1_General_CI_AS);

update postpaid..em_subscribers set
  PassportNr = (select PassportNr from tmp_subs S 
                where S.SubsId = postpaid..em_subscribers.SubsId)
where SubsId in (select SubsId from tmp_subs);

select * from postpaid..em_subscribers where PassportNr like '%1111%';
update postpaid..em_subscribers set PassportNr='' where PassportNr like '%1111%';
update postpaid..em_subscribers set PassportNr = replace(PassportNr, ' ', '') where PassportNr is not null;

select PassportNr from postpaid..em_subscribers;


-----

use postpaid;

update em_subscribers set PassportNr = ltrim(rtrim(PassportNr)) where PassportNr is not null;
update em_subscribers set PassportNr = upper(PassportNr) where PassportNr is not null;
update em_subscribers set PassportNr = replace(PassportNr, ' ', '') where PassportNr is not null;

update em_subscribers set City = ltrim(rtrim(City)) where City is not null;
update em_subscribers set City = upper(substring(City,1,1)) + lower(substring(City,2,len(City)));

update em_subscribers 
set Address1 = dbo.RegExReplace(Address1, ' +', ' ', DEFAULT) 
where Address1 is not null;

update em_subscribers 
set Address1 = dbo.RegExReplace(Address1, '^[\s\r\n]*', '', DEFAULT) 
where Address1 is not null;

update em_subscribers 
set Address1 = dbo.RegExReplace(Address1, '[\s\r\n]*$', '', DEFAULT) 
where Address1 is not null;

update em_subscribers 
set Address1 = dbo.RegExReplace(Address1, '\s*, *', ', ', DEFAULT) 
where Address1 is not null;

update em_subscribers 
set Address1 = dbo.RegExReplace(Address1, '\s*; *', ', ', DEFAULT) 
where Address1 is not null;

-----

use postpaid_scratch;

update subscribers set PassportNr = ltrim(rtrim(PassportNr)) where PassportNr is not null;
update subscribers set PassportNr = upper(PassportNr) where PassportNr is not null;

update subscribers set City = ltrim(rtrim(City)) where City is not null;
update subscribers set City = upper(substring(City,1,1)) + lower(substring(City,2,len(City)));

update subscribers 
set Address = dbo.RegExReplace(Address, ' +', ' ', DEFAULT) 
where Address is not null;

update subscribers 
set Address = dbo.RegExReplace(Address, '^[\s\r\n]*', '', DEFAULT) 
where Address is not null;

update subscribers 
set Address = dbo.RegExReplace(Address, '[\s\r\n]*$', '', DEFAULT) 
where Address is not null;

update subscribers 
set Address = dbo.RegExReplace(Address, '\s*, *', ', ', DEFAULT) 
where Address is not null;

update subscribers 
set Address = dbo.RegExReplace(Address, '\s*; *', ', ', DEFAULT) 
where Address is not null;
