
create table s_products_alb (product_name_en varchar(100), product_name_al varchar(100));

insert into s_products_alb (product_name_en, product_name_al) values ('Annual Tax 1 Jan 30 Jun', 'Taksa Vjetore e aparatit 1 Janar-30 Qershor');
insert into s_products_alb (product_name_en, product_name_al) values ('Annual Tax 1 Jul 31 Dec', 'Taksa Vjetore e aparatit 1 Korrik-31 Dhjetor');
insert into s_products_alb (product_name_en, product_name_al) values ('Edge Monthly Fee 2000 Leke', 'Tarifa fikse mujore EDGE 2000 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Edge Monthly Fee 250 Leke', 'Tarifa fikse mujore EDGE 250 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Edge Monthly Fee 3000 Leke Full', 'Tarifa fikse mujore EDGE  3000 Leke (e plote)');
insert into s_products_alb (product_name_en, product_name_al) values ('Edge Monthly Fee 3000 Leke Partial', 'Tarifa fikse mujore EDGE 3000 Leke (e pjesshme)');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 0 Leke', 'Tarifa fikse mujore 0 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 100 Leke', 'Tarifa fikse mujore 100 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 1000 Leke', 'Tarifa fikse mujore 1000 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 1400 Leke', 'Tarifa fikse mujore 1400 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 150 Leke', 'Tarifa fikse mujore 150 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 1500 Leke', 'Tarifa fikse mujore 1500 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 1600 Leke', 'Tarifa fikse mujore 1600 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 1800 Leke', 'Tarifa fikse mujore 1800 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 1900 Leke', 'Tarifa fikse mujore 1900 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 200 Leke', 'Tarifa fikse mujore 200 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 2000 Leke', 'Tarifa fikse mujore 2000 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 2400 Leke', 'Tarifa fikse mujore 2400 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 350 Leke', 'Tarifa fikse mujore 350 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 400 Leke', 'Tarifa fikse mujore 400 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 500 Leke', 'Tarifa fikse mujore 500 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 600 Leke', 'Tarifa fikse mujore 600 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 700 Leke', 'Tarifa fikse mujore 700 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 800 Leke', 'Tarifa fikse mujore 800 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee 900 Leke', 'Tarifa fikse mujore 900 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee Service 100 Leke', 'Tarifa fikse mujore e sherbimit 100 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee Service 1000 Leke', 'Tarifa fikse mujore e sherbimit 1000 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee Service 150 Leke', 'Tarifa fikse mujore e sherbimit 150 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee Service 1500 Leke', 'Tarifa fikse mujore e sherbimit 1500 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee Service 200 Leke', 'Tarifa fikse mujore e sherbimit 200 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee Service 350 Leke', 'Tarifa fikse mujore e sherbimit 350 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee Service 400 Leke', 'Tarifa fikse mujore e sherbimit 400 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee Service 500 Leke', 'Tarifa fikse mujore e sherbimit 500 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee Service 700 Leke', 'Tarifa fikse mujore e sherbimit 700 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly fee Service 800 Leke', 'Tarifa fikse mujore e sherbimit 800 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Monthly Fee Service 900 Leke', 'Tarifa fikse mujore e sherbimit 900 Leke');
insert into s_products_alb (product_name_en, product_name_al) values ('Prisoner Directorate MF 500 Leke', 'Drejtoria e Burgjeve - Tarifa fikse mujore  500 Leke');

insert into s_products_alb (product_name_en, product_name_al) values ('Voice mail service', 'Sektetaria Telefonike');
insert into s_products_alb (product_name_en, product_name_al) values ('CRBT Service', 'Sherbimi CRBT');
insert into s_products_alb (product_name_en, product_name_al) values ('PostCall', 'PostCall');

select * from s_products_alb;

select replace(product_name_al, 'Tarifa fikse ', 'Tarifa ') from s_products_alb 
where product_name_al like 'Tarifa fikse%';

-- remove the word 'fikse'
update s_products_alb 
set product_name_al = replace(product_name_al, 'Tarifa fikse ', 'Tarifa ')
where product_name_al like 'Tarifa fikse%';


select top(10) * from inv_details_1 where DetailCategory='feature';
select DetailName, count(*) from inv_details_1 where DetailCategory='feature' group by DetailName;

-- find the services which occur in the invoices, but do not have a corresponding translation
select DetailName, count(*) as cnt
into tmp_product_list
from inv_details_1 where DetailCategory='feature' group by DetailName;

drop table tmp_product_list;
select * from tmp_product_list;

select * from tmp_product_list where DetailName not in (select product_name_en from s_products_alb);

-- translate the monthly fees
select count(*) from inv_details_1 where DetailCategory='feature';
update inv_details_1
set DetailName = (select product_name_al from s_products_alb P 
                  where P.product_name_en = inv_details_1.DetailName)
where DetailCategory='feature';

select * from inv_details_1 where DetailName is null;


