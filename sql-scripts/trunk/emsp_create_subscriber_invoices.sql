
use postpaid_scratch;

-----------------------------------------------------------------
-- Fill the table inv_subscribers_1 with the subscriber invoices 
-- of the last month.
-- Initially, get the data of the invoices of the last month 
-- from the tables INV_SumTrans_1 and INV_InvoiceNumbers_1.
-- Then add subscriber details from the table postpaid..em_subscribers.
-----------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_create_subscriber_invoices' AND type = 'P')
DROP PROCEDURE emsp_create_subscriber_invoices;
GO

CREATE PROCEDURE emsp_create_subscriber_invoices
AS
BEGIN
SET NOCOUNT ON;

-- drop the table inv_subscribers_1, if it exists
if exists(select table_name from information_schema.tables where table_name = 'inv_subscribers_1') 
drop table inv_subscribers_1;

create table inv_subscribers_1
(
  subs_id           int,
  customer_id       int,
  SubsId            int,
  InvoiceID         int,
  CustomerInvoiceID int,
  TotalCharge       float,
  InvoiceDate       datetime,
  FromDate          datetime,
  ToDate            datetime,
  MSISDN            nvarchar(20),
  FirstName         nvarchar(50),
  LastName          nvarchar(50),
  Address1          nvarchar(200),
  Address2          nvarchar(200),
  City              nvarchar(50),
  COSID             int,
  PassportNr        nvarchar(50),
  SerialNumber      nvarchar(50),
  AccountID         int,
  payable           nvarchar(10)
);

-- get InvoiceID, SubsID, TotalCharge, InvoiceDate from INV_SumTrans_1
-- and FromDate, ToDate from INV_InvoiceNumbers_1
insert into inv_subscribers_1 
  (SubsID, InvoiceID, TotalCharge, InvoiceDate, FromDate, ToDate)
select
  T.[9060_SubsId]                  as SubsID,
  T.INVID                          as InvoiceID,
  cast(T.TotalNewCharge as float)  as TotalCharge,
  T.SumDate                        as InvoiceDate, 
  N.DateFrom                       as FromDate,
  N.DateTo                         as ToDate
from INV_SumTrans_1 T
     left join INV_InvoiceNumbers_1 N on (T.[INVID] = N.[INVID]);

-- add subscriber details from the table postpaid..em_subscribers
update inv_subscribers_1 set
  subs_id     = (select subs_id     from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId),
  customer_id = (select customer_id from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId),
  MSISDN      = (select MSISDN      from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId),
  FirstName   = (select FirstName   from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId),
  LastName    = (select LastName    from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId),
  Address1    = (select Address1    from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId),
  Address2    = (select Address2    from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId),
  City        = (select City        from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId),
  COSID       = (select COSID       from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId),
  PassportNr  = (select PassportNr  from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId),
  AccountID   = (select AccountID   from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId),
  payable     = (select PaymentResponsible from postpaid..em_subscribers S 
                                    where S.SubsId = inv_subscribers_1.SubsId);

END
GO
