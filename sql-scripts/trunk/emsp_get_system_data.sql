﻿-------------------------------------------------------------------------------------
-- Copy (refresh) some system tables.
-------------------------------------------------------------------------------------

use postpaid;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_get_system_data' AND type = 'P')
DROP PROCEDURE emsp_get_system_data;
GO

CREATE PROCEDURE emsp_get_system_data
AS
BEGIN
SET NOCOUNT ON;

-------------- get the list of classes of service -----------------

if exists(select table_name from information_schema.tables where table_name = 's_cos') 
drop table s_cos;

select [9180_CosId] as cos_id, 
       [9181_CosName] as cos_name, 
       [8860_SubscriberType] as postpaid
into s_cos
from [10.234.252.63].Magnolia.dbo.COS;

------------ get products (monthly fees etc.) and their prices ---------------

if exists(select table_name from information_schema.tables where table_name = 's_productprices') 
drop table s_productprices;

select P.ProductID as ProductId, P.ProductName, PT.ProductTariffsPrice as Price 
into s_productprices
from [10.234.252.63].Magnolia.dbo.Products P
     left join [10.234.252.63].Magnolia.dbo.ProductTariffPlanMembers PTPM 
                on (P.ProductID = PTPM.ProductID)
     left join [10.234.252.63].Magnolia.dbo.ProductTariffs PT 
                on (PT.[9620_TariffExId] = PTPM.[9620_TariffExId])
where PT.ProductTariffsPrice is not null
  and P.ProductName not like '%annual%'
  and P.ProductName not like '%service%'
order by P.ProductName;

--------------------- get the list of features -----------------------

if exists(select table_name from information_schema.tables where table_name = 's_features') 
drop table s_features;

select * into s_features from [10.234.252.63].Invoice.dbo.INV_Features;

END
GO
