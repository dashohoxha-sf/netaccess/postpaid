﻿
use postpaid_scratch;

-------------------------------------------------------------------------------------
-- Correct the monthly fees of the suscribers accroding to the values in the table
-- em_customer_fees. This only modifies the existing monthly fee names and amounts.
-- If a subscriber has no monthly fee, then it will not be added for him even if the
-- corresponding customer has a monthly fee.
-------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_add_monthly_fees' AND type = 'P')
DROP PROCEDURE emsp_add_monthly_fees;
GO

CREATE PROCEDURE emsp_add_monthly_fees
AS
BEGIN
SET NOCOUNT ON;

-- drop the table inv_details_1_monthlyfee_new, if it exists
if exists(select table_name from information_schema.tables where table_name = 'inv_details_1_monthlyfee_new') 
drop table inv_details_1_monthlyfee_new;

-- create the table inv_details_1_monthlyfee_new with the same fields as inv_details_1
select top(1) * into inv_details_1_monthlyfee_new from inv_details_1;
truncate table inv_details_1_monthlyfee_new;

-- add some auxiliary fields
alter table inv_details_1_monthlyfee_new 
add monthly_fee float, month_length int, invoice_period int;

-- get the new monthly fees into inv_details_1_monthlyfee_new
insert into inv_details_1_monthlyfee_new
select 'subscriber' as InvoiceType, InvoiceID, 'feature' as DetailCategory, 
       F.product_id as DetailType, P.ProductName as DetailName,
       20 as DetailOrder, 1 as UnitCount, 1 as Usage,
       0.0 as NetValue, 0.0 as VAT, 0.0 as TotalValue,
       P.Price as monthly_fee,
       day(ToDate) as month_length,
       datediff(day,FromDate,ToDate)+1 as invoice_period
from inv_subscribers_1 S
     inner join postpaid..em_customer_fees F 
		            on (S.customer_id=F.customer_id and S.COSID=F.cos_id)
		 left join postpaid..s_productprices P on (F.product_id=P.ProductId);

-- recalculate the monthly_fee for the registrations that happened in the middle of the month
--select * from inv_details_1_monthlyfee_new where month_length < 28;
delete from inv_details_1_monthlyfee_new where month_length < 28;
update inv_details_1_monthlyfee_new
set monthly_fee = (invoice_period * monthly_fee / month_length);

-- update NetValue, VAT, TotalValue according to monthly_fee
update inv_details_1_monthlyfee_new
set NetValue = 5.0 * monthly_fee / 6.0 / 100.0, 
    VAT = 1.0 * monthly_fee / 6.0 / 100.0, 
    TotalValue = monthly_fee / 100.0;

-- delete the auxiliary tables of inv_details_1_monthlyfee_new
alter table inv_details_1_monthlyfee_new 
drop column monthly_fee, column month_length, column invoice_period;



-- drop the table inv_details_1_monthlyfee_old, if it exists
if exists(select table_name from information_schema.tables where table_name = 'inv_details_1_monthlyfee_old') 
drop table inv_details_1_monthlyfee_old;

-- save the original monthly fee rows for later inspection
select * into inv_details_1_monthlyfee_old
from inv_details_1
where InvoiceType = 'subscriber'
  and DetailType in (select cast(ProductId as varchar(20)) from postpaid..s_productprices);
  --and InvoiceID in (select InvoiceID from inv_details_1_monthlyfee_new);
  
-- replace the original monthly fee rows with the new ones
delete from inv_details_1
where InvoiceType = 'subscriber' 
  and DetailType in (select cast(ProductId as varchar(20)) from postpaid..s_productprices);
  --and InvoiceID in (select InvoiceID from inv_details_1_monthlyfee_new);

insert into inv_details_1 select * from inv_details_1_monthlyfee_new;

-- do not drop the monthlyfee new and old tables
--drop table inv_details_1_monthlyfee_new;
--drop table inv_details_1_monthlyfee_old;


/*** queries to revert back the monthly fee changes (in case that needed):

-- delete the new monthly fees that were generated
delete from inv_details_1
where InvoiceType = 'subscriber' 
  and DetailType in (select cast(ProductId as varchar(20)) from postpaid..s_productprices);
  --and InvoiceID in (select InvoiceID from inv_details_1_monthlyfee_new);

-- restore back the original monthly fees that were deleted
insert into inv_details_1 select * from inv_details_1_monthlyfee_old;
*/

END
GO
