﻿
-- Check "Enable OLE Automation" in the Surface Area Configuration

IF NOT object_id('dbo.RegExReplace') IS NULL
DROP FUNCTION dbo.RegExReplace
go
CREATE FUNCTION dbo.RegExReplace
(
    @string varchar(8000),
    @regexp varchar(500),
    @replacestr varchar(8000),
    @casesensitive bit = 0
)
returns varchar(8000) AS
BEGIN
    DECLARE @handle int, @result varchar(8000)
 
    exec sp_oacreate 'vbscript.regexp', @handle output
    exec sp_oasetproperty @handle, 'pattern', @regexp
    exec sp_oasetproperty @handle, 'global', 'true'
    exec sp_oasetproperty @handle, 'ignorecase', @casesensitive
    exec sp_oamethod @handle, 'replace', @result output, @string, @replacestr
    exec sp_oadestroy @handle
 
    RETURN @result
END
go


-- example
-- select dbo.RegExReplace('<span>this shall remain.</span><br/>', '<[^>]*>', '', DEFAULT); 


