﻿
-------------------------------------------------------------------------------------
-- For some reasons, it happens sometimes that the same monthly fee is included more
-- than once in an invoice. This procedure cleans such cases from the table inv_details_1
-- and moves the multiple fees in the table inv_details_1_multiple_monthly_fees (for
-- inspecting them later).
-- This procedure is usually called after the procedure emsp_create_subscriber_details.
-------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_remove_multiple_monthly_fees' AND type = 'P')
DROP PROCEDURE emsp_remove_multiple_monthly_fees;
GO

CREATE PROCEDURE emsp_remove_multiple_monthly_fees
AS
BEGIN
SET NOCOUNT ON;

-- drop the table inv_details_1_multiple_monthly_fees, if it exists
if exists(select table_name from information_schema.tables where table_name = 'inv_details_1_multiple_monthly_fees') 
drop table inv_details_1_multiple_monthly_fees;

-- select all the multiple monthly fees into the table inv_details_1_multiple_monthly_fees
select * 
into inv_details_1_multiple_monthly_fees
from inv_details_1
where InvoiceID in
  (
    select InvoiceID from inv_details_1 where DetailType in (select ProductId from s_productprices)
    group by InvoiceType, InvoiceID, DetailCategory, DetailType, DetailName, 
             DetailOrder, UnitCount, Usage, NetValue, VAT, TotalValue
    having count(*) > 1
  )
and DetailType in (select ProductId from s_productprices);

-- remove from inv_details_1 the detals (rows) that are already copied 
-- to the table inv_details_1_multiple_monthly_fees
delete from inv_details_1
where InvoiceID in (select InvoiceID from inv_details_1_multiple_monthly_fees)
and DetailType in (select ProductId from s_productprices);

-- copy back to inv_details_1 a unique list of the details that were removed from it
insert into inv_details_1 
select distinct * from inv_details_1_multiple_monthly_fees;

-- do not drop the table inv_details_1_multiple_monthly_fees 
-- so that it can be used for later inspection
-- drop table inv_details_1_multiple_monthly_fees;

END
GO
