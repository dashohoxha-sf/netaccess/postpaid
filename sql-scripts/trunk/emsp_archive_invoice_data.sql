
use postpaid_archive;

-------------------------------------------------------------------------------------
-- Copy in the database postpaid_archive the invoice data of the last month.
-- Example: 
--   use postpaid_archive;
--   exec emsp_archive_invoice_data '200810';
-------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_archive_invoice_data' AND type = 'P')
DROP PROCEDURE emsp_archive_invoice_data;
GO

CREATE PROCEDURE emsp_archive_invoice_data
	@p_month varchar(10)
AS
BEGIN
SET NOCOUNT ON;

/*
select * into m200810_inv_customers_1 from postpaid_scratch..inv_customers_1;
select * into m200810_inv_subscribers_1 from postpaid_scratch..inv_subscribers_1;
select * into m200810_inv_details_1 from postpaid_scratch..inv_details_1;

select * into m200810_INV_SumTrans_1 from postpaid_scratch..INV_SumTrans_1;
select * into m200810_INV_InvoiceNumbers_1 from postpaid_scratch..INV_InvoiceNumbers_1;
select * into m200810_INV_AREA_SubsInvDialed_1 from postpaid_scratch..INV_AREA_SubsInvDialed_1;
select * into m200810_INV_TDR_1 from postpaid_scratch..INV_TDR_1;
select * into m200810_INV_CDR_1 from postpaid_scratch..INV_CDR_1;
select * into m200810_INV_Features from postpaid_scratch..INV_Features;
*/

exec('select * into m'+@p_month+'_inv_customers_1 from postpaid_scratch..inv_customers_1');
exec('select * into m'+@p_month+'_inv_subscribers_1 from postpaid_scratch..inv_subscribers_1');
exec('select * into m'+@p_month+'_inv_details_1 from postpaid_scratch..inv_details_1');

exec('select * into m'+@p_month+'_INV_SumTrans_1 from postpaid_scratch..INV_SumTrans_1');
exec('select * into m'+@p_month+'_INV_InvoiceNumbers_1 from postpaid_scratch..INV_InvoiceNumbers_1');
exec('select * into m'+@p_month+'_INV_AREA_SubsInvDialed_1 from postpaid_scratch..INV_AREA_SubsInvDialed_1');
exec('select * into m'+@p_month+'_INV_TDR_1 from postpaid_scratch..INV_TDR_1');
exec('select * into m'+@p_month+'_INV_CDR_1 from postpaid_scratch..INV_CDR_1');
exec('select * into m'+@p_month+'_INV_Features from postpaid_scratch..INV_Features');

END
GO
