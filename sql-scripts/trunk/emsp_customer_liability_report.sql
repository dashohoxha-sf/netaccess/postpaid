
----------------------------------------------------------------------------------
-- Get a summary report for the subscribers of the given customer.
----------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_customer_liability_report' AND type = 'P')
DROP PROCEDURE emsp_customer_liability_report;
GO

CREATE PROCEDURE emsp_customer_liability_report
	@p_customer_id integer
AS
BEGIN
SET NOCOUNT ON;

-- drop the table tmp_customer_invoices, if it exists
if exists(select table_name from information_schema.tables where table_name = 'tmp_customer_invoices') 
drop table tmp_customer_invoices;

-- get into tmp_customer_invoices all the invoices of the subscribers of the customer
select * into tmp_customer_invoices
from inv_subscribers where customer_id = @p_customer_id
order by MSISDN asc, month desc, FirstName, LastName;

-- drop the table tmp_liability_table, if it exists
if exists(select table_name from information_schema.tables where table_name = 'tmp_liability_table') 
drop table tmp_liability_table;

-- get the subscribers that belong to the given customer
select SubsId into tmp_liability_table
from tmp_customer_invoices group by SubsId;

-- add the fields MSISDN, FirstName and LastName to the liability table 
alter table tmp_liability_table add
  MSISDN varchar(200),
  FirstName varchar(200),
  LastName varchar(200);
  
update tmp_liability_table
set
  MSISDN    = (select top(1) MSISDN from tmp_customer_invoices I 
                                 where I.SubsId = tmp_liability_table.SubsId),
  FirstName = (select top(1) FirstName from tmp_customer_invoices I 
                                 where I.SubsId = tmp_liability_table.SubsId),
  LastName  = (select top(1) LastName from tmp_customer_invoices I 
                                 where I.SubsId = tmp_liability_table.SubsId);

declare months cursor READ_ONLY for
    select distinct month from inv_subscribers 
    where customer_id = @p_customer_id
    order by month;

declare @month varchar(20);

open months;
fetch next from months into @month;
while @@fetch_status=0
  begin
  exec('alter table tmp_liability_table add ['+@month+'] float;');
  exec('update tmp_liability_table '
       + 'set ['+@month+'] = (select sum(TotalCharge) from inv_subscribers I ' 
                            + ' where I.SubsId = tmp_liability_table.SubsId '
                            + ' and month='''+@month+''');');
  fetch next from months into @month;
  end
close months
deallocate months

-- add the column Total
alter table tmp_liability_table add Total float;
update tmp_liability_table
set Total = (select sum(TotalCharge) from inv_subscribers I
             where I.SubsId = tmp_liability_table.SubsId);
             
-- output the data
select * from tmp_liability_table order by MSISDN, FirstName, LastName;

-- drop the auxiliary tables
drop table tmp_liability_table;
drop table tmp_customer_invoices;

END
GO
