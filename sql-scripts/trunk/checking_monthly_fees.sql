select;

------------------------------------------------------------------------------------------
-- These are preliminary checks that are done before the invoices are generated.
------------------------------------------------------------------------------------------

select top(10) * from TDR;
select * from Magnolia.dbo.Products;
select top(10) * from Magnolia.dbo.SubsPPS;
select top(10) * from Magnolia.dbo.SubsPIM;
select * from Magnolia.dbo.COS;


select TDR.[9060_SubsId], PIM.FirstName, PIM.LastName, PPS.[9061_SubsNumber], COS.[9181_CosName], P.ProductName, 
   TDR.[10005_Time], TDR.[10004_ChargeableAmount], TDR.[10006_TDREndingBalance]
from TDR 
     left join Magnolia.dbo.Products as P on (TDR.[9033_CardSerialNumber] = P.ProductID)
		 left join Magnolia.dbo.SubsPPS as PPS on (TDR.[9060_SubsId] = PPS.[9060_SubsId])
		 left join Magnolia.dbo.SubsPIM as PIM on (TDR.[9060_SubsId] = PIM.[9060_SubsId])
		 left join Magnolia.dbo.COS as COS on (TDR.COSId = COS.[9180_CosId])
where TDR.[10001_TransactionKey]=2
	and COS.[8860_SubscriberType]=1
	and TDR.[10004_ChargeableAmount]!=0
	and PPS.[9061_SubsNumber] is not null
  and TDR.[10005_Time] >= '2008-08-01' and TDR.[10005_Time] < '2008-09-01'
	and P.ProductName like 'Annual%'
  and PPS.[9061_SubsNumber] in
(
select PPS1.[9061_SubsNumber]
from TDR as TDR1
     left join Magnolia.dbo.Products as P1 on (TDR1.[9033_CardSerialNumber] = P1.ProductID)
		 left join Magnolia.dbo.SubsPPS as PPS1 on (TDR1.[9060_SubsId] = PPS1.[9060_SubsId])
		 left join Magnolia.dbo.COS as COS1 on (TDR.COSId = COS1.[9180_CosId])
where TDR1.[10001_TransactionKey]=2
	and COS1.[8860_SubscriberType]=1
	and TDR1.[10004_ChargeableAmount]!=0
	and PPS1.[9061_SubsNumber] is not null
  and TDR1.[10005_Time] >= '2008-08-01' and TDR1.[10005_Time] < '2008-09-01'
	and P1.ProductName like 'Annual%'
group by PPS1.[9061_SubsNumber], P1.ProductName
having count(*) > 1
)
order by PPS.[9061_SubsNumber], P.ProductName, PIM.FirstName, PIM.LastName, TDR.[10005_Time];

select PPS.[9061_SubsNumber], P.ProductName, count(*)
from TDR 
     left join Magnolia.dbo.Products as P on (TDR.[9033_CardSerialNumber] = P.ProductID)
		 left join Magnolia.dbo.SubsPPS as PPS on (TDR.[9060_SubsId] = PPS.[9060_SubsId])
		 left join Magnolia.dbo.SubsPIM as PIM on (TDR.[9060_SubsId] = PIM.[9060_SubsId])
		 left join Magnolia.dbo.COS as COS on (TDR.COSId = COS.[9180_CosId])
where TDR.[10001_TransactionKey]=2
	and COS.[8860_SubscriberType]=1
	and TDR.[10004_ChargeableAmount]!=0
	and PPS.[9061_SubsNumber] is not null
  and TDR.[10005_Time] >= '2008-08-01' and TDR.[10005_Time] < '2008-09-01'
	and P.ProductName like 'Annual%'
group by PPS.[9061_SubsNumber], P.ProductName
having count(*) > 1
order by count(*) desc;


select top(100) TDR.[9060_SubsId], PIM.FirstName, PIM.LastName, PPS.[9061_SubsNumber], COS.[9181_CosName], P.ProductName, TDR.[10005_Time],
   TDR.[10004_ChargeableAmount], TDR.[10006_TDREndingBalance], TDR.[10007_EndingDebt], TDR.[10008_DebtChargeableAmont],
	 TDR.ReferenceNumber, TDR.SysDRNumber
from TDR 
     left join Magnolia.dbo.Products as P on (TDR.[9033_CardSerialNumber] = P.ProductID)
		 left join Magnolia.dbo.SubsPPS as PPS on (TDR.[9060_SubsId] = PPS.[9060_SubsId])
		 left join Magnolia.dbo.SubsPIM as PIM on (TDR.[9060_SubsId] = PIM.[9060_SubsId])
		 left join Magnolia.dbo.COS as COS on (TDR.COSId = COS.[9180_CosId])
where TDR.[10001_TransactionKey]=2
  and TDR.[10005_Time] >= '2008-07-01'
	and PPS.[9061_SubsNumber] = '355672104806'
order by TDR.[10005_Time];


select PPS.[9060_SubsId], PIM.FirstName, PIM.LastName, PPS.[9061_SubsNumber], COS.[9181_CosName], PPS.[9062_CreationDate]
from Magnolia.dbo.SubsPPS as PPS 
		 left join Magnolia.dbo.SubsPIM as PIM on (PPS.[9060_SubsId] = PIM.[9060_SubsId])
		 left join Magnolia.dbo.COS as COS on (PPS.[9180_COSId] = COS.[9180_CosId])
where PPS.[9062_CreationDate] between '2008-08-01' and '2008-09-01'
	and COS.[8860_SubscriberType]=1
  and COS.[9180_CosId]!=28
	and PIM.LastName!='EAGLE MOBILE'
	and COS.[9181_CosName] not like 'Tap%'
	--and COS.[9181_CosName] not like '%Edge%'
  and PPS.[9061_SubsNumber] not in
(
select distinct PPS.[9061_SubsNumber]
from TDR 
     left join Magnolia.dbo.Products as P on (TDR.[9033_CardSerialNumber] = P.ProductID)
		 left join Magnolia.dbo.SubsPPS as PPS on (TDR.[9060_SubsId] = PPS.[9060_SubsId])
		 left join Magnolia.dbo.COS as COS on (TDR.COSId = COS.[9180_CosId])
where TDR.[10001_TransactionKey]=2
	and COS.[8860_SubscriberType]=1
	and TDR.[10004_ChargeableAmount]!=0
	and PPS.[9061_SubsNumber] is not null
  and TDR.[10005_Time] between '2008-08-01' and '2008-09-01'
	and P.ProductName like 'Annual%'
)
order by PPS.[9062_CreationDate], PIM.LastName, PIM.FirstName, PPS.[9061_SubsNumber];

select count(*) from Magnolia.dbo.SubsPPS as PPS where PPS.[9062_CreationDate] between '2008-08-01' and '2008-09-01';

select PPS.[9060_SubsId], PIM.FirstName, PIM.LastName, PPS.[9061_SubsNumber], COS.[9181_CosName], PPS.[9062_CreationDate]
from Magnolia.dbo.SubsPPS as PPS 
		 left join Magnolia.dbo.SubsPIM as PIM on (PPS.[9060_SubsId] = PIM.[9060_SubsId])
		 left join Magnolia.dbo.COS as COS on (PPS.[9180_COSId] = COS.[9180_CosId])
where PPS.[9062_CreationDate] between '2008-08-01' and '2008-09-01'
	and COS.[8860_SubscriberType]=1
  and COS.[9180_CosId]!=28
	and PIM.LastName!='EAGLE MOBILE'
	and COS.[9181_CosName] not like 'Tap%'
	--and COS.[9181_CosName] not like '%Edge%'
  and PPS.[9061_SubsNumber] not in
(
select distinct PPS.[9061_SubsNumber]
from TDR 
     left join Magnolia.dbo.Products as P on (TDR.[9033_CardSerialNumber] = P.ProductID)
		 left join Magnolia.dbo.SubsPPS as PPS on (TDR.[9060_SubsId] = PPS.[9060_SubsId])
		 left join Magnolia.dbo.COS as COS on (TDR.COSId = COS.[9180_CosId])
where TDR.[10001_TransactionKey]=2
	and COS.[8860_SubscriberType]=1
	and TDR.[10004_ChargeableAmount]!=0
	and PPS.[9061_SubsNumber] is not null
  and TDR.[10005_Time] between '2008-08-01' and '2008-09-01'
	and P.ProductName like 'Monthly%'
)
order by PPS.[9062_CreationDate], PIM.LastName, PIM.FirstName, PPS.[9061_SubsNumber];
