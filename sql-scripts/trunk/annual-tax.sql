﻿------------------------------------------------------------------------------------
-- check that the annual tax is not entered multiple times for the same subscriber
------------------------------------------------------------------------------------

select top(10) * from inv_subscribersDetails where DetailType = 'annual-tax' and TotalValue='400';
select top(10) * from postpaid..em_subscust_matrix;
select top(10) * from INV_SubsDetails;
select top(10) * from inv_subscribersDetails;
select top(10) * from inv_subscribers;

-- get the number of the postpaid subscribers that were created (registered) last month
select count(*) from postpaid..em_subscust_matrix 
where CreationDate >= '2008-08-01' and CreationDate < '2008-09-01'
  and postpaid='1';

-- get the number of the annual taxe charges
select count(*) from inv_subscribersDetails where DetailType = 'annual-tax';  --2403

-- find the subscribers which have multiple annual taxes
select I.MSISDN, count(*)  
from inv_subscribers I left join inv_subscribersDetails D on (D.InvoiceID = I.InvoiceID)
where D.DetailType = 'annual-tax'
group by I.MSISDN
having count(*) > 1;

select InvoiceID, 'charge' as DetailCategory, 'annual-tax' as DetailType, 
       'Taksa Vjetore' as DetailName, '11' as DetailOrder, '1' as UnitCount, '1' as Usage,
       min(NetValue) as NetValue, '0' as VAT, min(TotalValue) as TotalValue
into IN_temp
from inv_subscribersDetails 
where DetailType = 'annual-tax'
group by InvoiceID having count(*) > 1;

select * from IN_temp;

select * 
from inv_subscribersDetails
where InvoiceID in (select InvoiceID from inv_subscribersDetails 
                    where DetailType = 'annual-tax'
                    group by InvoiceID having count(*) > 1)
  and DetailType = 'annual-tax'                  
;

delete from inv_subscribersDetails
where InvoiceID in (select InvoiceID from inv_subscribersDetails 
                    where DetailType = 'annual-tax'
                    group by InvoiceID having count(*) > 1)
  and DetailType = 'annual-tax'                  
;

insert into inv_subscribersDetails select * from IN_temp;

drop table IN_temp;

-- find the subscribers which have multiple annual taxes
select I.MSISDN, I.GroupName, I.LastName, I.FirstName, I.SubsId, I.InvoiceId, 
       C.[9181_CosName], D.DetailName, D.TotalValue
from inv_subscribers I 
     left join inv_subscribersDetails D on (D.InvoiceID = I.InvoiceID)
		 left join INV_COS C on (I.COSID = C.[9180_CosId])
where D.DetailType = 'annual-tax' 
  and I.MSISDN in
    (
      select I1.MSISDN
      from inv_subscribers I1 left join inv_subscribersDetails D1 on (D1.InvoiceID = I1.InvoiceID)
      where D1.DetailType = 'annual-tax'
      group by I1.MSISDN having count(*) > 1
    )
order by I.GroupName, I.LastName, I.MSISDN;


-- get the number of the postpaid subscribers that are registered last month
select count(*) from postpaid..em_subscust_matrix 
where CreationDate >= '2008-08-01' and CreationDate < '2008-09-01'
  and postpaid='1';

-- get the number of subscribers that got an annual tax last month
select count(*) 
from inv_subscribers I
     left join inv_subscribersDetails D on (D.InvoiceID = I.InvoiceID)
where DetailType = 'annual-tax';

-- find the number of the subscribers that are registered last month but do not have an annual tax
select MSISDN, CosName, CreationDate, customer, FirstName, LastName
from postpaid..em_subscust_matrix
where CreationDate >= '2008-08-01' and CreationDate < '2008-09-01'
  and postpaid='1'
	and MSISDN not in
         (
           select distinct MSISDN  
           from inv_subscribers I
                left join inv_subscribersDetails D on (D.InvoiceID = I.InvoiceID)
           where DetailType = 'annual-tax'
         )
order by customer, CreationDate, MSISDN;



-- get the number of the annual taxes that are charged to the subscribers that are registered last month
select count(*)   --1377
from inv_subscribersDetails D
     left join inv_subscribers I on (D.InvoiceID = I.InvoiceID)
		 left join postpaid..em_subscust_matrix M on (I.MSISDN = M.MSISDN)
where D.DetailType = 'annual-tax'
	and M.CreationDate >= '2008-08-01' and M.CreationDate < '2008-09-01';

-- get the annual taxes that are charged to the subscribers that are registered in the previous months
select I.LastName, I.FirstName, I.MSISDN, I.SubsId, M.CreationDate, D.DetailName
from inv_subscribersDetails D
     left join inv_subscribers I on (D.InvoiceID = I.InvoiceID)
		 left join postpaid..em_subscust_matrix M on (I.MSISDN = M.MSISDN)
where D.DetailType = 'annual-tax' and M.CreationDate < '2008-08-01'
order by LastName;


--------------------------------------------------------------------
-- Fix annual taxes for the month august-2008
--------------------------------------------------------------------

select top(10) * from postpaid..em_subscust_matrix; 

select count(*) from postpaid..em_subscust_matrix 
where CreationDate >= '2008-08-01' and CreationDate <= '2008-08-19';

select count(*) from inv_subscribersDetails where DetailType = 'annual-tax';  --1531
select count(*) from inv_subscribersDetails where DetailType='annual-tax';
select count(*) from inv_subscribersDetails where DetailType='annual-tax' and DetailValue='400';  --1531
select top(10) * from inv_subscribersDetails where DetailType = 'annual-tax';

-- InvoiceID:21089	DetailCategory:charge	DetailType:annual-tax	DetailName:Taksa Vjetore
-- DetailOrder:11	UnitCount:1	Usage:1	NetValue:200	VAT:0	TotalValue:200

select * from inv_subscribersDetails where DetailCategory='charge' and DetailType='annual-tax' and TotalValue!='200';
delete from inv_subscribersDetails where DetailCategory='charge' and DetailType='annual-tax' and TotalValue!='200';

select D.* 
--into EM_temp1
from inv_subscribersDetails D
     left join inv_subscribers I on (D.InvoiceID = I.InvoiceID)
     left join postpaid..em_subscust_matrix M on (I.MSISDN = M.MSISDN)
where D.DetailType='annual-tax'
  and M.CreationDate >= '2008-08-01' and M.CreationDate < '2008-08-19';

select * from EM_temp1;
select * from EM_temp1 where InvoiceID in (select InvoiceID from EM_temp1 group by InvoiceID having count(*) > 1);

select D.* 
from inv_subscribersDetails D
     left join inv_subscribers I on (D.InvoiceID = I.InvoiceID)
     left join postpaid..em_subscust_matrix M on (I.MSISDN = M.MSISDN)
where D.DetailType='annual-tax' and M.CreationDate >= '2008-08-19';

select *
--delete 
from inv_subscribersDetails
where InvoiceID in
(
select D.InvoiceID
from inv_subscribersDetails D
     left join inv_subscribers I on (D.InvoiceID = I.InvoiceID)
     left join postpaid..em_subscust_matrix M on (I.MSISDN = M.MSISDN)
where D.DetailType='annual-tax' and M.CreationDate >= '2008-08-19'
)
and DetailCategory = 'charge' and DetailType='annual-tax';


select D.* 
from inv_subscribersDetails D
     left join inv_subscribers I on (D.InvoiceID = I.InvoiceID)
     left join postpaid..em_subscust_matrix M on (I.MSISDN = M.MSISDN)
where D.DetailType='annual-tax' and M.CreationDate < '2008-07-01';

--update inv_subscribersDetails set NetValue=400, TotalValue=400
select * from inv_subscribersDetails
where InvoiceID in
(
select D.InvoiceID
from inv_subscribersDetails D
     left join inv_subscribers I on (D.InvoiceID = I.InvoiceID)
     left join postpaid..em_subscust_matrix M on (I.MSISDN = M.MSISDN)
where D.DetailType='annual-tax' and M.CreationDate < '2008-07-01'
)
and DetailCategory = 'charge' and DetailType='annual-tax';

select count(*) from inv_subscribersDetails where DetailType='annual-tax';
delete from inv_subscribersDetails where DetailType='annual-tax';

insert into inv_subscribersDetails select * from EM_temp1;

drop table EM_temp1;
