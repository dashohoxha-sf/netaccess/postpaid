﻿
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_transfer_db' AND type = 'P')
DROP PROCEDURE emsp_transfer_db;
GO

CREATE PROCEDURE emsp_transfer_db
AS
BEGIN
SET NOCOUNT ON;

-- drop the tables, if they exist
if exists(select table_name from information_schema.tables where table_name = 'em_customer_fees') 
drop table em_customer_fees;
if exists(select table_name from information_schema.tables where table_name = 'em_customers') 
drop table em_customers;
if exists(select table_name from information_schema.tables where table_name = 'em_customers_x') 
drop table em_customers_x;
if exists(select table_name from information_schema.tables where table_name = 'em_subscribers') 
drop table em_subscribers;
if exists(select table_name from information_schema.tables where table_name = 'em_subscribers_x') 
drop table em_subscribers_x;
if exists(select table_name from information_schema.tables where table_name = 'em_subscust_matrix') 
drop table em_subscust_matrix;
if exists(select table_name from information_schema.tables where table_name = 'inv_customers') 
drop table inv_customers;
if exists(select table_name from information_schema.tables where table_name = 'inv_details') 
drop table inv_details;
if exists(select table_name from information_schema.tables where table_name = 'inv_serialnumbers') 
drop table inv_serialnumbers;
if exists(select table_name from information_schema.tables where table_name = 'inv_subscribers') 
drop table inv_subscribers;
if exists(select table_name from information_schema.tables where table_name = 's_cos') 
drop table s_cos;
if exists(select table_name from information_schema.tables where table_name = 's_features') 
drop table s_features;
if exists(select table_name from information_schema.tables where table_name = 's_productprices') 
drop table s_productprices;

-- transfer the tables
select * into em_customer_fees from mysql...em_customer_fees;
select * into em_customers from mysql...em_customers;
select * into em_customers_x from mysql...em_customers_x;
select * into em_subscribers from mysql...em_subscribers;
select * into em_subscribers_x from mysql...em_subscribers_x;
select * into em_subscust_matrix from mysql...em_subscust_matrix;
select * into inv_customers from mysql...inv_customers;
select * into inv_details from mysql...inv_details;
select * into inv_serialnumbers from mysql...inv_serialnumbers;
select * into inv_subscribers from mysql...inv_subscribers;
select * into s_cos from mysql...s_cos;
select * into s_features from mysql...s_features;
select * into s_productprices from mysql...s_productprices;



END
GO
