﻿
-------------------------------------------------------------------------------------
-- Correct the fields FirstName and LastName of the given table.
-------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_fix_names' AND type = 'P')
DROP PROCEDURE emsp_fix_names;
GO

CREATE PROCEDURE emsp_fix_names
	@p_table varchar(200)
AS
BEGIN
SET NOCOUNT ON;

/**** example
alter table em_subscribers_test add fullname varchar(250), idx1 int, idx2 int;

update em_subscribers_test set FirstName='' where FirstName is null;
update em_subscribers_test set LastName='' where LastName is null;
update em_subscribers_test set fullname = ltrim(rtrim(FirstName))+' '+ltrim(rtrim(LastName))+' ';
update em_subscribers_test set idx1 = charindex(' ', fullname);
update em_subscribers_test set idx2 = charindex(' ', fullname, idx1+1);

select top(100) fullname, substring(fullname, 1, idx1 - 1) from em_subscribers_test;
select top(100) fullname, substring(fullname, idx1+1, idx2-idx1-1) from em_subscribers_test;

update em_subscribers_test set FirstName = substring(fullname, 1, idx1 - 1);
update em_subscribers_test set LastName = substring(fullname, idx1+1, idx2-idx1-1);

update em_subscribers_test 
set FirstName = upper(substring(FirstName,1,1)) + lower(substring(FirstName,2,len(FirstName)));
update em_subscribers_test 
set LastName = upper(substring(LastName,1,1)) + lower(substring(LastName,2,len(LastName)));

select fullname, FirstName, LastName from em_subscribers_test;

alter table em_subscribers_test drop column fullname, column idx1, column idx2;
*/

exec('alter table '+@p_table+' add fullname varchar(250), idx1 int, idx2 int');

exec('update '+@p_table+' set FirstName='''' where FirstName is null');
exec('update '+@p_table+' set LastName='''' where LastName is null');
exec('update '+@p_table+' set fullname = ltrim(rtrim(FirstName))+'' ''+ltrim(rtrim(LastName))+'' ''');
exec('update '+@p_table+' set idx1 = charindex('' '', fullname)');
exec('update '+@p_table+' set idx2 = charindex('' '', fullname, idx1+1)');

exec('update '+@p_table+' set FirstName = substring(fullname, 1, idx1 - 1)');
exec('update '+@p_table+' set LastName = substring(fullname, idx1+1, idx2-idx1-1)');

exec('update '+@p_table
  +' set FirstName = upper(substring(FirstName,1,1)) + lower(substring(FirstName,2,len(FirstName)))');
exec('update '+@p_table
  +' set LastName = upper(substring(LastName,1,1)) + lower(substring(LastName,2,len(LastName)))');

exec('alter table '+@p_table+' drop column fullname, column idx1, column idx2');

END
GO
