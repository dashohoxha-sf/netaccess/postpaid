﻿
use postpaid_scratch;

-------------------------------------------------------------------------------------
-- Get the list of all the subscribers from the tables Magnolia.dbo.SubsPPS 
-- and CRM.dbo.Subscribers and save them into the tables: subscribers_pps,
-- subscribers_crm, and subscribers
-------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_get_subscribers_all' AND type = 'P')
DROP PROCEDURE emsp_get_subscribers_all;
GO

CREATE PROCEDURE emsp_get_subscribers_all
AS
BEGIN
SET NOCOUNT ON;

-- drop the table subscribers_pps
if exists(select table_name from information_schema.tables where table_name = 'subscribers_pps') 
drop table subscribers_pps;

-- get data from [10.234.252.53].Magnolia.dbo.SubsPPS into subscribers_pps
select [9060_SubsId] as SubsId, [9061_SubsNumber] as MSISDN, [9050_SubsIMSI] as IMSI,
   [9180_COSId] as COSID, [1011_StatusId] as StatusId, [9260_AcountId] as AccountId,
   [9062_CreationDate] as CreationDate, [9063_ActivationDate] as ActivationDate
into subscribers_pps
from [10.234.252.53].Magnolia.dbo.SubsPPS;

-- drop the table subscribers_crm
if exists(select table_name from information_schema.tables where table_name = 'subscribers_crm') 
drop table subscribers_crm;

-- get data from [10.234.252.61].CRM.dbo.Subscribers into subscribers_crm
select SubsId as CC_SubsId, PrepaidSubsID as SubsId, CustomerID,
   SubscriberNumber as MSISDN, FirstName, LastName, Address, City, State, 
   BirthDate, Gender, Comments, PassportNumber as PassportNr, CreationDate
into subscribers_crm
from [10.234.252.61].CRM.dbo.Subscribers;

-- drop the table subscribers
if exists(select table_name from information_schema.tables where table_name = 'subscribers') 
drop table subscribers;

-- join the data from both tables into subscribers
select P.SubsId, CC_SubsId, C.CustomerID, P.MSISDN, IMSI, COSID, StatusId, AccountId, 
       P.CreationDate, ActivationDate, FirstName, LastName, Address, City, State,
       BirthDate, Gender, Comments, PassportNr, cos_name, postpaid
into subscribers
from subscribers_pps P 
     left join subscribers_crm C on (P.SubsId = C.SubsId)
     left join s_cos on (P.COSID = s_cos.cos_id);

-- drop the table customers_crm
if exists(select table_name from information_schema.tables where table_name = 'customers_crm') 
drop table customers_crm;

-- get data from [10.234.252.61].CRM.dbo.Customers into customers_crm
select * into customers_crm
from [10.234.252.61].CRM.dbo.Customers;


END
GO

