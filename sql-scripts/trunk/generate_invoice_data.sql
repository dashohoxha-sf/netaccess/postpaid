select ;


-------------------------------------------------------------------------------------
-- Import from the tables Magnolia.dbo.SubsPPS and CRM.dbo.Subscribers the subscribers
-- that are registered lately.
-------------------------------------------------------------------------------------

use postpaid_scratch;

exec emsp_update_subscribers_clean;

exec emsp_get_latest_subscribers;

select count(*) from subscribers_crm;
select count(*) from subscribers_pps;
select count(*) from subscribers;
select count(*) from SDR;
select top(10) * from subscribers_crm;
select top(10) * from subscribers_pps;
select top(10) * from subscribers;
select top(10) * from SDR;
select * from subscribers_crm_x;
select * from subscribers_pps_x;
select * from subscribers_em_x;

exec emsp_update_subscribers;

select * from only_in_SubsPPS;

exec emsp_update_subscribers_clean;

--exec emsp_get_latest_modifications;

select * from SDR;
select top(10) * from SDR where PropertyKey=3;
select top(10) * from SDR where PropertyKey=15;
select top(10) * from SDR where PropertyKey=25;


-------------------------------------------------------------------------------------
-- Get the data of the invoices of the last month from the database Invoice at 10.234.252.63
-- into the local Invoice database. The data that are updated are those in the selected
-- intervals (@p_fromInvDate,@p_toInvDate) and (@p_fromDate,@p_toDate).
-- The parameters: @p_fromInvDate and @p_toInvDate are the dates when the invoice
-- records are processed (INV_SumTrans, INV_InvoiceNumbers), and the parameters 
-- @p_fromDate and @p_toDate are the dates of the records (INV_CDR, INV_TDR).
-- The invoice data of the last month are saved in the tables: INV_SumTrans_1,
-- INV_InvoiceNumbers_1, INV_TDR_1, INV_AREA_SubsInvDialed_1, INV_CDR_1, etc.
-- After processing them for generating the Eagle invoices, their data are appended to
-- the main tables: INV_SumTrans, INV_InvoiceNumbers, INV_TDR, INV_AREA_SubsInvDialed, INV_CDR,
-------------------------------------------------------------------------------------

-- move to archive the invoice data of the last month
use postpaid_archive;
exec emsp_archive_invoice_data '200811';

-- update also some tables from the billing system
use postpaid;
exec emsp_get_system_data;

use postpaid_scratch;
exec emsp_get_invoice_data '2008-11-03', '2008-12-02', '2008-11-01', '2008-12-01';


------------------------------------------------------------------------------
-- Make some preliminary checks before generating the invoices.
------------------------------------------------------------------------------

use postpaid_scratch;

select top(10) * from INV_SumTrans_1;
select top(10) * from INV_InvoiceNumbers_1;
select top(10) * from INV_AREA_SubsInvDialed_1;

select * from INV_SumTrans_1 where SumDate < '2008-12-01';
select * from INV_SumTrans_1 where SumDate > '2008-12-01';
select count(*) from INV_SumTrans_1 where SumDate = '2008-12-01';
select count(*) from [10.234.252.63].Invoice.dbo.INV_SumTrans where SumDate = '2008-12-01';

select * from INV_InvoiceNumbers_1 where INVDate < '2008-12-01';
select * from INV_InvoiceNumbers_1 where INVDate > '2008-12-01';
select count(*) from INV_InvoiceNumbers_1 where INVDate = '2008-12-01';

select * from INV_AREA_SubsInvDialed_1 
where invID in (select INVID from INV_InvoiceNumbers_1 where INVDate < '2008-12-01');

-- check the subscribers who had invoices before '2008-12-01'
select * 
from INV_AREA_SubsInvDialed_1 D 
     left join postpaid..em_subscribers S on (D.SubsId=S.SubsId)
		 left join INV_InvoiceNumbers_1 N on (D.invID=N.INVID)
where N.INVDate < '2008-12-01'
order by D.invID, D.CallsDate;

-- check that INV_SumTrans_1 and INV_InvoiceNumbers1 have the same number of records
select count(*) from INV_SumTrans_1;  -- 14398
select count(*) from INV_InvoiceNumbers_1;  -- 14399

select * from INV_SumTrans_1
where INVID not in (select INVID from INV_InvoiceNumbers_1);
                  
select * from INV_InvoiceNumbers_1    
where INVID not in (select INVID from INV_SumTrans_1 );
----- INVID=73477, SubsId=135016
select top(10) * from INV_SubsDetails where [9060_SubsId]='135016';  -- Test number, Billing

-- check that the rows in INV_SumTrans_1 and INV_InvoiceNumbers_1 have a one-to-one relation
select count(*)    -- 0
from INV_SumTrans_1 S join INV_InvoiceNumbers_1 N on (S.INVID = N.INVID)
where (S.SumDate != N.INVDate or S.[9060_SubsId] != N.[9060_Subsid]);

-- see how many invoice records are generated each day
select SumDate, count(*) from INV_SumTrans_1 
group by SumDate order by SumDate;

select min(INVID) from INV_SumTrans_1;  -- 52372
select max(INVID) from INV_SumTrans_1;  -- 151097, 102109

select min(invID) from INV_AREA_SubsInvDialed_1;   -- 52404
select max(invID) from INV_AREA_SubsInvDialed_1;   -- 151097, 102109

select * from INV_AREA_SubsInvDialed_1 
where invID not in (select INVID from INV_SumTrans_1);

select * from INV_AREA_SubsInvDialed_1 
where invID not in (select INVID from INV_InvoiceNumbers_1);

-- there are 1775 invoices that do not have call/sms records
select * from INV_SumTrans_1 
where INVID not in (select invID from INV_AREA_SubsInvDialed_1);

select * 
from INV_SumTrans_1 T 
     left join INV_SubsDetails D on (T.[9060_SubsId]=D.[9060_SubsId])
where INVID not in (select invID from INV_AREA_SubsInvDialed_1);

select N.DateFrom, N.DateTo, S.[9061_SubsNumber], S.FirstName, S.LastName, S.customerName, * 
from INV_InvoiceNumbers_1 N 
     left join INV_SubsDetails S on (N.[9060_SubsId]=S.[9060_SubsId])
where INVID not in (select invID from INV_AREA_SubsInvDialed_1)
  and len(S.[9061_SubsNumber])=12 and S.LastName!='Roaming';

-- check that there are no records about them in the original Invoice database as well
select * from [10.234.252.63].Invoice.dbo.INV_AREA_SubsInvDialed
where invID in (select INVID from INV_SumTrans_1 
                where INVID not in (select invID from INV_AREA_SubsInvDialed_1));

-- check for subscribers that have multiple invoice records
select SumDate, [9060_SubsId], count(*)
from INV_SumTrans_1 
group by SumDate, [9060_SubsId]
having count(*) > 1
order by SumDate, [9060_SubsId];

-- check for subscribers that have multiple invoice records
select * from INV_SubsDetails
where [9060_SubsId] in
  (
    select [9060_SubsId]
    from INV_SumTrans_1 
    group by SumDate, [9060_SubsId]
    having count(*) > 1
  );
-- [9060_SubsId]=74677, [9061_SubsNumber]=355672011133, Anareta Brahimi
select * from INV_SumTrans_1 where [9060_SubsId]=74677;
select * from INV_InvoiceNumbers_1 where [9060_SubsId]=74677;

select * from INV_SubsDetails
where [9060_SubsId] in
  (
    select [9060_SubsId] from INV_SumTrans_1 
    group by [9060_SubsId] having count(*) > 1
  );
  
-- check for subscribers that have multiple invoice records during the invoice period
select D.[9061_SubsNumber], D.[9060_SubsId], count(*)
from INV_SubsDetails D 
     left join INV_SumTrans_1 T on (D.[9060_SubsId] = T.[9060_SubsId])
where len(D.[9061_SubsNumber])>5
group by D.[9061_SubsNumber], D.[9060_SubsId]
having count(*) > 1
order by D.[9061_SubsNumber], D.[9060_SubsId];

-- check for negative chargeable amounts
select * from INV_AREA_SubsInvDialed_1 where TotalChargeableAmount < 0;

-- trace negative chargeable amounts to the Invoice database as well
select * from [10.234.252.63].Invoice.dbo.INV_AREA_SubsInvDialed
where CallsDate between '2008-11-03' and '2008-12-02' and TotalChargeableAmount < 0;

-- subscribers that have negative chargeable amounts
select * from INV_SubsDetails 
where [9060_SubsId] in (select SubsId from INV_AREA_SubsInvDialed_1
                        where TotalChargeableAmount < 0);

-- get a list of postpaid subscribers from the postpaid application
drop table postpaid_subs;
select * into postpaid_subs from postpaid..em_subscribers 
where postpaid = '1';

-- find the subscribers that are not in the invoices, but are in the postpaid application 
select S2.* 
from INV_SubsDetails S1 full outer join postpaid_subs S2 on (S1.[9060_SubsId] = S2.SubsId)
where S1.[9060_SubsId] is null
  and S2.RegistrationDate < '2008-12-01';

-- find the subscribers that are in INV_SubsDetails, but are not in the postpaid application
-- these numbers maybe have changed COS from postpaid to prepaid
select S1.postPaid_startDate, S1.* 
from INV_SubsDetails S1 full outer join postpaid_subs S2 on (S1.[9060_SubsId] = S2.SubsId)
where S2.SubsId is null
  and S1.postPaid_startDate < '2008-12-01' 
  and ((S1.DeletionDate >= '2008-11-01') or S1.DeletionDate is null);

select * from postpaid..em_subscribers where SubsId='225543';

-- find the subscribers that have different MSISDN in the invoice and in the postpaid application
select S1.[9061_SubsNumber], S2.MSISDN, * 
from INV_SubsDetails S1 inner join postpaid..em_subscribers S2 on (S1.[9060_SubsId] = S2.SubsId)
where S1.[9061_SubsNumber]!=S2.MSISDN 
  and S1.DeletionDate is null;

-- find a list of the subscribers which have invoices but are not in the list of em_subscribers
select D.LastName, D.postPaid_startDate, * 
from INV_SubsDetails D right join INV_SumTrans_1 T on (D.[9060_SubsId] = T.[9060_SubsId])
where D.[9060_SubsId] not in (select SubsId from postpaid..em_subscribers S) 
  --and len([9061_SubsNumber])=12
  and T.INVID is not null
  and D.postPaid_startDate < '2008-12-01'
order by D.postPaid_startDate desc;

---------------------------------------------------------------------

-- check the correctness of the subscriber numbers
select * from INV_SubsDetails where [9061_SubsNumber] like '67%';  -- 672050150, SubsId: 76407
select * from INV_SubsDetails where [9061_SubsNumber] like '%672050150%'; -- SubsId: 76573
select * from INV_SumTrans_1 where [9060_SubsId]='76573';

select * from INV_SubsDetails where [9061_SubsNumber] like '067%';
select * from INV_SubsDetails where [9061_SubsNumber] like '%672087268%';
select * from INV_SumTrans_1 where [9060_SubsId] in (76761,76762,84687,137184);
select * from postpaid..em_subscribers where SubsId='137184';

select * from INV_SubsDetails where len([9061_SubsNumber]) > 12;
select * from INV_SubsDetails where len([9061_SubsNumber]) < 12;
select * from INV_SubsDetails where len([9061_SubsNumber])=12 and [9061_SubsNumber] not like '35567%';

select * from INV_SubsDetails where [9061_SubsNumber] like 'C%';

-- make sure that there are no invoices for dummy subscribers (customers)
select * from INV_SumTrans_1 
where [9060_SubsId] in (select [9060_SubsId] from INV_SubsDetails
                        where [9061_SubsNumber] like 'C%');

select * from postpaid..em_subscribers where MSISDN like '067%';
select * from postpaid..em_subscribers where len(MSISDN) > 12;
select * from INV_SubsDetails where [9060_SubsId]='76162';
select * from postpaid..em_subscribers where len(MSISDN) < 12;
select * from postpaid..em_subscribers where len(MSISDN)=12 and MSISDN not like '35567%';

---------------------------------------------------------------------------
-- check the fields PaymentResponsible of the customers and subscribers
---------------------------------------------------------------------------

use postpaid;

select * from em_customers where PaymentResponsible is null;
select * from em_customers where PaymentResponsible!='true' and PaymentResponsible!='false';
select * from em_subscribers where PaymentResponsible is null;
select * from em_subscribers where PaymentResponsible!='true' and PaymentResponsible!='false';

select C.PaymentResponsible, S.PaymentResponsible, * 
from em_customers C left join em_subscribers S on (C.customer_id = S.customer_id)
where C.PaymentResponsible=S.PaymentResponsible 
  and C.customer_id>0 and C.customer_id!=810;

/*
update em_subscribers set PaymentResponsible='true'
where subs_id in
(
  select S.subs_id 
  from em_customers C left join em_subscribers S on (C.customer_id = S.customer_id)
  where C.PaymentResponsible='false' and S.PaymentResponsible='false'
);

update em_subscribers set PaymentResponsible='false'
where subs_id in
(
  select S.subs_id 
  from em_customers C left join em_subscribers S on (C.customer_id = S.customer_id)
  where C.PaymentResponsible='true' and S.PaymentResponsible='true'
);
*/

---------------------------------------------------

-- check that there is a subscriber detail for each invoice
select * from INV_SumTrans_1 where [9060_SubsId] not in (select [9060_SubsId] from INV_SubsDetails);
select * from INV_SumTrans_1 where [9060_SubsId] not in (select SubsId from postpaid..em_subscribers);


------------------------------------------------------------------------------
-- Get the data of the invoices into the table postpaid..inv_subscribers
------------------------------------------------------------------------------

use postpaid_scratch;

-- make sure that the SubsId is unique for each subscriber
select SubsId, count(*) from postpaid..em_subscribers group by SubsId having count(*) > 1;

exec emsp_create_subscriber_invoices;

-- check that there is no doublicated InvoiceID
select InvoiceID, count(*) from inv_subscribers_1 group by InvoiceID having count(*) > 1;

------------------------------------------------------------------------------
-- Check the data that are generated in the table inv_subscribers_1.
------------------------------------------------------------------------------

select top(10) * from inv_subscribers_1;
select count(*) from inv_subscribers_1;  -- 14398, 14361
select * from inv_subscribers_1 where MSISDN is null;
select count(*) from inv_subscribers_1 where len(MSISDN)<=5;  -- 391, 288
select count(*) from inv_subscribers_1 where len(MSISDN)>5;  -- 14079, 14073
select count(*) from inv_subscribers_1 where len(MSISDN)=12;  --14062

-- find the subscribers that have multiple invoices
select SubsId, count(*) from inv_subscribers_1 group by SubsId having count(*)>1; 
select SubsId, count(*) from inv_subscribers_1 
where len(MSISDN)=12 group by SubsId having count(*)>1;

-- find the subscribers that have multiple invoices
select MSISDN, count(*) from inv_subscribers_1 group by MSISDN having count(*)>1; 
select MSISDN, count(*) from inv_subscribers_1 
where len(MSISDN)=12 group by MSISDN having count(*)>1; 

-- find the subscribers that have multiple invoices
select * from postpaid..em_subscribers 
where MSISDN in (select MSISDN from inv_subscribers_1 
                 where len(MSISDN)=12 group by MSISDN having count(*)>1);

-- find the subscribers that have multiple invoices
select * from inv_subscribers_1 
where SubsId in (select SubsId from inv_subscribers_1 
                 where len(MSISDN)=12 group by SubsId having count(*)>1);

select MSISDN, count(*) from inv_subscribers_1 where InvoiceDate = '2008-12-01' 
group by MSISDN having count(*)>1; 
select SubsId, count(*) from inv_subscribers_1 where InvoiceDate = '2008-12-01'
group by SubsId having count(*)>1 ;

select * from inv_subscribers_1
where SubsId in (select SubsId from inv_subscribers_1 
                 where InvoiceDate = '2008-12-01' group by SubsId having count(*)>1)
order by SubsId;

-- check for subscribers that have an invoice but currently have a prepaid COS
select C.cos_name, * 
from inv_subscribers_1 S left join postpaid..s_cos C on (S.COSID = C.cos_id)
where C.postpaid=0;

-- check the subscribers that do not belong to a customer
select * from inv_subscribers_1 where customer_id is null;
select * from inv_subscribers_1 where customer_id='0';
select * from inv_subscribers_1 where customer_id='1';

select * from INV_SubsDetails 
where [9060_SubsId] in (select SubsId from inv_subscribers_1 S1 
                        where S1.customer_id='0' or S1.customer_id='1');

select * from INV_SubsDetails 
where [9060_SubsId] in (select SubsId from inv_subscribers_1 S1 where S1.customer_id is null);

-- find the groups that have only one subscriber
select * from postpaid..em_customers C1 left join postpaid..em_subscribers S1 
              on (C1.customer_id = S1.customer_id)
where C1.customer_id in 
(
  select C2.customer_id
  from postpaid..em_customers C2 left join postpaid..em_subscribers S2  
       on (C2.customer_id = S2.customer_id)
  group by C2.customer_id having count(*)<2
);

--------------------------------------------------------------------------------------
-- Create Customer Invoices
--------------------------------------------------------------------------------------

exec emsp_create_customer_invoices;

select top(10) * from inv_customers_1;
select count(*) from inv_customers_1;

--------------------------------------------------------------------------------------
-- Customer Invoice checks
--------------------------------------------------------------------------------------

select top(10) * from inv_subscribers_1;
select top(10) * from inv_customers_1;

select top(10) * from inv_subscribers_1 where CustomerInvoiceID is null;

------------------------------------------------------------------------------
-- Create the table inv_details_1 and fill it with the details of the subscribers.
------------------------------------------------------------------------------

select count(*) from INV_AREA_SubsInvDialed_1;   -- 64560, 62540
select min(invID) from INV_AREA_SubsInvDialed_1; -- 52404
select max(invID) from INV_AREA_SubsInvDialed_1; -- 151097, 102109
select min(InvoiceID) from inv_subscribers_1;    -- 52372
select max(InvoiceID) from inv_subscribers_1;    -- 151097, 102109

select * from inv_subscribers_1 where InvoiceID > 102109;

exec emsp_create_subscriber_details;

select * from inv_details_1_annualtax;

select top(10) * from inv_details_1;
select count(*) from inv_details_1;  -- 77734, 75714
select * from inv_details_1 where TotalValue is null and InvoiceType='subscriber';
select * from inv_details_1 where TotalValue is null and InvoiceType='customer';

select * from inv_subscribers_1 where InvoiceID is null;
select * from inv_subscribers_1 where CustomerInvoiceID is null;
select * from inv_customers_1 where InvoiceID is null;
select * from inv_customers_1 where customer_id is null;

-- check for negative TotalValues
select * from inv_details_1 where TotalValue < 0;

-- correct negative TotalValues
select * into tmp_details from inv_details_1 where TotalValue < 0;
select * from tmp_details;
update tmp_details set
  DetailCategory = 'charge',
	DetailType = 'discount',
	DetailName = 'Rimbursim p�r SMS t� pad�rguara',
	DetailOrder = 31,
	UnitCount = 1,
  NetValue = TotalValue, 
	VAT = 0;

-- correct negative TotalValues
select * from inv_details_1 where TotalValue < 0;
update inv_details_1 set NetValue = 0, VAT = 0, TotalValue = 0 where TotalValue < 0;

-- correct negative TotalValues
insert into inv_details_1 select * from tmp_details;
drop table tmp_details;

	
-------------##################################################------------------
-------------##################################################------------------
-------------##################################################------------------
-------------##################################################------------------

select * from inv_details_1 where InvoiceID is null;

select count(*) from INV_CDR_1;
select count(*) from INV_TDR_1;


select * from tmp_subsid_list;

select * from inv_subscribers_1 where SubsId in (select [9060_SubsId] from tmp_subsid_list);

select * from postpaid..em_subscribers
where SubsId in (select [9060_SubsId] from tmp_subsid_list);

select top(10) * from INV_SubsDetails;
select top(10) * from INV_SumTrans_1;

select * from INV_SumTrans_1 
where [9060_SubsId] in (select [9060_SubsId] from tmp_subsid_list);

select * into tmp_INV_SumTrans
from [10.234.252.63].Invoice.dbo.INV_SumTrans
where [9060_SubsId] in (select [9060_SubsId] from tmp_subsid_list);

delete from tmp_INV_SumTrans where SumDate < '2008-11-01';
select * from tmp_INV_SumTrans order by SumDate;
select max(INVID) from tmp_INV_SumTrans;
select min(INVID) from INV_SumTrans_1;

select top(10) * from INV_TDR_1;
select top(10) * from INV_CDR_1;

select min(Date) from INV_TDR_1;  -- 2008-10-01
select max(Date) from INV_TDR_1;  -- 2008-10-30

select min(AnswerTime) from INV_CDR_1;  -- 2008-10-01
select max(AnswerTime) from INV_CDR_1;  -- 2008-10-30

select min([9060_SubsId]) from INV_TDR_1;  -- 10
select max([9060_SubsId]) from INV_TDR_1;  -- 209602

select min([9060_SubsId]) from INV_CDR_1;  -- 10
select max([9060_SubsId]) from INV_CDR_1;  -- 209599

select * from INV_SumTrans_1 where [9060_SubsId] in (select [9060_SubsId] from tmp_INV_SumTrans);

select top(10) * from INV_SumTrans_1 where SumDate >= '2008-11-01';

select * from INV_SubsDetails
where [9060_SubsId] in (select [9060_SubsId] from tmp_subsid_list);


/*
-------------------------------------------------------------
-- check for multiple monthly fees and fix them
-------------------------------------------------------------

exec emsp_remove_multiple_monthly_fees;

select * from inv_details_1_multiple_monthly_fees;

-- get a list of subscribers that had multiple monthly fees
select I.InvoiceID, C.customer, I.FirstName, I.LastName, I.MSISDN, D.DetailName, D.TotalValue, S.RegistrationDate
from inv_details_1 D
     left join inv_subscribers_1 I on (D.InvoiceID = I.InvoiceID)
		 left join em_subscribers S on (I.subs_id = S.subs_id)
		 left join em_customers C on (I.customer_id = C.customer_id)
where DetailName like '%monthly%'
and D.InvoiceID in (select InvoiceID from inv_details_1_multiple_monthly_fees)
order by C.customer, I.MSISDN;

-- get a list of subscribers that had multiple monthly fees
select top(1000) I.InvoiceID, C.customer, I.FirstName, I.LastName, I.MSISDN, 
       D.DetailName, D.TotalValue, T.[10005_Time], S.RegistrationDate, T.*
from inv_details_1 D
     left join inv_subscribers_1 I on (D.InvoiceID = I.InvoiceID)
		 left join em_subscribers S on (I.subs_id = S.subs_id)
		 left join em_customers C on (I.customer_id = C.customer_id)
     left join Invoice..INV_TDR_1 T on (T.[9060_SubsId] = I.SubsId)
where DetailName like '%monthly%'
and D.InvoiceID in (select InvoiceID from inv_details_1_multiple_monthly_fees)
--and T.[10004_ChargeableAmount] != 0
order by I.customer_id, I.MSISDN, D.DetailName, T.[10005_Time], D.TotalValue;

*/


---------------------------------------------------------------------------------------
-- Correct monthly fees according to the table em_customer_fees.
---------------------------------------------------------------------------------------

drop table inv_details_1_monthlyfee_old;
drop table inv_details_1_monthlyfee_new;

exec emsp_add_monthly_fees;

select * from inv_details_1 where TotalValue is null and InvoiceType='subscriber';
select * from inv_details_1_monthlyfee_new where TotalValue is null and InvoiceType='subscriber';
select * from inv_details_1_monthlyfee_old where TotalValue is null and InvoiceType='subscriber';

select * from postpaid..s_productprices;

select count(*) from inv_details_1_monthlyfee_new;
select count(*) from inv_details_1_monthlyfee_old;
select top(10) * from inv_details_1_monthlyfee_new where InvoiceType='subscriber';
select top(10) * from inv_details_1_monthlyfee_old where InvoiceType='subscriber';


/*** queries to revert back the monthly fee changes (in case that needed):

-- delete the new monthly fees that were generated
delete from inv_details_1
where InvoiceType = 'subscriber' and DetailCategory='feature'
  and InvoiceID in (select InvoiceID from inv_details_1_monthlyfee_new);
  --and DetailType in (select cast(ProductId as varchar(20)) from postpaid..s_productprices);

-- restore back the original monthly fees that were deleted
insert into inv_details_1 select * from inv_details_1_monthlyfee_old;
*/

--drop table inv_details_1_monthlyfee_new;
--drop table inv_details_1_monthlyfee_old;

---------------------------------------------------------------------------------------
-- Translate monthly fee names.
---------------------------------------------------------------------------------------

-- check the file translate_products.sql


---------------------------------------------------------------------------------------
-- Add Refunds and Discounts details
---------------------------------------------------------------------------------------

select top(10) * from postpaid..em_discount_cust;
select top(10) * from postpaid..em_discount_subs;

update postpaid..em_discount_subs 
set discount_name='Zbritje nga fatura mujore'
where discount_name='Zbritja e TVSH-s� s� Shtatorit';

select count(*) from postpaid..em_discount_cust;
select count(*) from postpaid..em_discount_subs;

exec emsp_add_subs_discounts '2008-11';

select top(10) * from inv_details_1_discounts;
select count(*) from inv_details_1_discounts;

select * from inv_details_1_discounts where TotalValue >=0;
select * from inv_details_1_discounts where TotalValue is null;

select top(10) * from inv_details_1 where DetailName like 'Zbritje %';
select count(*) from inv_details_1 where DetailName like 'Zbritje %';

-- delete from inv_details_1 where DetailName like 'Zbritje %';
-- drop table inv_details_1_discounts;

------------------------------------------------------------------------------
-- Insert into the table inv_details_1 the details of the customers.
------------------------------------------------------------------------------

-- delete from inv_details_1 where InvoiceType='customer';

exec emsp_create_customer_details;

select * from inv_details_1 where InvoiceType='customer' and TotalValue is null;
select * from inv_details_1 where InvoiceType='customer' and InvoiceID is null;
select * from inv_details_1 where InvoiceType='subscriber' and InvoiceID is null;
select * from inv_details_1 where InvoiceType='subscriber' and TotalValue is null;

select * from inv_details_1 where TotalValue is null;
delete from inv_details_1 where TotalValue is null;

------------------------------------------------------------------------------
-- Update the field TotalCharge of the invoices 
------------------------------------------------------------------------------

-- calculate the TotalCharge of the subscriber invoices
update inv_subscribers_1
set TotalCharge = (select sum(TotalValue) from inv_details_1 D
                   where D.InvoiceType='subscriber' and D.InvoiceID = inv_subscribers_1.InvoiceID);
update inv_subscribers_1 set TotalCharge=0 where TotalCharge is null;

-- calculate the TotalCharge of the customer invoices
update inv_customers_1
set TotalCharge = (select sum(TotalValue) from inv_details_1 
                   where InvoiceType='customer' and InvoiceID = inv_customers_1.InvoiceID);
update inv_customers_1 set TotalCharge=0 where TotalCharge is null;

-- check for negative TotalCharge
select * from inv_subscribers_1 where TotalCharge < 0;
select * from inv_subscribers_1 where TotalCharge < -0.001;

select * from inv_details_1 
where InvoiceType='subscriber'
  and InvoiceID in (select InvoiceID from inv_subscribers_1 where TotalCharge < -0.001)
order by InvoiceID, DetailOrder;

-- check for negative TotalCharge
select * from inv_customers_1 where TotalCharge < 0;

---------------------------------------------------------------------------------------
-- Copy the invoices of the last month to the main tables of invoices.
---------------------------------------------------------------------------------------

exec emsp_copy_invoices '2008-11';

select * from inv_subscribers_err;
select * from inv_customers_err;



---------------------------------------------------------------------------------
-- Assign serial numbers to invoices
---------------------------------------------------------------------------------

use postpaid;

select top(10) * from inv_serialnumbers;
select max(SerialNumber) from inv_serialnumbers; -- 36294003

-- check the field payable of the invoices
select * from inv_customers where payable is null;
select * from inv_customers where payable!='true' and payable!='false';
select * from inv_subscribers where payable is null;
select * from inv_subscribers where payable is null and month='2008-11';
select * from inv_subscribers where payable!='true' and payable!='false';

select * from inv_subscribers where subs_id not in (select subs_id from em_subscribers);
select * from inv_subscribers 
where subs_id not in (select subs_id from em_subscribers) and month='2008-11';
select * from inv_customers where customer_id not in (select customer_id from em_customers);

/*
drop table inv_subscribers_bak;
select * into inv_subscribers_bak from inv_subscribers;

update inv_subscribers
set payable = (select PaymentResponsible from em_subscribers S 
               where S.subs_id = inv_subscribers.subs_id)
where payable is null;

update inv_customers
set payable = (select PaymentResponsible from em_customers C 
               where C.customer_id = inv_customers.customer_id)
where payable is null;
*/

exec emsp_assign_serialnumbers '2008-11', 36294003;

select top(10) * from inv_serialnumbers;
select max(SerialNumber) from inv_serialnumbers;
select min(SerialNumber) from inv_serialnumbers;

-------------------------------------------------------------------------------------
-- generate reports
-------------------------------------------------------------------------------------

exec emsp_customer_summary_report 247, '2008-11';




-------------------------------------------------------------------------------------
-- Checks that did not work so well.
-------------------------------------------------------------------------------------

use postpaid_scratch;

-- there are 1768 invoices that do not have call/sms records
select * from INV_SumTrans_1 
where INVID not in (select invID from INV_AREA_SubsInvDialed_1);

select * 
from INV_SumTrans_1 T 
     left join INV_SubsDetails D on (T.[9060_SubsId]=D.[9060_SubsId])
where INVID not in (select invID from INV_AREA_SubsInvDialed_1);

-- check for subscribers that have multiple invoice records
select * from INV_SubsDetails
where [9060_SubsId] in
  (
    select [9060_SubsId]
    from INV_SumTrans_1 
    group by SumDate, [9060_SubsId]
    having count(*) > 1
  );
-- [9060_SubsId]=74677, [9061_SubsNumber]=355672011133, Anareta Brahimi
select * from INV_SumTrans_1 where [9060_SubsId]=74677;
select * from INV_InvoiceNumbers_1 where [9060_SubsId]=74677;

