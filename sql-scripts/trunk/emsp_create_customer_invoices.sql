
-----------------------------------------------------------------
-- Create the table inv_customers_1 with the customer invoices 
-- of the last month.
-----------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_create_customer_invoices' AND type = 'P')
DROP PROCEDURE emsp_create_customer_invoices;
GO

CREATE PROCEDURE emsp_create_customer_invoices
AS
BEGIN
SET NOCOUNT ON;

-- drop the table inv_customers_1, if it exists
if exists(select table_name from information_schema.tables where table_name = 'inv_customers_1') 
drop table inv_customers_1;

create table inv_customers_1
(
  customer_id   int,
  customer_name nvarchar(200),
  InvoiceID     int,
  InvoiceDate   datetime,
  FromDate      datetime,
  ToDate        datetime,
  TotalCharge   float,
  NIPT          nvarchar(50),
  Address       nvarchar(200),
  SerialNumber  nvarchar(200),
  payable       nvarchar(10)
);

-- create customer invoices from the invoices of the subscribers
insert into inv_customers_1
  (customer_id, InvoiceID, InvoiceDate, FromDate, ToDate, TotalCharge)
select 
  customer_id, min(InvoiceID), min(InvoiceDate), min(FromDate), max(ToDate),
	sum(cast(TotalCharge as float))
from inv_subscribers_1
group by customer_id; 

-- get customer details from the table postpaid..em_customers
update inv_customers_1 set
  customer_name = (select customer from postpaid..em_customers C 
                   where C.customer_id = inv_customers_1.customer_id),
  NIPT          = (select NIPT from postpaid..em_customers C 
                   where C.customer_id = inv_customers_1.customer_id),
  Address       = (select Address from postpaid..em_customers C 
                   where C.customer_id = inv_customers_1.customer_id),
  payable       = (select PaymentResponsible from postpaid..em_customers C 
                   where C.customer_id = inv_customers_1.customer_id);

-- add a link (foreign key) from subscriber invoices to customer invoices
update inv_subscribers_1 
set CustomerInvoiceID = (select InvoiceID from inv_customers_1 C
                         where C.customer_id = inv_subscribers_1.customer_id);

END
GO
