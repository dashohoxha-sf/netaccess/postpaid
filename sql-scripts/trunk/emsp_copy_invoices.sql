﻿
use postpaid_scratch;

-------------------------------------------------------------------------------------
-- Copy the invoices of the last month from the tables inv_subscribers_1, inv_customers_1 
-- and inv_details_1, into the tables inv_subscribers, inv_customers and inv_details.
-------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects  WHERE name = 'emsp_copy_invoices' AND type = 'P')
DROP PROCEDURE emsp_copy_invoices;
GO

CREATE PROCEDURE emsp_copy_invoices
	@p_month varchar(20)
AS
BEGIN
SET NOCOUNT ON;

-------------------------- inv_subscribers ----------------------------

-- move the rows with problems into inv_subscribers_err
if exists(select table_name from information_schema.tables where table_name = 'inv_subscribers_err') 
drop table inv_subscribers_err;

select * into inv_subscribers_err
from inv_subscribers_1 
where subs_id is null or SubsId is null;

delete from inv_subscribers_1 
where subs_id is null or SubsId is null;

-- delete from postpaid..inv_subscribers any rows that might have been added in previous attempts
delete from postpaid..inv_subscribers where [month] = @p_month;

-- copy the data of the last month
insert into postpaid..inv_subscribers
  ([month], subs_id, customer_id, SubsId, InvoiceID, CustomerInvoiceID, TotalCharge,
  InvoiceDate, FromDate, ToDate, MSISDN, FirstName, LastName, Address1, Address2, City,
  COSID, PassportNr, SerialNumber, AccountID, payable)
select @p_month, * from inv_subscribers_1;

-------------------------- inv_customers ----------------------------

-- move the rows with problems into inv_customers_err
if exists(select table_name from information_schema.tables where table_name = 'inv_customers_err') 
drop table inv_customers_err;

select * into inv_customers_err from inv_customers_1 where customer_id is null;
delete from inv_customers_1 where customer_id is null;

-- delete any rows that might have been added in previous attempts
delete from postpaid..inv_customers where [month] = @p_month;

-- copy the data of the last month
insert into postpaid..inv_customers
  ([month], customer_id, customer_name, InvoiceID, InvoiceDate, FromDate, ToDate, 
   TotalCharge, NIPT, Address, SerialNumber, payable)
select @p_month, * from inv_customers_1;


-------------------------- inv_details ----------------------------

-- delete any rows that might have been added in previous attempts
delete from postpaid..inv_details where [month] = @p_month;

-- copy the data of the last month
insert into postpaid..inv_details
  ([month], InvoiceType, InvoiceID, DetailCategory, DetailType, DetailName, 
   DetailOrder, UnitCount, Usage, NetValue, VAT, TotalValue)
select @p_month, * from inv_details_1;

END
GO
