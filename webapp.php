<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Standard file for all the applications,
   * doesn't need to be modified.
   */
  /** */

  //define APP_PATH and APP_URL
$script_filename = $_SERVER["SCRIPT_FILENAME"];
$app_path =  dirname($script_filename)."/";
define("APP_PATH", $app_path);

$script_name = $_SERVER["SCRIPT_NAME"];
$app_url =  dirname($script_name);
if ($app_url!='/')  $app_url.='/';
define("APP_URL", $app_url);

//define the path and the URL of the upper folder
$up_path = ereg_replace('[^/]+/$', '', APP_PATH);
define("UP_PATH", $up_path);
$up_url = ereg_replace('[^/]+/$', '', APP_URL);
define("UP_URL", $up_url);

//define CONFIG_PATH and include the other path constants
define("CONFIG_PATH",   APP_PATH."config/");

include CONFIG_PATH."const.Paths.php";

//include configuration constants
include CONFIG_PATH."const.Options.php";
include CONFIG_PATH."const.Debug.php";

//include the WebApp framework
include WEBAPP_PATH."WebApp.php";

if (WebApp::first_time())
  {
    //first time that the user enters in the application
    $request->targetPage = FIRSTPAGE;
    $event->targetPage = FIRSTPAGE;
    if (file_exists("init.php"))  include_once "init.php";
  }

if (file_exists("global.php"))  include_once "global.php";

if ($event->target=="none")
  {
    //call the free event
    WebApp::callFreeEvent($event);
  }
?>
