<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Functions and variables that are global
   * and are used through the whole application.
   */

  /** Don't display the logo: Powered by phpWebApp */
define('hide_webapp_logo', 'true');

add_last_month();

function add_last_month()
{
  $Y = date('Y');  $m = date('m');
  $month = date('Y-m', mktime(0, 0, 0, $m-1, 1, $Y));
  WebApp::addSVar('last_month', $month);
}

/** Execute a shell command, checking it first for invalid input. */
function shell($cmd, $cmd_path =BIN)
{
  //some basic check for invalid input
  $cmd = ereg_replace(';.*', '', $cmd);
  $cmd = $cmd.' 2>&1';
  $output = shell_exec($cmd_path.$cmd);
  //WebApp::debug_msg("<xmp>$output</xmp>",
  //                  "shell(<strong>$cmd</strong>)"); //debug

  return $output;
}

function write_file($fname, &$fcontent)
{
  //open the file and write the content
  $fp = fopen($fname, 'w');
  fputs($fp, $fcontent);
  fclose($fp);
}

/** Add a new record in the table 'em_change_logs'. */
function log_event($event, $username =UNDEFINED,
		   $time =UNDEFINED, $comment =UNDEFINED,  $field =UNDEFINED,
		   $old_value =UNDEFINED, $new_value =UNDEFINED)
{
  if ($username==UNDEFINED)  $username = WebApp::getSVar('username');
  if ($time==UNDEFINED)      $time = date('Y-m-d H:i:s');
  if ($comment==UNDEFINED)
    {
      $firstname = WebApp::getSVar('firstname');
      $lastname = WebApp::getSVar('lastname');
      $comment = "Done by $firstname $lastname";
    }

  $field = ($field==UNDEFINED ? 'NULL' : "'$field'");
  $old_value = ($old_value==UNDEFINED ? 'NULL' : "'$old_value'");
  $new_value = ($new_value==UNDEFINED ? 'NULL' : "'$new_value'");

  $query = "
INSERT INTO em_change_logs
  (time, event, username, comment, field, old_value, new_value)
VALUES 
  ('$time', '$event', '$username', '$comment', $field, $old_value, $new_value)";

  WebApp::sqlQuery($query);
}

/** 
 * Log all the changes between two recordsets.
 * $old_rec and $new_rec are associated arrays with field names as keys.
 */
function log_change_event($event, $old_rec, $new_rec, $comment =UNDEFINED)
{
  $username = WebApp::getSVar('username');
  $time = date('Y-m-d H:i:s');
  if ($comment==UNDEFINED)
    {
      $firstname = WebApp::getSVar('firstname');
      $lastname = WebApp::getSVar('lastname');
      $comment = "Done by $firstname $lastname";
    }

  $arr_diffs = array();
  foreach ($new_rec as $field=>$new_value)
    {
      $old_value = $old_rec[$field];
      if ($new_value==$old_value)  continue;
      $arr_diffs[] = compact('field', 'old_value', 'new_value');
    }

  foreach ($arr_diffs as $arr)
    {
      extract($arr);
      log_event($event, $username, $time, $comment,
                $field, $old_value, $new_value);
    }  
}
?>