<?php
set_time_limit(0); //don't interrupt until it is all done

//CSS and GRAPHICS are defined relative to OUTPUT
define('CSS', '../../../css/');
define('GRAPHICS', '../../../graphics/');

//include the web application framework
include_once 'webapp.php';

$rinv = new RenderInvoices('2008-10'); 
//$rinv->generate_invoices();
$rinv->generate_envelopes(); 

exit(0);


/*---------------------------------------------------------------*/

/**
 * Create the html files of invoices, expenses and envelopes.
 */
class RenderInvoices
{
  var $tpl_invoice  = 'modules/reports/invoices/invoice.html';
  var $tpl_envelope = 'modules/reports/envelopes/envelope.html';
  var $month = '2008-10';

  /** The output directory of the html and pdf files. */
  var $html_dir;
  var $pdf_dir;

  /** The current subdirectory where the html files are output. */
  var $subdir;

  /** The number of files that are created in the current directory. */
  var $file_cnt;

  /** If a subdirectory contains more than this many files, 
   *  we should create a new one. */
  var $max_file_nr;

  /** Array of html filenames that are generated in the currect subdirectory. */
  var $arr_fnames;

  function RenderInvoices($month)
  {
    $this->tpl_invoice  = 'modules/reports/invoices/invoice.html';
    $this->tpl_envelope = 'modules/reports/envelopes/envelope.html';
    $this->month = $month;

    $this->html_dir = 'outputs/html/';
    $this->pdf_dir = 'outputs/pdf/';
    $this->max_file_nr = 500;
    $this->file_cnt = 0;
    $this->subdir = 'A001';
    $this->arr_fnames = array();
  }

  function generate_invoices()
  {
    global $event;
    $event->target = 'invoice';
    $event->name = 'view';

    $this->subdir = 'invoices/A001';
    mkdir($this->html_dir.$this->subdir);
    mkdir($this->pdf_dir.$this->subdir);
    $this->file_cnt = 0;

    //generate the invoices of all the customers and their subscribers
    $this->generate_customer_invoices();

    //create the invoices for the single subscribers
    //$this->generate_subscriber_invoices();

    //write the batch that converts the files to PDF
    //$this->write_batch_file();
  }

  function generate_envelopes()
  {
    global $event;
    $event->target = 'envelope';
    $event->name = 'view';

    $this->subdir = 'envelopes/B001';
    mkdir($this->html_dir.$this->subdir);
    mkdir($this->pdf_dir.$this->subdir);
    $this->file_cnt = 0;

    //generate the envelopes of the customers and subscribers
    $this->generate_customer_envelopes();

    //write the batch that converts the files to PDF
    //$this->write_batch_file();
  }
  
  function write_file($fname, &$fcontent)
  {
    //get the filename
    $file_name = $this->html_dir.$this->subdir.'/'.$fname;

    //open the file and write the content
    $fp = fopen($file_name, 'w');
    fputs($fp, $fcontent);
    fclose($fp);

    //increment the file counter
    $this->file_cnt++;
    $this->arr_fnames[] = $fname;
  }

  function change_dir()
  {
    //$this->write_batch_file();

    $this->subdir++;
    $this->file_cnt = 0;
    $this->arr_fnames = array();
    mkdir($this->html_dir.$this->subdir);
    mkdir($this->pdf_dir.$this->subdir);
  }

  /** Write a batch file that converts all the HTML files to PDF. */
  function write_batch_file()
  {
    $NitroPDF = '"C:\Program Files\Nitro PDF\Professional\NitroPDF.exe"';
    //$NitroPDF = "c:/Program Files/Nitro PDF/Professional/NitroPDF.exe";
    $root_dir = dirname(__FILE__);
    $html_dir = $root_dir.'/'.$this->html_dir.$this->subdir;
    $pdf_dir  = $root_dir.'/'.$this->pdf_dir.$this->subdir;

    //open the batch file for writing
    $subdir = basename($this->subdir);
    $file_name = "outputs/convert_${subdir}_files.sh";
    $fp = fopen($file_name, 'w');

    $cnt = 0;  //debug
    //write commands for each file
    for ($i=0; $i < sizeof($this->arr_fnames); $i++)
      {
	if ($cnt==0)
	  {
	    $cmd_convert = "\n$NitroPDF /CV ";
	    fputs($fp, $cmd_convert);
	  }
	$fname = $this->arr_fnames[$i];
	$html_file = $html_dir.'/'.$fname;
	$html_file = str_replace('/', '\\', $html_file);
	fputs($fp, $html_file.' ');
	$cnt++;
	if ($cnt > 30)  $cnt = 0;
      }

    //close the batch file
    fclose($fp);
  }

  function generate_customer_invoices()
  {
    global $event;
    $event->args['month'] = $this->month;

    print "<h3>Generating customer invoices </h3> \n";
    print "<ol> \n";
    $query = ( "SELECT * FROM inv_customers " .
	       "WHERE month = '$this->month' " .
	       "ORDER BY customer_name" );

    $i = 100;  //debug
    $rs = WebApp::sqlQuery($query);
    while (!$rs->EOF())
      {
	if ($i++ == 3)  break;  //debug

	//get the data of the invoice as variables
	extract($rs->Fields());

	//generate the html page of the invoice
	$event->args['module'] = 'customer';
	$event->args['InvoiceID'] = $InvoiceID;
	$html_page = WebApp::getHtmlPage($this->tpl_invoice);

	//write the html invoice to a file
	$ext = ($payable=='true' ? 'invoice' : 'expenses');
	$fname = "${InvoiceID}_000000000000_${InvoiceID}.${ext}.html";
	$this->write_file($fname, $html_page);

	//output a notification line
	print ("  <li> ($InvoiceID, $customer_id) $customer_name <br/> \n"
	       . " <strong>$fname</strong> </li>");
	flush();

	//create the invoices of each subscriber of this customer
	$this->generate_subscriber_invoices($customer_id);

	//write also an end-page
	$end_page = "<h1>-----</h1><h1>END of $customer_name</h1><h1>-----</h1>";
	$fname_1 = "${InvoiceID}_zzzzzzzzzzzzzzzzzzzzzz_end.${ext}.html";
	$this->write_file($fname_1, $end_page);

	//if there are too many files, change the output subdirectory
	if ($this->file_cnt > $this->max_file_nr)  $this->change_dir();

	//get the next record
	$rs->MoveNext();
      }

    print "</ol> \n\n";
  }

  function generate_subscriber_invoices($customer_id =UNDEFINED)
  {
    global $event;
    $event->args['month'] = $this->month;

    print "<h4>Generating subscriber invoices for customer_id=$customer_id</h4>\n";
    print "<ol> \n";
    $query = "SELECT * FROM inv_subscribers 
	      WHERE customer_id = '$customer_id'
		AND month = '$this->month'
		AND len(MSISDN) > 5
	      ORDER BY MSISDN, subs_id";

    $rs = WebApp::sqlQuery($query);
    $i = 100;  //debug
    while (!$rs->EOF())
      {
	if ($i++ == 2)  break;  //debug

	//get the data of the invoice as variables
	extract($rs->Fields());

	//generate the html page of the invoice
	$event->args['module'] = 'subscriber';
	$event->args['InvoiceID'] = $InvoiceID;
	$html_page = WebApp::getHtmlPage($this->tpl_invoice);

	//write the html invoice to a file
	if (trim($CustomerInvoiceID)=='')  $CustomerInvoiceID = 'S';
	$ext = ($payable=='true' ? '1.invoice' : '1.expenses');
	$fname = "${CustomerInvoiceID}_${MSISDN}_${InvoiceID}.${ext}.html";
	$this->write_file($fname, $html_page);

	//output a notification line
	print ("  <li> ($InvoiceID, $subs_id) $MSISDN $customer_name $FirstName $LastName<br/> \n"
	       . " <strong>$fname</strong> </li>");
	flush();

	//if there are too many files, change the output subdirectory
	if ($customer_id=='0' and ($this->file_cnt > $this->max_file_nr))
	  $this->change_dir();

	//get the next record
	$rs->MoveNext();
      }

    print "</ol> \n <br/><br/> \n";
  }

  function generate_customer_envelopes()
  {
    global $event;
    $event->args['month'] = $this->month;

    print "<h3>Generating customer envelopes </h3> \n";
    print "<ol> \n";
    $query = ( "SELECT * FROM inv_customers "
	       . " WHERE month = '$this->month' "
	       . " ORDER BY customer_name");

    $rs = WebApp::sqlQuery($query);
    $i = 100;  //debug
    while (!$rs->EOF())
      {
	if ($i++ == 20)  break;  //debug

	//get the data of the invoice as variables
	extract($rs->Fields());

	//generate the html page of the invoice
	$event->args['module'] = 'customer';
	$event->args['id'] = $customer_id;
	$html_page = WebApp::getHtmlPage($this->tpl_envelope);

	//write the html invoice to a file
	$ext = 'envelope.C4';
	$fname = "${InvoiceID}_000000000000_${InvoiceID}.$ext.html";
	$this->write_file($fname, $html_page);

	//output a notification line
	print ("  <li> ($InvoiceID) $customer_name <br/> \n"
	       . " <strong>$fname</strong> </li>");
	flush();

	//create the envelopes of each subscriber of this customer
	$this->generate_subscriber_envelopes($customer_id);

	//if there are too many files, change the output subdirectory
	if ($customer_id=='0' and $this->file_cnt > $this->max_file_nr)
	  $this->change_dir();

	//get the next record
	$rs->MoveNext();
      }

    print "</ol> \n\n";
  }

  function generate_subscriber_envelopes($customer_id)
  {
    global $event;
    $event->args['month'] = $this->month;

    print "<h4>Generating subscriber envelopes</h4> \n";
    print "<ul> \n";
    $query = ("SELECT * FROM inv_subscribers "
	      . " WHERE month = '$this->month' "
	      . " AND customer_id='$customer_id' "
	      . " AND len(MSISDN) > 5 "
	      . " ORDER BY MSISDN, subs_id");

    $rs = WebApp::sqlQuery($query);
    $i = 100;  //debug
    while (!$rs->EOF())
      {
	if ($i++ == 20)  break;  //debug

	//get the data of the invoice as variables
	extract($rs->Fields());

	//generate the html page of the invoice
	$event->args['module'] = 'subscriber';
	$event->args['id'] = $subs_id;
	$html_page = WebApp::getHtmlPage($this->tpl_envelope);

	//write the html invoice to a file
	if (trim($CustomerInvoiceID)=='')  $CustomerInvoiceID = 'S';
	$ext = 'envelope.C4';
	$fname = "${CustomerInvoiceID}_${MSISDN}_${InvoiceID}.${ext}.html";

	if ($payable=='true')
	  {
	    $this->write_file($fname, $html_page);
	  }
	else
	  {
	    $this->file_cnt++;
	  }

	//output a notification line
	print ("  <li> ($InvoiceID, $subs_id) $MSISDN $FirstName $LastName<br/> \n"
	       . " <strong>$fname</strong> </li>");
	flush();

	//if there are too many files, change the output subdirectory
	if ($this->file_cnt > $this->max_file_nr)  $this->change_dir();

	//get the next record
	$rs->MoveNext();
      }

    print "</ul> \n<br /><br />\n";
  }
}
?>