#!/bin/bash
### set the superuser password

### go to this directory
cd $(dirname $0)

### get the password
echo "Modifying the superuser password... "
echo -n "Enter the superuser password: "
read su_passwd

### encript the password and save it
openssl passwd -1 $su_passwd > ../.su/supasswd

