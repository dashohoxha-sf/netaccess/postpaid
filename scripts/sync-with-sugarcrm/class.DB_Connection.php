<?php
require_once 'MDB2.php';

  /** */
class DB_Connection
{
  /** The connection to the DB. */
  var $mdb2;

  function __construct($dsn)
  {
    $mdb2_options = array(
			  'debug'       => 2,
			  'portability' => MDB2_PORTABILITY_ALL,
			  );
    $this->mdb2 =& MDB2::factory($dsn, $mdb2_options);
    if (PEAR::isError($this->mdb2))
      {
	$this->show_error($this->mdb2->getMessage());
      }
    else
      {
	$this->mdb2->setFetchMode(MDB2_FETCHMODE_ASSOC);
      }
  }
        
  function query($query)
  {
    $result = $this->mdb2->query($query);
    if (PEAR::isError($result))
      {
	$this->show_error($result->getMessage(), $query);
	return false;
      }
                
    return $result;
  }

  function exec($query)
  {
    $result = $this->mdb2->exec($query);
    if (PEAR::isError($result))
      {
	$this->show_error($result->getMessage(), $query);
	return false;
      }

    return $result;
  }

  function show_error($err_msg, $query ='')
  {
    $err_msg = htmlentities($err_msg);

    if (trim($query) != '')
      {
	$query = '<strong>Query:</strong>'.htmlentities($query);
      }

    $error_html = "<div class='db_error'> <strong>MySQL Error:</strong>
$err_msg $query</div>
";
    print $error_html;
  }
}
?>