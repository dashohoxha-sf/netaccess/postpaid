<?php
require_once 'config.php';
require_once 'class.DB_Connection.php';

/** Transfer data between the SugarCRM DB and the Invoice DB. */
class Transfer
{
  var $sugar_db;
  var $invoice_db;

  function __construct()
  {
    $this->sugar_db = new DB_Connection(SUGAR_DSN);
    $this->invoice_db = new DB_Connection(INVOICE_DSN);
  }

  /**
   * Cet the customers from the sugar DB.
   * It is done in steps in order to avoid RAM limitation problems.
   */
  function get_subscribers_from_eaglecrm()
  {
    //empty the destination table
    $this->invoice_db->exec("truncate table EM_eaglecrm_subscribers");

    print "Getting eaglecrm subscribers: ";
    $offset = 0;
    $n = 20;
    try
      { 
        do
          {
            $m = $this->transfer_n_rows_from_sugar_to_invoice($offset, $n);
            print '* ';  flush();   //return; //debug
            $offset += $n;
          }
        while ($m==$n);
      }
    catch (Exception $e)
      {
        trigger_error('get_customers_from_sugar(): ' . $e->getMessage(),
                      E_USER_ERROR);
      }
    print " done\n";
  }


  /**
   * Transfer $n rows starting from the given $offset.
   * Return the number of rows that were actually processed.
   */
  function transfer_n_rows_from_sugar_to_invoice($offset, $n)
  {
    $query = "SELECT first_name, last_name, primary_address_street,
                     primary_address_city, msisdn_c, cos_c, postpaid_c,
                     status_c, passport_nr_c, C.name AS customer,
                     nipt_c, nr_of_lines_c, 
                     C1.payment_accountable_c AS bill_customer
              FROM em_subscribers S
                   LEFT JOIN em_subscribers_cstm S1 ON (S.id = S1.id_c)
		   LEFT JOIN em_customer_subscribers_c CS
                        ON (S.id = CS.em_customerubscribers_idb)
		   LEFT JOIN em_customers C 
                        ON (C.id = CS.em_customer_customers_ida)
		   LEFT JOIN em_customers_cstm C1 ON (C.id = C1.id_c)";

    //get $n rows starting from $offset
    $this->sugar_db->mdb2->setLimit($n, $offset);
    $result = $this->sugar_db->query($query);
    $arr_result = $result->fetchAll();
    $nr_rows = sizeof($arr_result);

    if ($nr_rows==0)  return 0;

    //construct an insert query
    $query_template =
      "INSERT INTO EM_eaglecrm_subscribers
	   (first_name, last_name, primary_address_street,
	    primary_address_city, msisdn_c, cos_c, postpaid_c,
	    status_c, passport_nr_c, customer,
	    nipt_c, nr_of_lines_c, 
	    bill_customer)
       VALUES
	   (:first_name, :last_name, :primary_address_street,
	    :primary_address_city, :msisdn_c, :cos_c, :postpaid_c,
	    :status_c, :passport_nr_c, :customer,
	    :nipt_c, :nr_of_lines_c, 
	    :bill_customer)";
    $query = $this->invoice_db->mdb2->prepare($query_template);

    //insert all the results in the invoice database
    for ($i=0; $i<$nr_rows; $i++)  $query->execute($arr_result[$i]);

    //return the number of the rows processed
    return $nr_rows;
  }


  /**
   * Copy the data of the given users from radius to sugar.
   * If the second parameter is true, then the given users are
   * actually created in sugar (instead of being updated with data
   * from radius).
   */
  /*
  function update_from_radius_to_sugar($arr_usernames, $create =false)
  {
    print "Update from radius to sugar: ";
    $step = 20;
    for ($offset=0; $offset < sizeof($arr_usernames); $offset+=$step)
      {
        $arr_u = array_slice($arr_usernames, $offset, $step);
        $this->updatestep_from_radius_to_sugar($arr_u, $create);
        print '*';
      }
    print " done\n";
  }

  function updatestep_from_radius_to_sugar($arr_usernames, $create =false)
  {
    //get the radius clients
    $client_list = "'" . implode("', '", $arr_usernames) . "'";
    $query = "SELECT s.id, 
                     r.username         AS username_c,
                     r.password         AS password_c,
                     r.service          AS service_c,
                     r.nr_conn          AS simultaneous_connections_c,
                     r.expiration_date  AS expiration_date_c,
                     r.mac              AS mac_address_c,
                     r.fullname         AS name,
                     r.email            AS email1
              FROM radius_clients AS r
                   LEFT JOIN sugar_clients AS s USING(username)
              WHERE r.username IN ($client_list)";
    $result = $this->sync_db->query($query);

    //convert them to an array of entries
    $entries = array();
    while ($client = $result->fetch_assoc())
      {
        $service_name = $client['service_c'];
        $service_id = $this->services[$service_name];
        $client['account_id_c'] = $service_id;

        if ($client['name']=='AlbaniaOnline' or $client['name']=='AOLSP')
          $client['name'] = $client['username_c'];

        $entries[] = $this->client_to_entry($client, $create);
      }

    //if we are creating new clients, then they must be deleted
    //from radius first, otherwise the creation will fail with
    //the error 'this username is already used in RADIUS'
    if ($create)  $this->delete_from_radius($arr_usernames);

    //update these entries in sugar
    try
      {
         $this->soap_client->set_entries($this->session_id, 'Accounts', $entries);
      }
    catch (SoapFault $fault)
      {
         $this->print_request_response();
         trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
      }

    //remove them from tables sugar_clients and radius_clients
    $this->remove_from_sync_db($arr_usernames);
  }
  */

  /** copy the data of the given users from radius to sugar */
  /*
  function update_from_sugar_to_radius($arr_usernames, $insert =false)
  {
    print "Update from sugar to radius: ";
    $step = 20;
    for ($offset=0; $offset < sizeof($arr_usernames); $offset+=$step)
      {
        $arr_u = array_slice($arr_usernames, $offset, $step);
        $this->updatestep_from_sugar_to_radius($arr_u, $insert);
        print '*';
      }
    print " done\n";
  }
  */

  /*
  function updatestep_from_sugar_to_radius($arr_usernames, $insert =false)
  {
    //get a list of the clients
    $client_list = "'" . implode("', '", $arr_usernames) . "'";

    $module_name = 'Accounts';
    $query = "accounts_cstm.username_c IN ($client_list)";
    $orderby = '';
    $offset = 0;
    $select_fields = array('id', 'username_c', 'password_c', 'service_c', 
                           'simultaneous_connections_c', 'expiration_date_c', 
                           'mac_address_c', 'name', 'email1');
    $max_results = 2;
    $deleted = false;

    $result = 
      $this->soap_client->get_entry_list($this->session_id, $module_name,
                                         $query, $orderby, $offset,
                                         $select_fields, $max_results,
                                         $deleted);

    //get the number of clients
    $nr_clients = sizeof($result->entry_list);

    //update these clients in radius
    for ($i=0; $i < $nr_clients; $i++)
      {
        $entry = $result->entry_list[$i]->name_value_list;
        $assoc_array = $this->entry_to_client($entry);
        extract($assoc_array);

        //construct a sql query
        if ($insert)
          {
            $query = "CALL user_add('$username_c',
                                    '$password_c',
                                    '$service_c',
                                    '$simultaneous_connections_c',
                                    '$expiration_date_c',
                                    '$mac_address_c',
                                    '$name',
                                    '$email1')";
            $this->radius_db->query($query);
          }
        else
          {
            $query = "CALL user_update('$username_c',
                                       '$service_c',
                                       '$simultaneous_connections_c',
                                       '$expiration_date_c',
                                       '$mac_address_c',
                                       '$name',
                                       '$email1')";
            $this->radius_db->query($query);
            $query = "CALL user_set_password('$username_c', '$password_c')";
            $this->radius_db->query($query);
          }
      }

    //remove them from tables sugar_clients and radius_clients
    $this->remove_from_sync_db($arr_usernames);
  }
  */

  /*
  function delete_from_sugar($arr_usernames)
  {
    //get the id of each client
    $client_list = "'" . implode("', '", $arr_usernames) . "'";
    $query = ( "SELECT id, '1' AS deleted FROM sugar_clients "
               . "WHERE username IN ($client_list)" );
    $result = $this->sync_db->query($query);

    //convert them to an array of entries
    $entries = array();
    while ($client = $result->fetch_assoc())
      {
        $entries[] = $this->client_to_entry($client, $create);
      }
    //update these entries in sugar
    $this->soap_client->set_entries($this->session_id, 'Accounts', $entries);

    //remove them from table sugar_clients in sync_db
    $query = "DELETE FROM sugar_clients WHERE username IN ($client_list)";
    $this->sync_db->query($query);
  }
  */

  /** convert an associative array to a client soap entry */
  function client_to_entry($assoc_array, $create =false)
  {
    $entry = array();
    while (list($name,$value) = each($assoc_array))
      {
        //in order to create a new entry in sugar,
        //the field id must not be set
        if ($create and $name=='id')  continue;

        $entry[] = compact('name', 'value');
      }


    return $entry;
  }

  /** convert a soap entry to an associative array with the client data */
  function entry_to_client($entry)
  {
    $client = array();
    for ($i=0; $i < sizeof($entry); $i++)
      {
        $client[$entry[$i]->name] = $entry[$i]->value;
      }
    return $client;
  }

  /** print debug output */
  function print_request_response()
  {
    print '<xmp>';

    print "REQUEST HEADERS: \n";
    print_r($this->soap_client->__getLastRequestHeaders());

    print "\nREQUEST: \n";
    $request = $this->soap_client->__getLastRequest();
    $request = str_replace('<', "\n<", $request);
    $request = str_replace("\n</", "</", $request);
    $request = str_replace('xmlns', "\n    xmlns", $request);
    print_r($request);
    print "\n\nRESPONSE HEADERS: \n";
    print_r($this->soap_client->__getLastResponseHeaders());

    print "\nRESPONSE: \n";
    $response = $this->soap_client->__getLastResponse();
    $response = str_replace('<', "\n<", $response);
    $response = str_replace("\n</", "</", $response);
    $response = str_replace('xmlns', "\n    xmlns", $response);
    print_r($response);

    print '</xmp>';
  }

  function get_module_fields($module)
  {
    $res = $this->soap_client->get_module_fields($this->session_id, $module);
    print_r($res);
  }
}
?>
