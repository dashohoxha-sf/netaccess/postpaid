<?php
set_time_limit(0); //don't interrupt until it is all done

//include the web application framework
include_once 'webapp.php';

$month = '2008-10';
$query = ( "SELECT InvoiceID, MSISDN, CustomerInvoiceID "
	   . " FROM inv_subscribers where month='2008-10'");
$rs = WebApp::sqlQuery($query);

$arr_subs = array();
while (!$rs->EOF())
  {
    $msisdn = $rs->Field('MSISDN');
    $arr_subs[$msisdn] = $rs->Fields();
    $rs->MoveNext();
  }

$dir = "K:\Invoices\October-2008\CallDetails";
exec("dir /b /l $dir", $arr_fnames);

$batch_file = "$dir\\00_rename.bat";
$fp = fopen($batch_file, 'w');

print '<xmp>';
for ($i=0; $i < sizeof($arr_fnames); $i++)
  {
    $fname = $arr_fnames[$i];

    $fname1 = str_replace('.pdf', '', $fname);
    $arr = explode('_', $fname1);
    $msisdn = $arr[0];
    $invoice_id = $arr[1];

    if (strlen($msisdn) != 12)
      {
	print "* MSISDN not correct: $fname\n";
	continue;
      }

    if (!isset($arr_subs[$msisdn]))
      {
	print "* MSISDN does not exist: $fname\n";
	continue;
      }

    $InvoiceID = $arr_subs[$msisdn]['InvoiceID'];
    if ($InvoiceID!=$invoice_id)
      {
	print "* InvoiceID does not match: $fname\n";
	continue;
      }

    $CustomerInvoiceID = $arr_subs[$msisdn]['CustomerInvoiceID'];
    if (trim($CustomerInvoiceID)=='')  $CustomerInvoiceID = 'S';

    $new_fname = "${CustomerInvoiceID}_${msisdn}_${InvoiceID}.2.calldetails.pdf";
    $mv_cmd = "move $fname $new_fname\n";
    print $mv_cmd;
    fputs($fp, $mv_cmd);
  }
print '</xmp>';
fclose($fp);
?>