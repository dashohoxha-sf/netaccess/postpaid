<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * The constants defined in this file change the behaviour of the 
   * framework and the application. You can change the values of the 
   * constants according to the instructions given in comments, or add 
   * new constants that you use in your application.
   */


  /** Enable or disable automatic printing of envoices, envelopes, etc. */
define('ENABLE_PRINTING', 'no');

  /**
   * This is the first page of the application. The framework looks
   * for it at the template folder (specified by TPL). 
   */
define("FIRSTPAGE", "main/main.html");

/**
 * Define the Data Source Name that is used by MDB2 to connect
 * to the database, and the MDB2 options for the default connection.
 */
define('MDB2_DSN', "mssql://cc:eagle_cc@10.1.21.201:1433/postpaid?new_link=true");
//define('MDB2_DSN', "mssql://cc:eaglecc@127.0.0.1/postpaid?new_link=true");
//define('MDB2_DSN', "mssql://sa:eagle@10.234.252.121/Invoice08?new_link=true");
//define('MDB2_DSN', "mysqli://root@127.0.0.1/postpaid?new_link=true");

$MDB2_OPTIONS = array(
		      'debug'       => 2,
		      'portability' => MDB2_PORTABILITY_ALL,
		      );

/**
 * Define the DSN and options of the connection that is used
 * by web_app to connect to the database. If they are not defined
 * then the default ones are used.
 */
define('WEBAPP_MDB2_DSN', "mysqli://root@127.0.0.1/webapp?new_link=true");
$WEBAPP_MDB2_OPTIONS = 
  array(
	'debug'       => 2,
	'portability' => MDB2_PORTABILITY_ALL,
	);

/** The DSN of the connection that is used to access the database Magnolia */
define('MAGNOLIA_MDB2_DSN', "mssql://magnolia:hcfxku2111@10.234.252.63/Magnolia?new_link=true");
//define('MAGNOLIA_MDB2_DSN', "mssql://magnolia:hcfxku2111@10.234.252.53/Magnolia?new_link=true");

/**
 * This constant is the value returned by the framework 
 * for a DB variable that has a NULL value. It can be
 * "", "NULL", NULL, etc.
 */
define("NULL_VALUE", "");

/**
 * This constant sets the format of the error message that is displayed
 * when a {{variable}} is not found. 'var_name' is replaced by
 * the actual variable name. Examples: "'var_name' is not defined",
 * "", "undefined", etc. It cannot contain "{{var_name}}" inside.
 */
define("VAR_NOT_FOUND", "{var_name}");

/**
 * When this constant is true, then the CGI vars are displayed
 * at the URL window of the browser. See also SHOW_EXTERNAL_LINK
 * at const.Debug.php.
 */
define("DISPLAY_CGI_VARS", false);

/**
 * The constants LNG and CODESET set a default language and codeset for
 * the application. They are used for the localization (translation)
 * of the messages. They can be changed by calling:
 *    $l10n->set_lng($lng, $codeset)
 * where $l10n is a global variable and $codeset is optional.
 * LNG can be something like 'en_US' or 'en' or UNDEFINED.
 * CODESET can be UNDEFINED, 'iso-latin-1', etc.
 */
//define('LNG', 'sq_AL');
define('LNG', 'en');
define('CODESET', 'iso-8859-1');

/** if true, then use the php-gettext instead of GNU gettext */
define('USE_PHP_GETTEXT', true);

//etc.
?>