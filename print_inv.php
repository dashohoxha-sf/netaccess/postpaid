<?php
set_time_limit(0); //don't interrupt until it is all done

//CSS and GRAPHICS are defined relative to OUTPUT
define('CSS', '../../../css/');
define('GRAPHICS', '../../../graphics/');

include_once 'webapp.php';
include_once 'authenticate.php';

//construct the target page of the transition
$tpl_page = TPL.$event->targetPage;
WebApp::constructHtmlPage($tpl_page);
?>