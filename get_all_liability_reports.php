<?php
set_time_limit(0); //don't interrupt until it is all done

//include the web application framework
include_once 'webapp.php';

//include the excel writer
require_once "Spreadsheet/Excel/Writer.php";

//create a workbook
//$xls =& new Spreadsheet_Excel_Writer();


get_all_the_reports();

exit;


function get_all_the_reports()
{
  $query = ("SELECT customer_id, customer_name FROM inv_customers "
	    . " WHERE payable='false' "
	    . " GROUP BY customer_id, customer_name "
	    . " ORDER BY customer_name");
  $rs = WebApp::sqlQuery($query);

  mkdir('liability_reports');

  print "<ol>\n";
  while (!$rs->EOF())
    {
      $customer_id = $rs->Field('customer_id');
      $customer_name = $rs->Field('customer_name');

      print "<li> <strong>($customer_id)</strong> $customer_name </li>\n";
      flush();
      get_customer_report($customer_id, $customer_name);

      $rs->MoveNext();
    }
  print "</ol>\n";
}


function get_customer_report($customer_id, $customer_name)
{
  //get the report data
  $query = "exec emsp_customer_liability_report $customer_id;";
  $rs = WebApp::sqlQuery($query);

  //get an array of months by removing the first columns
  //(SubsId, MSISDN, FirstName, LastName) and the last column (Total)
  $arr_months = array_keys($rs->Fields());
  $arr_months = array_slice($arr_months, 4);
  array_pop($arr_months);
  $nr_months = sizeof($arr_months);

  //create a workbook
  $xls =& new Spreadsheet_Excel_Writer("liability_reports/${customer_name}.xls");
  if (PEAR::isError($xls))
    {
      print $xls->getMessage();
      exit;
    }

  $worksheet = substr($customer_name, 0, 30);
  $rpt =& $xls->addWorksheet($worksheet);
  if (PEAR::isError($rpt))
    {
      print $rpt->getMessage();
      exit;
    }

  //add the title to the top left cell of the worksheet
  $format =& $xls->addFormat();
  $format->setFontFamily('Helvetica');
  $format->setBold();
  $format->setSize('12');
  $format->setColor('navy');
  $format->setAlign('merge');
  $rpt->write(0, 0, $customer_name, $format);
  //merge with empty cells
  for ($i=0; $i < $nr_months+3; $i++)  $arr[] = '';
  $rpt->writeRow(0, 1, $arr, $format);

  $rpt->setRow(0, 20);  //set row height
  $rpt->setColumn(0, 0, 7);  //set column width for the first col
  $rpt->setColumn(1, 1, 15);  //set width for the second column
  $rpt->setColumn(2, 2, 25);  //set width for the third column
  $rpt->setColumn(3, 3+$nr_months, 12);

  //add column headings
  $format =& $xls->addFormat();
  $format->setBold();
  $format->setFontFamily('Helvetica');
  $format->setBold();
  $format->setSize('11');
  $format->setAlign('center');
  $colNames = $arr_months;
  array_unshift($colNames, 'SubsId', T_("Phone Number"), T_("Name Surname"));
  array_push($colNames, T_("Total"));
  $rpt->writeRow(1, 0, $colNames, $format);
  $rpt->setRow(1, 15, $format);

  //define some formats
  $msisdn_fmt =& $xls->addFormat();
  $msisdn_fmt->setHAlign('center');

  $value_fmt =& $xls->addFormat();
  $value_fmt->setNumFormat('0.00');

  //freeze the top 2 rows
  $freeze = array(2,0,2,0);
  $rpt->freezePanes($freeze);

  //keep track of the current excel row number
  $row_nr = 2;

  //loop through the data, adding it to the sheet
  while (!$rs->EOF())
    {
      $data_row = $rs->Fields();
      $rpt->writeNumber($row_nr, 0, $data_row['SubsId']);
      $rpt->writeString($row_nr, 1, $data_row['MSISDN'], $msisdn_fmt);
      $name = $data_row['FirstName'].' '.$data_row['LastName'];
      $rpt->writeString($row_nr, 2, $name);

      for ($i=0; $i < $nr_months; $i++)
	{
	  $month = $arr_months[$i];
	  $rpt->writeNumber($row_nr, 3+$i, $data_row[$month], $value_fmt);
	}

      //write the sum for each subscriber
      $cell1 = Spreadsheet_Excel_Writer::rowcolToCell($row_nr, 3);
      $cell2 = Spreadsheet_Excel_Writer::rowcolToCell($row_nr, $nr_months+2);
      $formula = "=SUM($cell1:$cell2)";
      $rpt->writeFormula($row_nr, $nr_months+3, $formula, $value_fmt);

      //set row height
      $rpt->setRow($row_nr, 15);

      //move to the next row
      $row_nr++;
      $rs->MoveNext();
    }

  //the format of the total cells
  $total_fmt =& $xls->addFormat();
  $total_fmt->setNumFormat('0.00');
  $total_fmt->setFontFamily('Helvetica');
  $total_fmt->setBold();
  $total_fmt->setTop(1); // Top border

  $rpt->writeString($row_nr, 2, T_("Total:"), $total_fmt);

  //add the totals for each column
  for ($i=0; $i <= $nr_months; $i++)
    {
      $cell1 = Spreadsheet_Excel_Writer::rowcolToCell(2, 3+$i);
      $cell2 = Spreadsheet_Excel_Writer::rowcolToCell($row_nr-1, 3+$i);
      $formula = "=SUM($cell1:$cell2)";
      $rpt->writeFormula($row_nr, 3+$i, $formula, $total_fmt);
    }

  //set row height and format
  $rpt->setRow($row_nr, 15, $total_fmt);

  //close the report
  $xls->close();
}

?>