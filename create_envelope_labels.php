<?php
//include the web application framework
include_once 'webapp.php';

//don't interrupt until it is all done
set_time_limit(0);

$month = '2008-10';
$env_dir = "k:/Invoices/2008-10/pdf/envelopes";
$label_path = dirname(__FILE__).'/outputs/labels';
$tpl_labels = 'modules/reports/labels/labels.html';
$file_cnt = '_000';

shell("mkdir -p $label_path");

$arr_subscribers = array();
$arr_customers = array();
$arr_addresses = array();
$rs_labels = new EditableRS('labels');


//get the customer and subscriber data from the database
get_data_from_db($month);

$output = shell("ls $env_dir");
$arr_subdirs = explode("\n", $output);
for ($i=0; $i < sizeof($arr_subdirs); $i++)
  {
    $subdir = $arr_subdirs[$i];
    if ($subdir=='')  continue;
    if ($subdir[0]!='A')  continue;

    //print $subdir." -\n";  //debug
    //if ($subdir=='A002')  break;  //debug

    shell("mkdir -p $label_path/$subdir");
    process_subdir($subdir);
  }

/* ------------------------------------------------------------------------- */

function get_data_from_db($month)
{
  global $arr_customers, $arr_subscribers;

  $query = ( "SELECT InvoiceID, MSISDN, FirstName, LastName, Address1, Address2, City "
	     . " FROM inv_subscribers where month='$month' and payable='true'");
  $rs = WebApp::sqlQuery($query);
  while (!$rs->EOF())
    {
      $InvoiceID = $rs->Field('InvoiceID');
      $arr_subscribers[$InvoiceID] = $rs->Fields();
      $rs->MoveNext();
    }

  $query = ( "SELECT InvoiceID, customer_name, Address  "
	     . " FROM inv_customers where month='$month'");
  $rs = WebApp::sqlQuery($query);
  while (!$rs->EOF())
    {
      $InvoiceID = $rs->Field('InvoiceID');
      $arr_customers[$InvoiceID] = $rs->Fields();
      $rs->MoveNext();
    }
}

function process_subdir($subdir)
{
  global $arr_customers, $arr_subscribers, $env_dir;

  $output = shell("ls $env_dir/$subdir");
  $arr_files = explode("\n", $output);
  for ($j=0; $j < sizeof($arr_files); $j++)
    {
      $fname = $arr_files[$j];
      if ($fname=='')  continue; 

      $fname = ereg_replace('\..*', '', $fname);
      list($CustomerInvoiceID, $MSISDN, $InvoiceID) = explode('_', $fname);

      if ($MSISDN=='000000000000')  //customer
	{
	  if (!isset($arr_customers[$InvoiceID]))
	    {
	      print "$InvoiceID $MSISDN NOT FOUND\n";
	    }
	  else
	    {
	      $customer = $arr_customers[$InvoiceID];
	      extract($customer);
	      $address = "$customer_name<br />\n$Address";
	      add_address($address);
	    }
	}
      else  //subscriber
	{
	  if (!isset($arr_subscribers[$InvoiceID]))  
	    {
	      print "$InvoiceID $MSISDN NOT FOUND\n";
	    }
	  else
	    {
	      $subscriber = $arr_subscribers[$InvoiceID];
	      extract($subscriber);
	      $address = "$FirstName $LastName<br />\n$Address1 $Address2<br />\n$City";
	      add_address($address);
	    }
	}
    }
}

function add_address($address)
{
  global $arr_addresses;

  $arr_addresses[] = $address;
  if (sizeof($arr_addresses)==16)
    {
      print_addresses();
      $arr_addresses = array();
    }
}

function print_addresses()
{
  global $arr_addresses, $label_path, $subdir, $file_cnt, $tpl_labels, $rs_labels;

  //put the between labels into a recordset with two columns
  $rs_labels = new EditableRS('labels');
  for ($i=0; $i < sizeof($arr_addresses); $i+=2)
    {
      $label1 = $arr_addresses[$i];
      $label2 = $arr_addresses[$i+1];
      $rs_labels->addRec(compact('label1', 'label2'));
    }

  $file_cnt++;
  $file_name = "$label_path/$subdir/labels${file_cnt}.html";
  $html_page = WebApp::getHtmlPage($tpl_labels);
  write_file($file_name, $html_page);

  $file_name = str_replace('\\', '/', $file_name);
  print "<br /> <a href='file:///$file_name'><strong>$file_name</strong></a>\n";
  print $rs_labels->toHtmlTable('no-headers');  //debug
}
?>
