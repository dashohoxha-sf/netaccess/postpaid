<?php
set_time_limit(0); //don't interrupt until it is all done

//include the web application framework
include_once 'webapp.php';

$rs_fees = WebApp::sqlQuery("select * from em_customer_fees");
//print $rs_fees->toHtmlTable();  //debug

$rs_products = WebApp::sqlQuery("select * from s_productprices");
//print $rs_products->toHtmlTable();  //debug

print '<xmp>';
while (!$rs_fees->EOF())
  {
    extract($rs_fees->Fields());
    //print "\n$customer_id, $cos_id, $product_id";  //debug
    change_monthly_fee($customer_id, $cos_id, $product_id);
    $rs_fees->MoveNext();
  }
print '</xmp>';


function change_monthly_fee($customer_id, $cos_id, $product_id)
{
  global $rs_products;

  //get the variables $ProductID, ProductName, $Price 
  //for the given monthly fee
  $rs_products->find("ProductId='$product_id'");
  extract($rs_products->Fields());  

  $query_1 = "
insert into inv_details_1_patch
select 'subscriber' as InvoiceType, D.InvoiceID, 'feature' as DetailCategory, 
       '$ProductId' as DetailType, '$ProductName' as DetailName,
       20 as DetailOrder, 1 as UnitCount, 1 as Usage,
       5.0 * (D.TotalValue * $Price) / P.Price / 6.0 as NetValue, 
       1.0 * (D.TotalValue * $Price) / P.Price / 6.0 as VAT, 
       (TotalValue * $Price) / Price as TotalValue
from inv_details_1 D
     left join inv_subscribers_1 S on (D.InvoiceID = S.InvoiceID)
     left join s_productprices P on (D.DetailName = P.ProductName)
where S.customer_id = '$customer_id' and S.COSID='$cos_id' 
  and D.DetailName like '%monthly%';
  ";

  $query_2 = "
delete from inv_details_1
where DetailName like '%monthly%'
  and InvoiceID in (select I.InvoiceID
                    from inv_details D
                         left join inv_subscribers_1 S on (D.InvoiceID = S.InvoiceID)
                    where S.customer_id = '$customer_id' and S.COSID='$cos_id'
                    and D.DetailName like '%monthly%');
";

  print $query_1;
  //print $query_2;
}

?>
