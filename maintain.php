#!/usr/bin/php -q
<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

   /**
    * This script is called periodically by cron (each weak or each month)
    * and it does some tasks like sending logs by email, updating, etc.
    */

if ($argc != 3)
  {
    print "Usage: $argv[0] period email
where 'period' is in days
and 'email' is the e-mail address that will get the message
";
    exit(1);
  }
$period = $argv[1];
$email_addr = $argv[2];

define("APP_PATH", dirname(__FILE__).'/');
define("APP_URL", './');
include_once "webapp.php";

//email_logs($period, $email_addr);
empty_session();

exit(0);

/*------------------------ functions -----------------------------*/

/**
 * Get the logs of the last period and send them by email.
 */
function email_logs($period, $email_addr)
{
  //get a list of the last logs
  $query = "SELECT * FROM logs "
    . "WHERE time > DATE_SUB(NOW(), INTERVAL $period DAY)";
  $rs = WebApp::sqlQuery($query);

  //create the list of logs
  while (!$rs->EOF())
    {
      extract($rs->Fields());
      $log_list .= "$id) ($time) $event: $details\n";
      $rs->MoveNext();
    }

  //get ISP details
  $query = "SELECT firstname, e_mail, phone1, address "
    . "FROM users WHERE username = 'ISP'";
  $rs = WebApp::sqlQuery($query);
  extract($rs->Fields());

  //get details like $nr_macs, $clients, $active_clients, etc.
  $arr_stats = get_stats();
  extract($arr_stats);

  //send a message
  $date = date('Y-m-d H:i');
  $msg_body = "
ISP   : $firstname
Email : $e_mail
Phone : $phone1
Address:
$address

Currently there are $nr_macs computers having internet connection.
There are $nr_clients clients and $nr_active_clients active (unexpired) clients.

List of The Logs of The Last Period ($period days), Up To $date :

$log_list
";
  $subject = "[logs] $date $firstname";
  mail($email_addr, $subject, $msg_body);
}

/**
 * Get some statistics that are included in the email of logs.
 * Returns an associated array with the details.
 */
function get_stats()
{
  //get the number of connected computers
  $rs = WebApp::sqlQuery('SELECT COUNT(mac) AS nr_macs FROM allowed_macs');
  $nr_macs = $rs->Field('nr_macs');

  //get the number of clients
  $rs = WebApp::sqlQuery('SELECT COUNT(username) AS nr_clients FROM clients');
  $nr_clients = $rs->Field('nr_clients');

  //get the number of active (unexpired) clients
  $query = ("SELECT COUNT(username) AS nr_active_clients FROM clients "
            . "WHERE expiration_time > NOW()");
  $rs = WebApp::sqlQuery($query);
  $nr_active_clients = $rs->Field('nr_active_clients');

  return compact('nr_macs', 'nr_clients', 'nr_active_clients');
}

/** empty the table session */
function empty_session()
{
  $query = "DELETE FROM session ";
  $rs = WebApp::sqlExec($query);
}
?>