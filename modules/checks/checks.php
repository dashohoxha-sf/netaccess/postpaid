<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

class checks extends WebObject
{
  function init()
  {
    $this->addSVar('report', 'cust_w/o_nipt');
  }

  function on_report($event_args)
  {
    $report = $event_args['report'];
    $this->setSVar('report', $report);
  }

  function on_get_excel()
  {
    $report = $this->getSVar('report');
    $query = WebApp::getSVar('table::checks->query');
    $rs = WebApp::sqlQuery($query);
    $rs->toExcelFile($report);
  }

  function onRender()
  {
    $report = $this->getSVar('report');
    switch ($report)
      {
      default:
      case 'cust_w/o_nipt':
	$report_title = T_("Customers Without NIPT");
	$report_query = '';
	$report_table = 'em_customers';
	$report_fields = 'customer,NIPT,Address';
	$report_where = "NIPT='' or NIPT is null";
	WebApp::addVars(compact('report_title',
				'report_query',
				'report_table',
				'report_fields',
				'report_where'));
	break;
      case 'cust_w/o_addr':
	$report_title = T_("Customers Without Address");
	$report_query = '';
	$report_table = 'em_customers';
	$report_fields = 'customer,NIPT,Address';
	$report_where = "Address='' or Address is null";
	WebApp::addVars(compact('report_title',
				'report_query',
				'report_table',
				'report_fields',
				'report_where'));
	break;
      case 'cust_w/o_contact_pers':
	$report_title = T_("Customers Without Contact Person");
	$report_query = '';
	$report_table = 'em_customers';
	$report_fields = 'customer,NIPT,contact_person,contact_phone';
	$report_where = "contact_person='' or contact_person is null";
	WebApp::addVars(compact('report_title',
				'report_query',
				'report_table',
				'report_fields',
				'report_where'));
	break;
      case 'cust_w/o_contact_phone':
	$report_title = T_("Customers Without Contact Phone");
	$report_query = '';
	$report_table = 'em_customers';
	$report_fields = 'customer,NIPT,contact_person,contact_phone';
	$report_where = "contact_phone='' or contact_phone is null";
	WebApp::addVars(compact('report_title',
				'report_query',
				'report_table',
				'report_fields',
				'report_where'));
	break;
      case 'single_subs_with_missing_data':
	$report_title = T_("Single Subscribers With Missing Data");
	$report_query = '';
	$report_table = 'em_subscribers';
	$report_fields = ( "MSISDN,FirstName,LastName,Address1,City,PassportNr,"
			   . "postpaid,PaymentResponsible,customer_id");
	$report_where = 
	  "(postpaid='1' and PaymentResponsible='true')
           and (FirstName=MSISDN or FirstName='' or FirstName is null
                or LastName is null or LastName=''
		or Address1 is null or Address1=''
		--or PassportNr is null or PassportNr=''
                )
           and MSISDN not like 'C%' and len(MSISDN)=12";
	WebApp::addVars(compact('report_title',
				'report_query',
				'report_table',
				'report_fields',
				'report_where'));
	break;
      case 'monthlyfee_corrections':
	$report_title = T_("Monthly Fee Corrections For Each Customer");
	$report_query = 'select C.customer as Customer, COS.cos_name as CosName, P.ProductName as MonthlyFee
from em_customer_fees F
     left join em_customers C on (F.customer_id = C.customer_id)
     left join s_cos COS on (F.cos_id = COS.cos_id)
     left join s_productprices P on (F.product_id = P.ProductId)
order by C.customer, P.ProductName;
';
	$report_table = '';
	$report_fields = '';
	$report_where = '';
	WebApp::addVars(compact('report_title',
				'report_query',
				'report_table',
				'report_fields',
				'report_where'));
	break;

	/*
      case 'monthlyfee_corrections_long':
	$report_title = T_("Monthly Fee Corrections For Each Subscriber");
	$report_query = 'select C.customer as Customer, s_cos.cos_name as COS, I.FirstName, I.LastName, I.MSISDN,
       O.DetailName as OldMonthlyFee, P.DetailName as NewMonthlyFee, 
       S.RegistrationDate, O.TotalValue as OldValue, P.TotalValue as NewValue --, * 
from postpaid_scratch..inv_details_1_monthlyfee_orig O
     full outer join postpaid_scratch..inv_details_1_monthlyfee_patch P 
                     on (P.InvoiceID = O.InvoiceID)
     left join postpaid_scratch..inv_subscribers_1 I 
                     on (P.InvoiceID = I.InvoiceID)
     left join em_customers C on (I.customer_id = C.customer_id)
     left join em_subscribers S on (I.subs_id = S.subs_id)
     left join s_cos on (I.COSID = s_cos.cos_id)
where O.DetailName != P.DetailName or O.InvoiceID is null
order by C.customer, s_cos.cos_name, I.MSISDN;
';
	$report_table = '';
	$report_fields = '';
	$report_where = '';
	WebApp::addVars(compact('report_title',
				'report_query',
				'report_table',
				'report_fields',
				'report_where'));
	break;
	*/

      case 'cust_w/o_monthly_fee':
	$report_title = T_("Customers Without Monthly Fee");
	$report_query = '
select customer, NIPT from em_customers 
where customer_id not in (select distinct customer_id from em_customer_fees)
and customer_id > 0;
';
	$report_table = '';
	$report_fields = '';
	$report_where = '';
	WebApp::addVars(compact('report_title',
				'report_query',
				'report_table',
				'report_fields',
				'report_where'));
	break;
      }
  }
}
?>
