// -*-C-*- //tell emacs to use C mode
/*
  This file is part of  HotSpot Manager.  HotSpot Manager can be used
  to manage the users of a network of HotSpot access points.

  Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

  HotSpot Manager  is free software;  you can redistribute  it and/or
  modify  it under the  terms of  the GNU  General Public  License as
  published by the Free Software  Foundation; either version 2 of the
  License, or (at your option) any later version.

  HotSpot Manager is distributed in  the hope that it will be useful,
  but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
  MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
  General Public License for more details.

  You should have  received a copy of the  GNU General Public License
  along  with   NetAccess;  if  not,  write  to   the  Free  Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
*/


function save_acc_rights()
{
  var modules = getCheckedModules();
  var event_args = 'modules='+modules;
  var domains = document.form_access_rights.domains;
  event_args += ';domains=' + domains.value; 
  SendEvent('access_rights', 'save_acc_rights', event_args);  
}

function getCheckedModules()
{
  var modules = document.form_access_rights.modules;
  var checked_items = new Array;
  for(i=0; modules[i]; i++)
    {
      if (modules[i].checked)
        {
          checked_items.push(modules[i].value);
        }
    }

  return checked_items.join();
}
