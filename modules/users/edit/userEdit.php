<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once FORM_PATH.'formWebObj.php';

/**
 * @package users
 */
class userEdit extends formWebObj
{
  function init()
  {
    $this->addSVar('mode', 'add');    // add | edit
  }

  /** add the new user in database */
  function on_add($event_args)
  {
    //check that such a username does not exist in DB
    $username = $event_args['username'];
    $rs = WebApp::openRS('get_user_id', array('username'=>$username));
    if (!$rs->EOF())
      {
        $msg = T_("The username 'var_username' is already used for \n\
somebody else.  Please choose another username.");
        $msg = str_replace('var_username', $username, $msg);
        WebApp::message($msg);
        $this->user_data['username'] = '';
        return;
      }

    //add the user
    $this->insert_record($event_args, 'users');

    //get the id of the just added user
    $rs = WebApp::openRS('get_user_id', array('username'=>$username));
    $user_id = $rs->Field('user_id');

    //set the new user as current user and change the mode to edit
    WebApp::setSVar('userList->current_user', $user_id);
    $this->setSVar('mode', 'edit');

    $this->update_password($event_args);

    //add a log record
    $user = WebApp::getSVar('username');
    $first = $event_args['firstname'];
    $last = $event_args['lastname'];
    $passw = $event_args['password'];
    $details = "username=$username, password=$passw, name=$first $last";
    log_event('+user', $user, '', $details);
  }

  function update_password($event_args)
  {
    //if the password is given, set the password of the new user
    $password = $event_args['password'];
    $password = trim($password);
    if ($password=='')  return;
  
    //encrypt the given password
    srand(time());
    $password = crypt($password, rand());

    //save the encrypted password
    WebApp::execDBCmd('updatePassword', array('password'=>$password));
  }

  /** delete the current user */
  function on_delete($event_args)
  {
    //get the data of the current user before deleting it
    $usr = WebApp::openRS('current_user');

    //delete user
    WebApp::execDBCmd('delete_user');

    //the current_user is deleted,
    //set current the first user in the list
    $userList = WebApp::getObject('userList');
    $userList->selectFirst();

    //acknowledgment message
    WebApp::message(T_("User deleted."));

    //log user deletion
    extract($usr->Fields());
    $user = WebApp::getSVar('username');
    $details = "username=$username, name=$firstname $lastname";
    log_event('-user', $user, '', $details);
  }

  /** save the changes */
  function on_save($event_args)
  {
    //get the old user record
    $old_user = WebApp::openRS('current_user');

    //update user data
    WebApp::execDBCmd('update_user', $event_args);

    //update the password
    $this->update_password($event_args);

    //add a log record
    $user = WebApp::getSVar('username');
    if (trim($event_args['password']==''))  unset($event_args['password']);
    $details = rec_diff($old_user->Fields(), $event_args);
    log_event('~user', $user, '', $details);
  }

  function onParse()
  {
    //get the current user from the list of users
    $user = WebApp::getSVar('userList->current_user');

    if ($user==UNDEFINED)
      {
        $this->setSVar('mode', 'add');
      }
    else
      {
        $this->setSVar('mode', 'edit');
      }
  }

  function onRender()
  {
    $mode = $this->getSVar('mode');
    if ($mode=='add')
      {
        $user_data = $this->pad_record(array(), 'users'); 
      }
    else
      {
        $rs = WebApp::openRS('current_user');
        $user_data = $rs->Fields();
      }
    WebApp::addVars($user_data);      
  }
}
?>