
DROP TABLE IF EXISTS users;
CREATE TABLE users (
  user_id int(11) NOT NULL auto_increment,
  username varchar(20) default NULL,
  password varchar(20) default NULL,
  firstname varchar(20) default NULL,
  lastname varchar(20) default NULL,
  phone1 varchar(20) default NULL,
  phone2 varchar(20) default NULL,
  email varchar(100) default NULL,
  address varchar(100) default NULL,
  notes text,
  modules varchar(250) default NULL,
  domains varchar(250) default NULL,
  PRIMARY KEY  USING BTREE (user_id)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
