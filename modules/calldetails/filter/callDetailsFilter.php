<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package callDetails
   */
class callDetailsFilter extends WebObject
{
  function init()
  {
    $time1 = time() - 24*60*60; //yesterday
    $time2 = time() + 24*60*60; //tomorrow
    $this->addSVars(array(
                          'time1'   => date('Y-m-d H:i', $time1),
                          'time2'   => date('Y-m-d H:i', $time2),
                          'user'    => '',
                          'domain'  => '',
                          'event'   => '',
                          'details' => ''
                          ));
  }

  function onParse()
  {
    WebApp::setSVar('callDetails->filter', $this->get_filter());
  }

  function get_filter()
  {
    //get the state vars as local variables
    $vars = $this->getSVars();
    array_walk($vars, 'trim');
    extract($vars);

    $arr_filters = array();

    //add the time filter
    $arr_filters[] = "(time >= '$time1' AND time <= '$time2')";

    //add the filters for the other fields
    if ($user!='')    $arr_filters[] = "user LIKE '%$user%'";
    if ($domain!='')  $arr_filters[] = "domain LIKE '%$domain%'";
    if ($event!='')   $arr_filters[] = "event LIKE '%$event%'";
    if ($details!='') $arr_filters[] = "details LIKE '%$details%'";

    $filter = implode(' AND ', $arr_filters);
    return $filter;
  }
}
?>