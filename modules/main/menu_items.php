<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /** 
   * The $menu_items array contains the items of the tabs1. 
   * @package main
   */

  //an array of all the available interfaces of the program
$interfaces = array(
                    'customers'   => T_("Customers"),
                    'subscribers' => T_("Subscribers"),
                    'invoices'    => T_("Invoices"),
                    //'calldetails' => T_("Call Details"),
                    'checks'      => T_("Checks"),
                    'update'      => T_("Update"),
                    );

//get user access rights
$modules = WebApp::getSVar('modules');
$arr_modules = explode(',', $modules);

//build the array menu_items, which contains only the
//interfaces that are allowed for this user (superuser has them all)
$menu_items = array();
while (list($id, $title) = each($interfaces))
  {
    if (SU=='true' or in_array($id, $arr_modules))
      {
        $menu_items[$id] = $title;
      }
  }

//add an interface that is available to everyone
$menu_items['user_settings'] = T_("Settings");

//superuser has access to the following items as well
if (SU=='true')
  {
    $menu_items['users'] = T_("Users"); 
    $menu_items['misc'] = T_("Misc"); 
  }
?>