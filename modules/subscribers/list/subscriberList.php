<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package subscribers
   */
class subscriberList extends WebObject
{
  function init()
  {
    $this->addSVar("current_subscriber", UNDEFINED);      
    //set current the first subscriber in the list
    $this->selectFirst();
    $this->addSVar('width', '300');      
  }

  /** set current_subscriber as the first subscriber in the list */
  function selectFirst()
  {  
    $rs = WebApp::openRS("selected_subscribers");
    $first_subscriber = $rs->Field("subs_id");
    $this->setSVar("current_subscriber", $first_subscriber);
    WebApp::setSVar('subscriberEdit->subs_id', $first_subscriber);
  }

  function on_change_width($event_args)
  {
    $increment = $event_args['increment'];
    $width = $this->getSVar('width');

    if ($increment=='+') 
      $width += 50;
    else if ($increment=='-')
      $width -= 50;
    else
      $width = 300;

    if ($width < 200)  $width = 200;
    if ($width > 500)  $width = 500;

    $this->setSVar('width', $width);  
  }

  function on_next($event_args)
  {
    $page = $event_args["page"];
    WebApp::setSVar("selected_subscribers->current_page", $page);
    $this->selectFirst();  
  }

  function on_refresh($event_args)
  {
    //recount the selected subscribers
    WebApp::setSVar("selected_subscribers->recount", "true");

    //select the first one in the current page
    $this->selectFirst();
  }

  function on_select($event_args)
  {
    //set current_subscriber to the selected one
    $rs = WebApp::openRS("get_subscriber", $event_args);
    if ($rs->EOF())
      {
        $this->selectFirst();   
      }
    else
      {
        $subs_id = $event_args["subs_id"];
        $this->setSVar("current_subscriber", $subs_id);
	WebApp::setSVar('subscriberEdit->subs_id', $subs_id);
      }
  }

  function on_export($event_args)
  {
    $rs = WebApp::openRS('get_subscriber_list');
    $rs->toExcelFile('selected_subscribers');
  }

  function on_add_new_subscriber($event_args)
  {
    //when the button Add New Subscriber is clicked
    //make current_customer UNDEFINED
    $this->setSVar("current_subscriber", UNDEFINED);
    WebApp::setSVar('subscriberEdit->subs_id', UNDEFINED);
  }

  function onParse()
  {
    //recount the selected subscribers
    WebApp::setSVar("selected_subscribers->recount", "true");
  }
}
?>