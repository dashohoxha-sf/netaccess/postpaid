<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once FORM_PATH.'formWebObj.php';

/**
 * @package subscribers
 */
class subscriber_info extends formWebObj
{
  /** associated array that contains some default values of the form */
  var $fields = array();

  /** Check whether the MSISDN is already registered. */
  function msisdn_exists($MSISDN)
  {
    $rs = WebApp::openRS('get_subscriber', compact('MSISDN'));
    if ($rs->EOF())  return false;
  
    $msg = T_("The MSISDN 'var_MSISDN' is already registered.");
    $msg = str_replace('var_MSISDN', $MSISDN, $msg);
    WebApp::message($msg);

    return true;
  }

  /**
   * Check that MSISDN is valid.
   * Return false if there is something wrong, otherwise return true. 
   */
  function check_msisdn($MSISDN)
  {
    //check the length of MSISDN is ok
    if (strlen($MSISDN) != 12)
      {
	$msg = T_("The length of MSISDN 'var_MSISDN' is not OK.");
	$msg = str_replace('var_MSISDN', $MSISDN, $msg);
	WebApp::message($msg);
	return false;
      }

    //check that MSISDN starts with '35567'
    if (!ereg('35567.+', $MSISDN))
      {
	$msg = T_("The MSISDN should start with '35567'.");
	WebApp::message($msg);
	return false;
      }

    return true;
  }

  /**
   * Make sure that customer has a correct value and set also customer_id.
   */
  function check_customer(&$record)
  {
    $customer = trim($record['customer']);

    if ($customer!='')
      {
	$rs = WebApp::openRS('get_customer', compact('customer'));
	if ($rs->EOF())
	  {
	    //customer does not exist
	    $msg = T_("The customer 'v_customer' does not exist!");
	    $msg = str_replace('v_customer', $customer, $msg);
	    WebApp::message($msg);
	    $customer = '';
	  }
	else
	  {
	    //customer exists, get the customer_id
	    $customer_id = $rs->Field('customer_id');
	  }
      }

    //if customer does not exist, set it to be either
    //'Single Subscribers' of 'Unclassified Subscribers'
    //depending on the PaymentResponsible field
    if ($customer=='')
      {
	if ($record['PaymentResponsible']=='true')
	  {
	    $customer = '1 -- Single Subscribers';
	    $customer_id = '1';
	  }
	else
	  {
	    $customer = '0 -- Unclassified Subscribers';
	    $customer_id = '0';
	  }
      }

    //update customer and customer_id in the record
    $record['customer'] = $customer;
    $record['customer_id'] = $customer_id;
  }

  /** Correct some fields of the record. */
  function correct_record(&$record)
  {
    $MSISDN = trim($record['MSISDN']);
    $record['MSISDN'] = $MSISDN;

    $postpaid = $record['postpaid'];
    $record['postpaid'] = ($postpaid=='true' ? '1' : '0');

    $record['last_modified'] = date('Y-m-d H:i:s', time());
    $record['modified_by'] = WebApp::getSVar('username');
  }

  /** add a new subscriber */
  function on_add($event_args)
  {
    //adding subscribers is disabled for the time being
    WebApp::message(T_("Adding subscribers is disabled for the time being."));
    return;

    $record = $event_args;

    //correct some fields of the $record
    $this->correct_record($record);

    //check that MSISDN is OK
    $MSISDN = $record['MSISDN'];
    if ($this->check_msisdn($MSISDN)==false or $this->msisdn_exists($MSISDN))
      {
	$this->fields = $record;
	return;
      }

    //check the customer and get the customer_id
    $this->check_customer($record);

    //add the subscriber
    $this->insert_record($record, 'em_subscribers');

    //set the new subscriber as current subscriber and change the mode to edit
    $rs = WebApp::openRS('get_subscriber', compact('MSISDN'));
    $subs_id = $rs->Field('subs_id');
    WebApp::setSVar('subscriberList->current_subscriber', $subs_id);
    WebApp::setSVar('subscriberEdit->subs_id', $subs_id);
    WebApp::setSVar('subscriberEdit->mode', 'edit');

    //add a log record
    /*
    $user = WebApp::getSVar('username');
    $firstname = $record['firstname'];
    $lastname = $record['lastname'];
    $details = "username=$username, name=$firstname $lastname";
    log_event('+client', $user, $domain, $details);
    */
  }

  /** save the changes */
  function on_save($event_args)
  {
    $record = $event_args;

    //correct some fields of the $record
    $this->correct_record($record);

    //get the old values of the subscriber
    $rs_old_subs = WebApp::openRS('get_subscriber_info');

    //check that MSISDN is OK
    $MSISDN = $record['MSISDN'];
    if ($MSISDN != $rs_old_subs->Field('MSISDN'))
      {
	if ($this->check_msisdn($MSISDN)==false 
	    or $this->msisdn_exists($MSISDN))
	  {
	    $this->fields = $record;
	    return;
	  }

	//change of MSISDN is disabled for the time being
	$record['MSISDN'] = $rs_old_subs->Field('MSISDN');
	WebApp::message(T_("Changing MSISDN is disabled for the time being."));
      }

    //change of COS is disabled for the time being
    $record['COSID']= $rs_old_subs->Field('COSID');

    //check the customer and get the customer_id
    $this->check_customer($record);

    //update subscriber data
    $record['subs_id'] = WebApp::getSVar('subscriberEdit->subs_id');
    //$this->update_record($record, 'em_subscribers', 'subs_id');
    WebApp::execDBCmd('update_subscriber', $record);
  }

  function onRender()
  {
    $mode = WebApp::getSVar('subscriberEdit->mode');
    if ($mode=='add')
      {
        $subscriber_data = $this->pad_record(array(), 'em_subscribers');
	$subscriber_data['customer'] = '';
      }
    else
      {
        $rs = WebApp::openRS('get_subscriber_info');
        $subscriber_data = $rs->Fields();
      }

    //modify some fields
    $postpaid = $subscriber_data['postpaid'];
    $subscriber_data['postpaid'] = ($postpaid=='0' ? '' : 'checked');
    $time = $subscriber_data['last_modified'];
    $subscriber_data['last_modified'] = date('Y-m-d H:i', strtotime($time));

    //merge $subscriber_data with $this->fields
    $subscriber_data = array_merge($subscriber_data, $this->fields);

    //add variables for the field values of the form
    WebApp::addVars($subscriber_data);
  }
}
?>