<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

/**
 * @package subscribers
 */
class subscriberEdit extends WebObject
{
  function init()
  {
    $this->addSVar('subs_id', UNDEFINED);
    $this->addSVar('mode', 'add');    // add | edit
  }

  function on_delete($event_args)
  {
    //get the number of the invoices of this subscriber
    $rs = WebApp::openRS('get_inv_nr');
    $inv_nr = $rs->Field('inv_nr');
    if ($inv_nr!=0)
      {
	$msg = T_("The subscriber cannot be deleted because it has invoices.");
	WebApp::message($msg);
	return;
      }

    //deleting subscribers is disabled for the time being
    WebApp::message(T_("Deleting subscribers is disabled for the time being. Please ask help for deleting it."));
    return;


    //get the data of the current subscriber before deleting it
    //$subscriber_data = WebApp::openRS('get_subscriber_name');

    //delete the subscriber
    $subs_id = $this->getSVar('subs_id');
    WebApp::execDBCmd('copy_subscriber', compact('subs_id'));
    WebApp::execDBCmd('delete_subscriber', compact('subs_id'));

    //the current_subscriber is deleted,
    //set current the first subscriber in the list
    $subscriberList = WebApp::getObject('subscriberList');
    $subscriberList->selectFirst();

    //acknowledgment message
    WebApp::message(T_("Subscriber deleted."));

    //log subscriber deletion
    /*
    extract($subscriber_data->Fields());
    $user = WebApp::getSVar('username');
    $details = "username=$username, name=$firstname $lastname";
    log_event('-subscriber', $user, $domain, $details);
    */
  }

  function onParse()
  {
    //get the current subscriber 
    $subs_id = $this->getSVar('subs_id');

    if ($subs_id==UNDEFINED)
      {
        $this->setSVar('mode', 'add');
      }
    else
      {
        $this->setSVar('mode', 'edit');
      }
  }

  function onRender()
  {
    $mode = $this->getSVar('mode');
    if ($mode=='edit')
      {
	$rs = WebApp::openRS('current_subscriber');
	WebApp::addVars($rs->Fields());
      }
  }
}
?>