// -*-C-*- //tell emacs to use C mode

function view_calldetails(subs_id)
{
  var idx = month.selectedIndex;
  var month_value = month.options[idx].value;

  var target = 'reports/calldetails/calldetails.html';
  var event_args = 'subs_id='+subs_id+';month='+month_value;

  var win_features = 'width=850,height=900,resizeable=1,scrollbars=1,menubar=1';
  window.open('', 'view_calldetails', win_features);

  wGoTo('view_calldetails', target+'?event=calldetails.view('+event_args+')');
}

function view_envelope(subs_id)
{
  var idx = month.selectedIndex;
  var month_value = month.options[idx].value;

  var target = 'reports/envelopes/envelope.html';
  var event_args = 'module=subscriber;id='+subs_id+';month='+month_value;

  var win_features = 'width=850,height=900,resizeable=1,scrollbars=1,menubar=1';
  window.open('', 'view_subs_envelope', win_features);

  wGoTo('view_subs_envelope', target+'?event=envelope.view('+event_args+')');
}
