<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package misc
   */
class change_password extends WebObject
{
  function on_change($event_args)
  {
    if (SU=='true') 
      $this->change_su_passwd($event_args);
    else
      $this->change_passwd($event_args);
  }

  function change_passwd($event_args)
  {
    $rs = WebApp::openRS('get_passwd');
    $crypted_passwd = $rs->Field('password');
    $old_passwd = $event_args["old_password"];
    if ($crypted_passwd == crypt($old_passwd, $crypted_passwd))
      {
        //encrypt the new password and save it
        $new_password = $event_args["new_password"];
        srand(time());
        $password = crypt($new_password, rand());
        //$password = shell("openssl passwd -1 '$new_password'");
        WebApp::execDBCmd('set_passwd', compact('password'));
        WebApp::message(T_("Password changed successfully."));
      }
    else
      {
        $msg = T_("The old password you supplied is not correct!\n\
Password not changed. Please try again.");
        WebApp::message($msg);
      }
  }

  function change_su_passwd($event_args)
  {
    //check that the old password supplied is correct
    $crypted_passwd = shell('cat .su/supasswd');
    $crypted_passwd = trim($crypted_passwd);
    $old_passwd = $event_args["old_password"];
    if ($crypted_passwd == crypt($old_passwd, $crypted_passwd))
      {
        //encrypt the new password and save it
        $new_password = $event_args["new_password"];
        //srand(time());
        //$password = crypt($new_password, rand());
        $password = shell("openssl passwd -1 '$new_password'");
        $output = shell("echo '$password' > .su/supasswd");
        if (trim($output)=='')
          {
            WebApp::message(T_("Password changed successfully."));
          }
        else
          {
            WebApp::message(T_("Could not save the new password."));            
          }
      }
    else
      {
        $msg = T_("The old password you supplied is not correct!\n\
Password not changed. Please try again.");
        WebApp::message($msg);
      }         
  }
}
?>