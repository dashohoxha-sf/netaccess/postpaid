<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once FORM_PATH.'formWebObj.php';

/**
 * @package settings
 */
class domain_edit extends formWebObj
{
  function init()
  {
    $many_domains = WebApp::getSVar('many_domains');
    if ($many_domains=='false')
      {
        $domain = WebApp::getSVar('domains');
        $this->setSVar('domain_id', $domain);
      }
  }

  /** save the changes */
  function on_save($event_args)
  {
    //get the current domain
    $rs = WebApp::openRS('user_domain');

    //update domain data
    $domain = $this->getSVar('domain_id');
    $event_args['id'] = $domain;
    $this->update_record($event_args, 'domains', 'id');

    //add a log record
    $user = WebApp::getSVar('username');
    $details = rec_diff($rs->Fields(), $event_args);
    log_event('~domain', $user, $domain, $details);
  }

  function onRender()
  {
    $rs = WebApp::openRS('user_domain');
    WebApp::addVars($rs->Fields());  
  }
}
?>