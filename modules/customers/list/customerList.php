<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package customers
   */
class customerList extends WebObject
{
  function init()
  {
    $this->addSVar("current_customer", UNDEFINED);      
    //set current the first customer in the list
    $this->selectFirst();  
  }

  /** set current_customer as the first customer in the list */
  function selectFirst()
  {  
    $rs = WebApp::openRS("selected_customers");
    $first_customer = $rs->Field("customer_id");
    $this->setSVar("current_customer", $first_customer);
    WebApp::setSVar('customerEdit->customer_id', $first_customer);
  }

  function on_next($event_args)
  {
    $page = $event_args["page"];
    WebApp::setSVar("selected_customers->current_page", $page);
    $this->selectFirst();  
  }

  function on_refresh($event_args)
  {
    //recount the selected customers
    WebApp::setSVar("selected_customers->recount", "true");

    //select the first one in the current page
    $this->selectFirst();
  }

  function on_select($event_args)
  {
    //set current_customer to the selected one
    $rs = WebApp::openRS("get_customer", $event_args);
    if ($rs->EOF())
      {
        $this->selectFirst();   
      }
    else
      {
        $customer_id = $event_args["customer_id"];
        $this->setSVar("current_customer", $customer_id);
	WebApp::setSVar('customerEdit->customer_id', $customer_id);
      }
  }

  function on_export($event_args)
  {
    $rs = WebApp::openRS('get_customer_list');
    $rs->toExcelFile('selected_customers');
  }

  function on_add_new_customer($event_args)
  {
    //when the button Add New Customer is clicked
    //make current_customer UNDEFINED
    $this->setSVar("current_customer", UNDEFINED);
    WebApp::setSVar('customerEdit->customer_id', UNDEFINED);
  }

  function onParse()
  {
    //recount the selected customers
    WebApp::setSVar("selected_customers->recount", "true");
  }
}
?>