
DROP TABLE IF EXISTS em_customers;
CREATE TABLE em_customers (
  domain varchar(20) default NULL,
  username varchar(20) NOT NULL default '',
  password varchar(100) default '',
  firstname varchar(20) default NULL,
  lastname varchar(20) default NULL,
  email varchar(50) default NULL,
  phone1 varchar(20) default NULL,
  phone2 varchar(20) default NULL,
  address varchar(100) default NULL,
  notes text,
  service varchar(20) default NULL,
  expiration_time datetime default NULL,
  download_limit int(11) default NULL,
  upload_limit int(11) default NULL,
  online_time time default NULL,
  last_modified timestamp NOT NULL default CURRENT_TIMESTAMP,
  modified_by varchar(20) default NULL,
  PRIMARY KEY  (username),
  UNIQUE KEY u (username)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
