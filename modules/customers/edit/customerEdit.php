<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

/**
 * @package customers
 */
class customerEdit extends WebObject
{
  function init()
  {
    $this->addSVar('customer_id', UNDEFINED);
    $this->addSVar('mode', 'add');    // add | edit
  }

  function on_delete($event_args)
  {
    //get the number of the subscribers of this customer
    $rs = WebApp::openRS('get_subs_nr');
    $subs_nr = $rs->Field('subs_nr');
    if ($subs_nr!=0)
      {
	$msg = T_("The customer cannot be deleted because it has subscribers.");
	WebApp::message($msg);
	return;
      }

    //get the number of the invoices of this customer
    $rs = WebApp::openRS('get_inv_nr');
    $inv_nr = $rs->Field('inv_nr');
    if ($inv_nr!=0)
      {
	$msg = T_("The customer cannot be deleted because it has invoices.");
	WebApp::message($msg);
	return;
      }

    //get the current customer_id
    $customer_id = $this->getSVar('customer_id');

    //delete the customer
    WebApp::execDBCmd('del_customer', compact('customer_id'));

    //the current_customer is deleted,
    //set current the first customer in the list
    $customerList = WebApp::getObject('customerList');
    $customerList->selectFirst();

    //acknowledgment message
    WebApp::message(T_("Customer deleted."));

    //log customer deletion
    log_event("-customer($customer_id)");
  }

  function onParse()
  {
    //get the current customer from the list of customers
    $customer_id = $this->getSVar('customer_id');

    if ($customer_id==UNDEFINED)
      {
        $this->setSVar('mode', 'add');
      }
    else
      {
        $this->setSVar('mode', 'edit');
      }
  }

  function onRender()
  {
    $mode = $this->getSVar('mode');
    if ($mode=='edit')
      {
        $rs = WebApp::openRS('get_customer_name');
	WebApp::addVars($rs->Fields());
      }
  }
}
?>