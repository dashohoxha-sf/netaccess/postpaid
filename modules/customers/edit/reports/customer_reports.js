// -*-C-*- //tell emacs to use C mode

function view_envelope(customer_id)
{
  var idx = month.selectedIndex;
  var month_value = month.options[idx].value;

  var target = 'reports/envelopes/envelope.html';
  var event_args = 'module=customer;id='+customer_id+';month='+month_value;

  var win_features = 'width=850,height=900,resizeable=1,scrollbars=1,menubar=1';
  window.open('', 'view_cust_envelope', win_features);

  wGoTo('view_cust_envelope', target+'?event=envelope.view('+event_args+')');
}

function view_rpt_debit(customer_id)
{
  var idx = month.selectedIndex;
  var month_value = month.options[idx].value;

  var event_args = 'customer_id='+customer_id+';month='+month_value;
  SendEvent('customer_reports', 'debit_report', event_args);
}

function view_rpt_liability(customer_id)
{
  var event_args = 'customer_id='+customer_id;
  SendEvent('customer_reports', 'liability_report', event_args);
}
