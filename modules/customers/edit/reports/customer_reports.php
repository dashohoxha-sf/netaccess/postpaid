<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


/**
 * @package customers
 */
class customer_reports extends WebObject
{
  function on_debit_report($event_args)
  {
    $customer_id = $event_args['customer_id'];
    $month = $event_args['month'];

    //get the name of the customer
    $rs = WebApp::openRS('get_customer_name');
    $customer = $rs->Field('customer');

    //get the report
    $query = "exec emsp_customer_summary_report $customer_id, '$month'";
    $rs = WebApp::sqlQuery($query);
    //print $rs->toHtmlTable();

    //send the report
    //$fname = "debit_${month}_${customer}.xls";
    //$rs->sendTabDelimitedFile($fname);
    $rs->toExcelFile("debit_${month}_${customer}");
  }

  function on_liability_report($event_args)
  {
    $customer_id = $event_args['customer_id'];

    //get the filename of the report
    $customer = $this->get_customer_name();
    $fname = "rpt_liability_${customer}.xls";

    //get and send the report
    $query = "exec emsp_customer_liability_report $customer_id;";
    $rs = WebApp::sqlQuery($query);
    $this->send_liability_report($rs);
  }

  function onRender()
  {
    $rs = WebApp::openRS('get_customer_info');
    WebApp::addVars($rs->Fields());
  }

  function send_liability_report($rs)
  {
    //get an array of months by removing the first columns
    //(SubsId, MSISDN, FirstName, LastName) and the last column (Total)
    $arr_months = array_keys($rs->Fields());
    $arr_months = array_slice($arr_months, 4);
    array_pop($arr_months);
    $nr_months = sizeof($arr_months);

    //get the name of the customer
    $rs1 = WebApp::openRS('get_customer_name');
    $customer = $rs1->Field('customer');

    //include the excel writer
    require_once "Spreadsheet/Excel/Writer.php";

    //create workbook and worksheet
    $xls =& new Spreadsheet_Excel_Writer();
    $worksheet = substr($customer, 0, 30);
    $rpt =& $xls->addWorksheet($worksheet);

    //add the title to the top left cell of the worksheet
    $format =& $xls->addFormat();
    $format->setFontFamily('Helvetica');
    $format->setBold();
    $format->setSize('12');
    $format->setColor('navy');
    $format->setAlign('merge');
    $rpt->write(0, 0, $customer, $format);
    //merge with empty cells
    for ($i=0; $i < $nr_months+3; $i++)  $arr[] = '';
    $rpt->writeRow(0, 1, $arr, $format);

    $rpt->setRow(0, 20);  //set row height
    $rpt->setColumn(0, 0, 7);  //set column width for the first col
    $rpt->setColumn(1, 1, 15);  //set width for the second column
    $rpt->setColumn(2, 2, 25);  //set width for the third column
    $rpt->setColumn(3, 3+$nr_months, 12);

    //add column headings
    $format =& $xls->addFormat();
    $format->setBold();
    $format->setFontFamily('Helvetica');
    $format->setBold();
    $format->setSize('11');
    $format->setAlign('center');
    $colNames = $arr_months;
    array_unshift($colNames, 'SubsId', T_("Phone Number"), T_("Name Surname"));
    array_push($colNames, T_("Total"));
    $rpt->writeRow(1, 0, $colNames, $format);
    $rpt->setRow(1, 15, $format);

    //define some formats
    $msisdn_fmt =& $xls->addFormat();
    $msisdn_fmt->setHAlign('center');

    $value_fmt =& $xls->addFormat();
    $value_fmt->setNumFormat('0.00');

    //freeze the top 2 rows
    $freeze = array(2,0,2,0);
    $rpt->freezePanes($freeze);

    //keep track of the current excel row number
    $row_nr = 2;

    //loop through the data, adding it to the sheet
    while (!$rs->EOF())
      {
	$data_row = $rs->Fields();
	$rpt->writeNumber($row_nr, 0, $data_row['SubsId']);
	$rpt->writeString($row_nr, 1, $data_row['MSISDN'], $msisdn_fmt);
	$name = $data_row['FirstName'].' '.$data_row['LastName'];
	$rpt->writeString($row_nr, 2, $name);

	for ($i=0; $i < $nr_months; $i++)
	  {
	    $month = $arr_months[$i];
	    $rpt->writeNumber($row_nr, 3+$i, $data_row[$month], $value_fmt);
	  }

	//write the sum for each subscriber
	$cell1 = Spreadsheet_Excel_Writer::rowcolToCell($row_nr, 3);
	$cell2 = Spreadsheet_Excel_Writer::rowcolToCell($row_nr, $nr_months+2);
	$formula = "=SUM($cell1:$cell2)";
	$rpt->writeFormula($row_nr, $nr_months+3, $formula, $value_fmt);

	//set row height
	$rpt->setRow($row_nr, 15);

	//move to the next row
	$row_nr++;
	$rs->MoveNext();
      }

    //the format of the total cells
    $total_fmt =& $xls->addFormat();
    $total_fmt->setNumFormat('0.00');
    $total_fmt->setFontFamily('Helvetica');
    $total_fmt->setBold();
    $total_fmt->setTop(1); // Top border

    $rpt->writeString($row_nr, 2, T_("Total:"), $total_fmt);

    //add the totals for each column
    for ($i=0; $i <= $nr_months; $i++)
      {
	$cell1 = Spreadsheet_Excel_Writer::rowcolToCell(2, 3+$i);
	$cell2 = Spreadsheet_Excel_Writer::rowcolToCell($row_nr-1, 3+$i);
	$formula = "=SUM($cell1:$cell2)";
	$rpt->writeFormula($row_nr, 3+$i, $formula, $total_fmt);
      }

    //set row height and format
    $rpt->setRow($row_nr, 15, $total_fmt);

    // Send the Spreadsheet to the browser
    $xls->send("liability $customer.xls");
    $xls->close();

    exit;
  }
}
?>