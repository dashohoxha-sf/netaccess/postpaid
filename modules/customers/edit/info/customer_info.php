<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once FORM_PATH.'formWebObj.php';

/**
 * @package customers
 */
class customer_info extends formWebObj
{
  /** associated array that contains some default values of the form */
  var $fields = array();

  function fix_customer_data($record)
  {
    $record['NIPT'] = strtoupper(trim($record['NIPT']));
    $record['Address'] = trim($record['Address']);
    $record['contact_person'] = trim($record['contact_person']);
    $record['contact_phone'] = trim($record['contact_phone']);
    $record['contact_email'] = trim($record['contact_email']);
    $record['last_modified'] = date('Y-m-d H:i:s', time());
    $record['modified_by'] = WebApp::getSVar('username');

    return $record;
  }

  /** add a new customer */
  function on_add($event_args)
  {
    //check that such a customer does not exist
    $customer = $event_args['customer'];
    if ($this->customer_exists($customer))
      {
        $this->fields = $event_args;
        return;
      }

    //add the customer
    $record = $this->fix_customer_data($event_args);
    unset($record['customer_id']);
    $this->insert_record($record, 'em_customers');

    //set the new customer as current customer and change the mode to edit
    $rs = WebApp::openRS('get_customer', compact('customer'));
    $customer_id = $rs->Field('customer_id');
    WebApp::setSVar('customerList->current_customer', $customer_id);
    WebApp::setSVar('customerEdit->customer_id', $customer_id);
    WebApp::setSVar('customerEdit->mode', 'edit');

    //add a log record
    log_event("+customer($customer_id)");
  }

  /** Check that such a customer does not exist. */
  function customer_exists($customer)
  {
    $rs = WebApp::openRS('get_customer', compact('customer'));
    if ($rs->EOF())  return false;

    //display a notification message
    $msg = T_("The customer 'var_customer' is already registered.");
    $msg = str_replace('var_customer', $customer, $msg);
    WebApp::message($msg);

    return true;
  }

  /** save the changes */
  function on_save($event_args)
  {
    $customer_id = WebApp::getSVar('customerEdit->customer_id');
    if ($customer_id=='0' or ($customer_id=='1'))
      {
	WebApp::message(T_("This customer cannot be modified."));
	return;
      }

    //get the old client record
    $rs = WebApp::openRS('get_customer_info');
    $old_customer = $this->fix_customer_data($rs->Fields());

    //update customer data
    $record = $this->fix_customer_data($event_args);
    $record['customer_id'] = $customer_id;
    //$this->update_record($record, 'em_customers', 'customer_id');
    WebApp::execDBCmd('update_customer', $record);

    //update the payment responsible field of the subscribers
    //set it to the opposite of the group's field
    $payment_r = $event_args['PaymentResponsible'];
    $payment_r = ($payment_r=='true' ? 'false' : 'true');
    WebApp::execDBCmd('set_payment_responsible', compact('payment_r'));

    //add a log record
    //unset($record['last_modified']);
    //unset($record['modified_by']);
    log_change_event("~customer($customer_id)", $old_customer, $record);
  }

  function onRender()
  {
    $mode = WebApp::getSVar('customerEdit->mode');
    if ($mode=='add')
      {
        $customer_data = $this->pad_record(array(), 'em_customers');
      }
    else
      {
        $rs = WebApp::openRS('get_customer_info');
        $customer_data = $rs->Fields();
      }

    //modify some fields
    $time = $customer_data['last_modified'];
    $customer_data['last_modified'] = date('Y-m-d H:i', strtotime($time));

    //merge $customer_data with $this->fields
    $customer_data = array_merge($customer_data, $this->fields);

    //add variables for the field values of the form
    WebApp::addVars($customer_data);

    /*
    //create and add a recordset with the services of the domain
    $domain = $client_data['domain'];
    $service_list = $this->get_service_list($domain);
    WebApp::openRS('services', compact('service_list'));
    */
  }
}
?>