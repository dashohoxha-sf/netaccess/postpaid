// -*-C-*- //tell emacs to use C mode
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function add_fee()
{
  var form = document.add_fee;
  var idx = form.cos.selectedIndex;
  var cos_id = form.cos.options[idx].value;
  idx = form.fees.selectedIndex;
  var product_id = form.fees.options[idx].value;
  var event_args = 'cos_id='+cos_id+';product_id='+product_id;

  //alert(event_args);  //debug
  SendEvent('feeList', 'add', event_args);
}

function del_fee(fee_id)
{
  SendEvent('feeList', 'del', 'fee_id='+fee_id);
}
