<?php
  /*
   This file  is part of NetAccess.   NetAccess is a  web application for
   managing/administrating the  network connections of the  clients of an
   ISP.

   Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   NetAccess is free  software; you can redistribute it  and/or modify it
   under the terms of the GNU  General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.

   NetAccess  is distributed  in the  hope that  it will  be  useful, but
   WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
   MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
   General Public License for more details.

   You  should have received  a copy  of the  GNU General  Public License
   along with NetAccess;  if not, write to the  Free Software Foundation,
   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
  */


  /**
   * @package    admin
   * @subpackage feeList
   */

class feeList extends WebObject
{
  function on_del($event_args)
  {
    $fee_id = $event_args['fee_id'];

    //get the record to be deleted before deleteing it
    $rs = WebApp::openRS('get_fee', compact('fee_id'));

    //delete the fee record
    WebApp::execDBCmd('del_fee', compact('fee_id'));

    //add a log record
    $this->add_log('del_fee', $rs->Fields());
  }

  function on_add($event_args)
  {
    $event_args['customer_id'] = WebApp::getSVar('customerEdit->customer_id');
    WebApp::execDBCmd('add_fee', $event_args);

    //add a log record
    $this->add_log('add_fee', $event_args);
  }

  function add_log($action, $params)
  {
    extract($params);

    //get customer_id
    $customer_id = WebApp::getSVar('customerEdit->customer_id');

    //get cos_name and product_name
    $rs = WebApp::openRS('get_cos_name', compact('cos_id'));
    $cos_name = $rs->Field('cos_name');
    $rs = WebApp::openRS('get_product_name', compact('product_id'));
    $product_name = $rs->Field('product_name');

    $username = UNDEFINED;
    $time = UNDEFINED;
    $comment = "$cos_name, $product_name";

    if ($action=='add_fee')
      {
	$field = '+fee';
	$old_value = '';
	$new_value = "$cos_id,$product_id";
      }
      else  //($action=='del_fee')
      {
	$field = '-fee';
	$old_value = "$cos_id,$product_id";
	$new_value = '';
      }

    //add a log record
    log_event("~customer($customer_id)", $username, $time, 
              $comment, $field, $old_value, $new_value);
  }

  function onRender()
  {
  }
}
?>