<?php
  /*
   This file  is part of NetAccess.   NetAccess is a  web application for
   managing/administrating the  network connections of the  clients of an
   ISP.

   Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   NetAccess is free  software; you can redistribute it  and/or modify it
   under the terms of the GNU  General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.

   NetAccess  is distributed  in the  hope that  it will  be  useful, but
   WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
   MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
   General Public License for more details.

   You  should have received  a copy  of the  GNU General  Public License
   along with NetAccess;  if not, write to the  Free Software Foundation,
   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
  */


  /**
   * @package    admin
   * @subpackage subsList
   */

class subsList extends WebObject
{
  function init()
  {
    $this->addSVar('filter', '(1=1)');
    $this->addSVar('order_by', 'MSISDN ASC');
  }

  function on_reorder($event_args)
  {
    $order_field = $event_args['order_field'];
    $order_by = $this->getSVar('order_by');
    list($field, $direction) = split(' ', $order_by, 2);

    if ($field==$order_field)
      {
	//change the direction
	$direction = ($direction=='ASC' ? 'DESC' : 'ASC');
      }
    else
      {
	//change the field
	$field = $order_field;
      }

    //set the new order_by variable
    $this->setSVar('order_by', "$field $direction");
  }

  function on_next($event_args)
  {
    $page = $event_args['page'];
    WebApp::setSVar('subsList_rs->current_page', $page);
  }

  function on_del($event_args)
  {
    WebApp::execDBCmd('remove_subs', $event_args);
    WebApp::message(T_("The subscriber was moved to the group 'Unclassified'."));
  }

  function on_add($event_args)
  {
    WebApp::setSVar('subsEdit->mode', 'add');
    WebApp::setSVar('subsEdit->subs_id', UNDEFINED);
  }

  function on_edit($event_args)
  {
    WebApp::setSVar('subsEdit->mode', 'edit');
    WebApp::setSVar('subsEdit->subs_id', $event_args['subs_id']);
  }

  function on_import($event_args)
  {
    WebApp::setSVar('importList->visible', 'true');
  }

  function on_export($event_args)
  {
    $rs = WebApp::openRS('subsList_all');
    $rs->toExcelFile('subscriber_list');
  }

  function on_new_customers($event_args)
  {
    $rs = WebApp::openRS('suggested_customers');
    $rs->toExcelFile('suggested_new_customers');
  }

  function on_new_customer_numbers($event_args)
  {
    $rs = WebApp::openRS('suggested_customer_numbers');
    $rs->toExcelFile('suggested_customer_numbers');
  }

  function on_bulk($event_args)
  {
    WebApp::setSVar('subsBulk->visible', 'true');
  }

  function onRender()
  {
    WebApp::setSVar('subsList_rs->recount', 'true');
    $rs = WebApp::openRS('subsList_rs');
    while (!$rs->EOF())
      {
	$postpaid = $rs->Field('postpaid');
	$postpaid = ($postpaid=='0' ? '' : 'checked');
	$rs->setFld('postpaid', $postpaid);
	$rs->MoveNext();
      }
  }
}
?>