// -*-C-*- //tell emacs to use C mode
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function subsList_order_by(order_field)
{
  SendEvent('subsList', 'reorder', 'order_field='+order_field);
}

function view_subs(subs_id)
{
  var event_args = 'module=subscribers;subs_id='+subs_id;
  wSendEvent('subs', 'main', 'select', event_args);
}

function add_subs()
{
  SendEvent('subsList', 'add');
}

function edit_subs(subs_id)
{
  SendEvent('subsList', 'edit', 'subs_id='+subs_id);
}

function del_subs(subs_id, msisdn)
{
  var msg = T_("You are removing from this customer the subscriber: ") + msisdn;
  if (!confirm(msg))  return;

  SendEvent('subsList', 'del', 'subs_id='+subs_id);
}

function import_subs()
{
  SendEvent('subsList', 'import');
}

function export_subs()
{
  SendEvent('subsList', 'export');
}

function bulk()
{
  SendEvent('subsList', 'bulk');
}

function new_customers()
{
  SendEvent('subsList', 'new_customers');
}

function new_customer_numbers()
{
  SendEvent('subsList', 'new_customer_numbers');
}
