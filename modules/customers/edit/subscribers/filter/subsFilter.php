<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    admin
 * @subpackage subsList
 */
class subsFilter extends WebObject
{
  function init()
    {
      $this->addSVars(array(
                            'msisdn'   => '',
                            's_name'   => '',
                            'address'  => '',
                            'cos'      => '',
                            'date'     => '',
			    'postpaid' => ''
                            ));
    }

  function onParse()
    {
      WebApp::setSVar('subsList->filter', $this->get_filter());
    }

  function get_filter()
    {
      //get the state vars as local variables
      $vars = $this->getSVars();
      array_walk($vars, 'trim');
      extract($vars);

      //get the filter conditions
      $conditions = array();

      if ($msisdn!='')
	$conditions[] = "MSISDN LIKE '%$msisdn%'";

      if ($s_name!='')  
	$conditions[] = "(FirstName+' '+LastName) LIKE '%$s_name%'";

      if ($address!='')
	$conditions[] = "(Address1+Address2+City) LIKE '%$address%'";

      if ($cos!='')
	$conditions[] = "COSID LIKE '%$cos%'";

      if ($date!='')
	$conditions[] = "RegistrationDate $date";

      if ($postpaid=='checked')
	$conditions[] = "postpaid = '1'";

      $filter = implode(' AND ', $conditions);
      if ($filter=='')  $filter = '1=1';

      return $filter;
    }
}
?>