// -*-C-*- //tell emacs to use C mode
/*
  This file is part of  HotSpot Manager.  HotSpot Manager can be used
  to manage the users of a network of HotSpot access points.

  Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

  HotSpot Manager  is free software;  you can redistribute  it and/or
  modify  it under the  terms of  the GNU  General Public  License as
  published by the Free Software  Foundation; either version 2 of the
  License, or (at your option) any later version.

  HotSpot Manager is distributed in  the hope that it will be useful,
  but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
  MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
  General Public License for more details.

  You should have  received a copy of the  GNU General Public License
  along  with   NetAccess;  if  not,  write  to   the  Free  Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
*/

function subsBulk_clear()
{
  var msg = T_("Clearing the MSISDN list.");
  if (confirm(msg))
    {
      var form = document.subsBulk;
      form.msisdn_list.value = '';
    }
}

function subsBulk_done()
{
  SendEvent('subsBulk', 'done');
}

function subsBulk_add()
{
  if (subsBulk_data_not_valid())  return;

  var event_args = getEventArgs(document.subsBulk);
  SendEvent('subsBulk', 'add', event_args);
}

function subsBulk_remove()
{
  var msisdn_list = document.subsBulk.msisdn_list.value;
  var msg = T_("You are removing from the customer these subscribers: \n" + msisdn_list);
  if (!confirm(msg))  return;

  SendEvent('subsBulk', 'remove', 'msisdn_list=' + msisdn_list);
}

function subsBulk_update()
{
  if (subsBulk_data_not_valid())  return;
  
  var event_args = getEventArgs(document.subsBulk);
  SendEvent('subsBulk', 'update', event_args);
}

/** returns true or false after validating the data of the form */
function subsBulk_data_not_valid()
{
  var form = document.subsBulk;
  //var customer = form.customer.value;

  /*
  //customer cannot not be empty
  customer = customer.replace(/ +/, '');
  if (customer=='')
    {
      alert(T_("The field Customer cannot be empty."));
      form.customer.focus();
      return true;
    }
  */

  return false;
}
