<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once FORM_PATH.'formWebObj.php';

/**
 * @package customers
 */
class subsBulk extends formWebObj
{
  //output message
  var $output_message = '';

  var $msisdn_list;

  function init()
  {
    $this->addSVar('visible', 'false');
  }

  function on_done($event_args)
  {
    $this->setSVar('visible', 'false');
  }

  /** 
   * Return an array with the variable {{msisdn_list}} 
   * that can be use in WHERE conditions. This variable
   * is like this: 'msisdn1','msisdn2','msisdn3'
   */
  function sql_vars($arr_msisdn)
  {
    $msisdn_list = "'".implode("','", $arr_msisdn)."'";
    return array('msisdn_list' => $msisdn_list);
  }

  function message($msg, $arr_msisdn =array())
  {
    $this->output_message .= $msg . "\n";
    $this->output_message .= "  * " . implode("\n  * ", $arr_msisdn) . "\n";
  }

  function on_add($event_args)
  {
    $msisdn_list = $event_args['msisdn_list'];
    $this->msisdn_list =  $msisdn_list;
    $arr_msisdn = preg_split("/[\s,]+/", trim($msisdn_list));

    $rs_classified = WebApp::openRS('get_classified_list',
				    $this->sql_vars($arr_msisdn));
    $rs_unclassified = WebApp::openRS('get_unclassified_list',
				      $this->sql_vars($arr_msisdn));

    $arr_classified_msisdn = $rs_classified->getColumn('MSISDN');
    $arr_unclassified_msisdn = $rs_unclassified->getColumn('MSISDN');
    $arr_new_msisdn = array_diff($arr_msisdn,
				 $arr_classified_msisdn,
				 $arr_unclassified_msisdn);


    //the msisdn-s that are registered and belong to a customer cannot be added
    if (sizeof($arr_classified_msisdn) > 0)
      {
	$msg = T_("These subscribers cannot be added because they are already registered:");
	$this->message($msg, $arr_classified_msisdn);
      }

    //for the msisdn-s that are registered but do not belong to a customer
    //just set the customer to this one
    if (sizeof($arr_unclassified_msisdn) > 0)
      {
	$rs = WebApp::openRS('get_customer_payment_r');
	$payment_r = $rs->Field('PaymentResponsible');
	$payment_r = ($payment_r=='true' ? 'false' : 'true');
	$vars = $this->sql_vars($arr_unclassified_msisdn);
	$vars['PaymentResponsible'] = $payment_r;
	WebApp::execDBCmd('assign_to_customer', $vars);
	$msg = T_("These subscribers are moved to the current customer:");
	$this->message($msg, $arr_unclassified_msisdn);
      }

    if (sizeof($arr_new_msisdn) != 0)
      {
	$msg = T_("These subscribers are unknown:");
	$this->message($msg, $arr_new_msisdn);
      }
    /*
    //create new subscribers for the msisdn-s that do not exist
    if (sizeof($arr_new_msisdn) > 0)
      {
	$record = $event_args;
	unset($record['msisdn_list']);
	unset($record['subs_id']);
	$record['customer_id'] = WebApp::getSVar('customerEdit->customer_id');
	foreach ($arr_new_msisdn as $msisdn)
	  {
	    $record['MSISDN'] = $msisdn;
	    $this->insert_record($record, 'em_subscribers');
	  }
	$msg = T_("These subscribers are added:");
	$this->message($msg, $arr_new_msisdn);
      }
    */
  }

  function on_remove($event_args)
  {
    $msisdn_list = $event_args['msisdn_list'];
    $this->msisdn_list =  $msisdn_list;
    $arr_msisdn = preg_split("/[\s,]+/", trim($msisdn_list));
    $rs = WebApp::openRS('check_msisdn_list', $this->sql_vars($arr_msisdn));
    $arr_existing_msisdn = $rs->getColumn('MSISDN');

    if (sizeof($arr_existing_msisdn) > 0)
      {
	WebApp::execDBCmd('remove_subs_list',
			  $this->sql_vars($arr_existing_msisdn));
	$this->message(T_("The subscribers that are removed:"),
		       $arr_existing_msisdn);
      }

    $arr_missing_msisdn = array_diff($arr_msisdn, $arr_existing_msisdn);
    if (sizeof($arr_missing_msisdn) > 0)
      {
	$this->message(T_("These subscribers cannot be removed because they do not belong to this customer:"), $arr_missing_msisdn);
      }
  }

  function on_update($event_args)
  {
    WebApp::message(T_("Update is disabled."));
    return;

    $msisdn_list = $event_args['msisdn_list'];
    $this->msisdn_list =  $msisdn_list;
    $arr_msisdn = preg_split("/[\s,]+/", trim($msisdn_list));
    $rs = WebApp::openRS('check_msisdn_list', $this->sql_vars($arr_msisdn));
    $arr_existing_msisdn = $rs->getColumn('MSISDN');

    if (sizeof($arr_existing_msisdn) > 0)
      {
	$record = $event_args;
	unset($record['msisdn_list']);
	$arr_set = array();
	foreach ($record as $field=>$value)
	  {
	    if (trim($value)!='') $arr_set[] = "$field = '$value'";
	  }
	$set_fields = implode(', ', $arr_set);
	foreach ($arr_existing_msisdn as $MSISDN)
	  {
	    if ($set_fields=='')  continue;
	    WebApp::execDBCmd('update_subs', compact('set_fields', 'MSISDN'));
	  }
	$this->message(T_("These subscribers are updated:"),
		       $arr_existing_msisdn);
      }

    $arr_missing_msisdn = array_diff($arr_msisdn, $arr_existing_msisdn);
    if (sizeof($arr_missing_msisdn)!=0)
      {
	$this->message(T_("\nThese subscribers cannot be updated because they do not belong to this customer:"), $arr_missing_msisdn);
      }
  }

  function onRender()
  {
    $visible = $this->getSVar('visible');
    if ($visible=='false')  return;

    //add the values of the first subscriber in the list
    //as default values
    $rs = WebApp::openRS('subsList_rs');
    if ($rs->EOF())
      $rec = $this->pad_record(array(), 'em_subscribers');
    else
      $rec = $rs->Fields();
    WebApp::addVars($rec);

    //add the variable {{msisdn_list}}
    if (trim($this->msisdn_list)=='')
      {
	$arr_msisdn = $rs->getColumn('MSISDN');
	$this->msisdn_list = implode("\n", $arr_msisdn);
      }
    WebApp::addVar('msisdn_list', $this->msisdn_list);

    //display the output message
    if (trim($this->output_message) == '')
      {
	WebApp::addVar('show_message', 'false');
	WebApp::addVar('message', '');
      }
    else
      {
	WebApp::addVar('show_message', 'true');
	$this->output_message = ( "<strong>" . T_("Message") . ":</strong>\n\n"
			   . $this->output_message );
	WebApp::addVar('message', $this->output_message);
      }
  }
}
?>