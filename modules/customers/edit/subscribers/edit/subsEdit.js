// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function save()
{
  var form = document.subsEdit;
  if (!validate(form))  return;
  var event_args = getEventArgs(form);
  SendEvent("subsEdit", "save", event_args);
}

function cancel()
{
  SendEvent("subsEdit", "cancel");
}

function validate(form)
{
  var msisdn = form.MSISDN.value;

  if (msisdn=="")
    {
      alert(T_("The field MSISDN cannot be empty."));
      form.MSISDN.focus();
      return false;
    }

  if (msisdn.length!=12)
    {
      alert(T_("The length of MSISDN is not ok."));
      form.MSISDN.focus();
      return false;
    }

  if (! msisdn.match(/^35567/))
    {
      alert(T_("The MSISDN should start with '35567'."));      
      form.MSISDN.focus();
      return false;
    }

  return true;
}
