<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    customers
 * @subpackage subscribers
 */
include_once FORM_PATH.'formWebObj.php';

class subsEdit extends formWebObj
{
  /** associated array that contains some default values of the form */
  var $fields = array();

  function init()
    {
      $this->addSVar('mode', 'hidden');  // add | edit | hidden
      $this->addSVar('subs_id', UNDEFINED);
    }

  function on_save($event_args)
    {
      $record = $event_args;
      $record['customer_id'] = WebApp::getSVar('customerEdit->customer_id');
      $postpaid = $record['postpaid'];
      $record['postpaid'] = ($postpaid=='true' ? '1' : '0');

      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
	  $this->insert_subscriber($record);
	}
      else if ($mode=='edit')
        {
	  $this->update_subscriber($record);
	}
    }

  function insert_subscriber($record)
  {
    //check that the MSISDN is not already registered
    $MSISDN = $record['MSISDN'];
    $subs = $this->check_msisdn($MSISDN, 'silent');

    if ($subs!=false and $subs['customer_id']!='0')
      {
	$msg = T_("The MSISDN 'var_MSISDN' is already registered.");
	$msg = str_replace('var_MSISDN', $MSISDN, $msg);
	WebApp::message($msg);
        $this->fields = $record;
	return;
      }

    if ($subs!=false)
      {
	//subscriber exists but is unclassified to a customer
	//update it
	$record = array_merge($subs, $record);
	$this->update_record($record, 'em_subscribers', 'subs_id');
      }
    else
      {
	//subscriber does not exist
	//add a new subscriber
	$this->insert_record($record, 'em_subscribers');
      }

    //set 'subs_rs->recount' to 'true'
    //so that the records are counted again
    WebApp::setSVar('subs_rs->recount', 'true');    

    //switch the editing mode to hidden
    $this->setSVar('mode', 'hidden');
    $this->setSVar('subs_id', UNDEFINED);
  }

  function update_subscriber($record)
  {
    $rs = WebApp::openRS('get_subscriber');

    //if msisdn is modified, check that the new msisdn does not exist
    $msisdn = $rs->Field('MSISDN');
    $new_msisdn = $record['MSISDN'];
    if ($new_msisdn != $msisdn)
      {
	if ($this->check_msisdn($new_msisdn))
	  {
	    $this->fields = $record;
	    return;
	  }
      }

    //update the subscriber data
    $subs_id = $this->getSVar('subs_id');
    $record['subs_id'] = $subs_id;
    $this->update_record($record, 'em_subscribers', 'subs_id');

    //switch the editing mode to hidden
    $this->setSVar('mode', 'hidden');
    $this->setSVar('subs_id', UNDEFINED);
  }

  /**
   * Check whether the given MSISDN exists or not.
   * Returns false if does not exist, or the data of the subscriber
   * otherwise.
   * If the given MSISDN exists and the parameter $silent
   * is not given (or is false), then a notification message
   * is displayed as well.
   */
  function check_msisdn($MSISDN, $silent =false)
  {
    $rs = WebApp::openRS('check_msisdn', compact('MSISDN'));

    if ($rs->EOF())  return false;

    if (!$silent)
      {
	$msg = T_("MSISDN 'var_MSISDN' is already registered.");
	$msg = str_replace('var_MSISDN', $MSISDN, $msg);
	WebApp::message($msg);
      }

    return $rs->Fields();
  }

  function on_cancel($event_args)
    {
      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
      $this->setSVar('subs_id', UNDEFINED);
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='edit')
        {
          $rs = WebApp::openRS('get_subscriber');
          $subs_data = $rs->Fields();
        }
      else
        {
	  //add some default values
	  $rs = WebApp::openRS('subsList_rs');
	  if ($rs->EOF())
	    {
	      $subs_data = $this->pad_record(array(), 'em_subscribers');
	    }
	  else
	    {
	      $subs_data = $rs->Fields();
	      unset($subs_data['subs_id']);
	      unset($subs_data['SubsId']);
	      unset($subs_data['AccountID']);
	      $subs_data['MSISDN'] = '';
	    }
        }

      //merge $subs_data with $this->fields
      $subs_data = array_merge($subs_data, $this->fields);

      //add variables for the field values of the form
      $postpaid = $subs_data['postpaid'];
      $subs_data['postpaid'] = ($postpaid=='0' ? '' : 'checked');
      WebApp::addVars($subs_data);
    }
}
?>