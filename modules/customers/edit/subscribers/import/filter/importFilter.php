<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    customers
 * @subpackage import
 */
class importFilter extends WebObject
{
  function init()
    {
      $this->addSVars(array(
                            'customer'    => '',
                            'cc_lastname' => '',
                            'name'        => '',
                            'date'        => '',
                            'msisdn'      => '',
                            'address'     => '',
                            'cos'         => '',
                            'postpaid'    => '',
                            'diff'        => 'checked'
                            ));
    }

  function init_vars()
  {
    $rs = WebApp::openRS('get_customer_name');
    $customer = $rs->Field('customer');
    $this->setSVar('customer', '');
    $this->setSVar('cc_lastname', $customer);
    $this->setSVar('name', '');
    $this->setSVar('date', '');
    $this->setSVar('msisdn', '');
    $this->setSVar('address', '');
    $this->setSVar('cos', '');
    $this->setSVar('postpaid', '');
    $this->setSVar('diff', 'checked');
  }

  function onParse()
    {
      global $event;
      if ($event->target=='customerList' 
	  and ($event->name=='select' or $event->name=='next'))
	{
	  $this->init_vars();
	}
    }

  function get_filter()
    {
      //get the state vars as local variables
      $vars = $this->getSVars();
      array_walk($vars, 'trim');
      extract($vars);

      //get the filter conditions
      $conditions = array();

      if ($customer!='')  
	$conditions[] = "C.customer LIKE '%$customer%'";

      if ($cc_lastname!='')  
	$conditions[] = "CC_LastName LIKE '%$cc_lastname%'";

      if ($name!='')  
	$conditions[] = "(FirstName+' '+LastName) LIKE '%$name%'";

      if ($date!='')  
	$conditions[] = "RegistrationDate $date";

      if ($msisdn!='')
	$conditions[] = "MSISDN LIKE '%$msisdn%'";

      if ($address!='')
	$conditions[] = "(Address1+Address2+City) LIKE '%$address%'";

      if ($cos!='')
	$conditions[] = "COSID = '$cos'";

      if ($postpaid=='checked')
	$conditions[] = "postpaid = '1'";

      if ($diff=='checked')
	{
	  $rs = WebApp::openRS('subsList_all');
	  if (!$rs->EOF())
	    {
	      $arr_msisdn = $rs->getColumn('MSISDN');
	      $msisdn_list = "'" . implode("','", $arr_msisdn) . "'";
	      $conditions[] = "MSISDN NOT IN ($msisdn_list)";
	    }
	}

      $filter = implode(' AND ', $conditions);
      if ($filter=='')  $filter = '1=1';

      return $filter;
    }

  //this filter is used when the connection is made to the Magnolia database
  function get_filter_1()
    {
      //get the state vars as local variables
      $vars = $this->getSVars();
      array_walk($vars, 'trim');
      extract($vars);

      //get the filter conditions
      $conditions = array();

      if ($cc_lastname!='')  
	$conditions[] = "CC_LastName LIKE '%$cc_lastname%'";

      if ($name!='')  
	$conditions[] = "(FirstName+' '+LastName) LIKE '%$name%'";

      if ($date!='')  
	$conditions[] = "[9062_CreationDate] $date";

      if ($msisdn!='')
	$conditions[] = "[9053_SubsMSISDN] LIKE '%$msisdn%'";

      if ($address!='')
	$conditions[] = "concat(Address1,Address2,City) LIKE '%$address%'";

      if ($cos!='')
	$conditions[] = "C.[9180_COSId] = '$cos'";

      if ($diff=='checked')
	{
	  $rs = WebApp::openRS('subsList_all');
	  if (!$rs->EOF())
	    {
	      $arr_msisdn = $rs->getColumn('MSISDN');
	      $msisdn_list = "'" . implode("','", $arr_msisdn) . "'";
	      $conditions[] = "[9053_SubsMSISDN] NOT IN ($msisdn_list)";
	    }
	}

      $filter = implode(' AND ', $conditions);
      if ($filter=='')  $filter = '1=1';

      return $filter;
    }
}
?>