<?php
  /*
   This file  is part of NetAccess.   NetAccess is a  web application for
   managing/administrating the  network connections of the  clients of an
   ISP.

   Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   NetAccess is free  software; you can redistribute it  and/or modify it
   under the terms of the GNU  General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.

   NetAccess  is distributed  in the  hope that  it will  be  useful, but
   WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
   MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
   General Public License for more details.

   You  should have received  a copy  of the  GNU General  Public License
   along with NetAccess;  if not, write to the  Free Software Foundation,
   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
  */


  /**
   * @package    customers
   * @subpackage subscribers
   */

class importList extends formWebObj
{
  //output message
  var $output_message = '';

  function init()
  {
    $this->addSVar('visible', 'false');
    $this->addSVar('filter', '(1=1)');
    $this->addSVar('order_by', 'MSISDN ASC');
  }

  function on_reorder($event_args)
  {
    $order_field = $event_args['order_field'];
    $order_by = $this->getSVar('order_by');
    list($field, $direction) = split(' ', $order_by, 2);

    if ($field==$order_field)
      {
	//change the direction
	$direction = ($direction=='ASC' ? 'DESC' : 'ASC');
      }
    else
      {
	//change the field
	$field = $order_field;
      }

    //set the new order_by variable
    $this->setSVar('order_by', "$field $direction");
  }

  /** 
   * Return an array with the variable {{msisdn_list}} 
   * that can be use in WHERE conditions. This variable
   * is like this: 'msisdn1','msisdn2','msisdn3'
   */
  function sql_vars($arr_msisdn)
  {
    $msisdn_list = "'".implode("','", $arr_msisdn)."'";
    return array('msisdn_list' => $msisdn_list);
  }

  function message($msg, $arr_msisdn =array())
  {
    $this->output_message .= $msg . "\n";
    $this->output_message .= "  * " . implode("\n  * ", $arr_msisdn) . "\n";
  }

  function get_filter()
  {
    $filter = WebApp::getObject('importFilter');
    $this->setSVar('filter', $filter->get_filter());
  }

  function on_next($event_args)
  {
    $page = $event_args['page'];
    WebApp::setSVar('importList_rs->current_page', $page);
  }

  function on_finish($event_args)
  {
    $this->setSVar('visible', 'false');
  }

  function on_export($event_args)
  {
    $this->get_filter();
    $rs = WebApp::openRS('importList_all');
    $rs->toExcelFile('subscriber_list');
  }

  function on_import($event_args)
  {
    //get the msisdn-s of the subscribers in the list
    $this->get_filter();
    $rs = WebApp::openRS('importList_rs');
    $arr_msisdn = $rs->getColumn('MSISDN');

    //find the subscribers of the list that are already registered
    //to another customer, those are registered but are unclassified
    //and those that are not registred yet in the application
 
    $rs_classified = WebApp::openRS('get_classified_list',
				    $this->sql_vars($arr_msisdn));
    $rs_unclassified = WebApp::openRS('get_unclassified_list',
				      $this->sql_vars($arr_msisdn));

    $arr_classified_msisdn = $rs_classified->getColumn('MSISDN');
    $arr_unclassified_msisdn = $rs_unclassified->getColumn('MSISDN');
    $arr_new_msisdn = array_diff($arr_msisdn,
				 $arr_classified_msisdn,
				 $arr_unclassified_msisdn);

    //the msisdn-s that are registered and belong to a customer cannot be imported
    if (sizeof($arr_classified_msisdn) > 0)
      {
	$msg = T_("These subscribers cannot be imported because they are already assigned to a customer:");
	$this->message($msg, $arr_classified_msisdn);
      }

    //for the msisdn-s that are registered but do not belong to a customer
    //just set the customer to this one
    if (sizeof($arr_unclassified_msisdn) > 0)
      {
	$rs = WebApp::openRS('get_customer_payment_r');
	$payment_r = $rs->Field('PaymentResponsible');
	$payment_r = ($payment_r=='true' ? 'false' : 'true');
	$vars = $this->sql_vars($arr_unclassified_msisdn);
	$vars['PaymentResponsible'] = $payment_r;
	WebApp::execDBCmd('assign_to_customer', $vars);
	$msg = T_("These subscribers are assigned to the current customer:");
	$this->message($msg, $arr_unclassified_msisdn);
      }

    if (sizeof($arr_new_msisdn) != 0)
      {
	$msg = T_("These subscribers are unknown:");
	$this->message($msg, $arr_new_msisdn);
      }
  }

  function onRender()
  {
    WebApp::setSVar('importList_rs->recount', 'true');
    $this->get_filter();
    $rs = WebApp::openRS('importList_rs');
    while (!$rs->EOF())
      {
	$postpaid = $rs->Field('postpaid');
	$postpaid = ($postpaid=='0' ? '' : 'checked');
	$rs->setFld('postpaid', $postpaid);
	$rs->MoveNext();
      }

    //display the output message
    if (trim($this->output_message) == '')
      {
	WebApp::addVar('show_message', 'false');
	WebApp::addVar('message', '');
      }
    else
      {
	WebApp::addVar('show_message', 'true');
	$this->output_message = ( "<strong>" . T_("Message") . ":</strong>\n\n"
			   . $this->output_message );
	WebApp::addVar('message', $this->output_message);
      }
  }
}
?>