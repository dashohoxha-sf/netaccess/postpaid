<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    customers
 */
class customer_tabs extends WebObject
{
  function init()
  {
    WebApp::setSVar('tabs2::customer->selected_item', 'subscribers');
  }

  function onParse()
    {
      //make sure that if mode==add only the tab 'info' can be selected
      $mode = WebApp::getSVar('customerEdit->mode');
      if ($mode=='add') 
	WebApp::setSVar('tabs2::customer->selected_item', 'info');

      //select which customer tab to display
      $item = WebApp::getSVar('tabs2::customer->selected_item');
      switch ($item)
        {
        case 'info':
          $tab = 'info/info.html';
          break;
        case 'preferencies':
          $tab = 'preferencies/preferencies.html';
          break;
        default:
        case 'subscribers':
          $tab = 'subscribers/subsList.html';
          break;
        case 'invoices':
          $tab = 'invoices/invoiceList.html';
          break;
        case 'expenses':
          $tab = 'expenses/expenses.html';
          break;
        case 'calls':
          $tab = 'calls/calls.html';
          break;
        case 'reports':
          $tab = 'reports/reports.html';
          break;
        }
      WebApp::addVar('tab', $tab);
    }
}
?>