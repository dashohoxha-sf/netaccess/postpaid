<?php
  /*
   This file  is part of NetAccess.   NetAccess is a  web application for
   managing/administrating the  network connections of the  clients of an
   ISP.

   Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   NetAccess is free  software; you can redistribute it  and/or modify it
   under the terms of the GNU  General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.

   NetAccess  is distributed  in the  hope that  it will  be  useful, but
   WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
   MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
   General Public License for more details.

   You  should have received  a copy  of the  GNU General  Public License
   along with NetAccess;  if not, write to the  Free Software Foundation,
   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
  */


  /**
   * @package    admin
   * @subpackage discountList
   */

class discountList extends WebObject
{
  function init()
  {
    $this->addSVar('month', date('Y-m'));
    $this->addSVar('editable', 'true');  // ('true'|'false')
  }

  function on_del($event_args)
  {
    $discount_id = $event_args['discount_id'];

    //get the record to be deleted before deleteing it
    $rs = WebApp::openRS('get_discount', compact('discount_id'));

    //delete the discount record
    WebApp::execDBCmd('del_discount', compact('discount_id'));

    //add a log record
    $this->add_log('del_discount', $rs->Fields());
  }

  function on_add($event_args)
  {
    $event_args['customer_id'] = WebApp::getSVar('customerEdit->customer_id');
    WebApp::execDBCmd('add_discount', $event_args);

    //add a log record
    $this->add_log('add_discount', $event_args);
  }

  function add_log($action, $params)
  {
    extract($params);

    //get customer_id and month
    $customer_id = WebApp::getSVar('customerEdit->customer_id');
    $month = WebApp::getSVar('discountList->month');

    $username = UNDEFINED;
    $time = UNDEFINED;
    $comment = UNDEFINED;

    if ($action=='add_discount')
      {
	$field = "+discount($month)";
	$old_value = '';
	$new_value = "$discount_value $discount_name";
      }
      else  //($action=='del_discount')
      {
	$field = "-discount($month)";
	$old_value = "$discount_value $discount_name";
	$new_value = '';
      }

    //add a log record
    log_event("~customer($customer_id)", $username, $time, 
              $comment, $field, $old_value, $new_value);
  }

  function onParse()
  {
    $month = $this->getSVar('month');
    $editable = ($month==date('Y-m') ? 'true' : 'false'); 
    $this->setSVar('editable', $editable);
  }

  function onRender()
  {
    //add the recordset of the month listbox
    $Y = date('Y');  $m = date('m');
    for ($i=0; $i < 5; $i++) 
      {
	$month = date('Y-m', mktime(0, 0, 0, $m-$i, 1, $Y));
	$arr_months[] = $month;
      }
    WebApp::add_listbox_rs('rs_months', $arr_months);

    //add the recordset of the discount names
    $arr_discount_names = 
      array(
	    'Zbritje dhe Rimbursim',
	    T_("Bonus")
	    );
    WebApp::add_listbox_rs('rs_discount_names', $arr_discount_names);

    //format the discount values with 2 decimal digits
    $rs = WebApp::openRS('discountList_rs');
    while (!$rs->EOF())
      {
	$value = $rs->Field('discount_value');
	$rs->setFld('discount_value', number_format($value, 2));
	$rs->MoveNext();
      }
  }
}
?>