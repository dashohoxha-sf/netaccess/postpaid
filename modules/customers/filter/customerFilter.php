<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package customers
   */
class customerFilter extends WebObject
{
  function init()
  {
    $this->addSVars( array(
                           'customer'      => '',
                           'nipt'          => '',
                           'address'       => '',
                           'contact'       => '',
			   'not_payment_r' => ''
                           ) );
    $this->addSVar('filter_condition', '(1=1)');
  }

  function onParse()
  {
    $this->build_filter_condition();
  }

  function build_filter_condition()
  {
    //get state vars
    extract($this->getSVars());

    $arr_filters = array();

    if ($customer != '')
      $arr_filters[] = '(customer LIKE "%'.$customer.'%")';

    if ($nipt != '')
      $arr_filters[] = '(NIPT LIKE "%'.$nipt.'%")';

    if ($address != '')
      $arr_filters[] = '(Address LIKE "%'.$address.'%")';

    if ($contact != '')
      $arr_filters[] = ( '(contact_person LIKE "%'.$contact.'%"'
			 . ' OR contact_phone LIKE "%'.$contact.'%"'
			 . ' OR contact_email LIKE "%'.$contact.'%")' );

    if ($not_payment_r != '')
      $arr_filters[] = '(PaymentResponsible = "false")';

    $filter = implode(' AND ', $arr_filters);

    if ($filter=='')  $filter = '1=1';
    $filter = '('.$filter.')';

    $this->setSVar('filter_condition', $filter);
  }  
}
?>