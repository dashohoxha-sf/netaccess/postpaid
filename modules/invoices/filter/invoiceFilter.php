<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package invoices
   */
class invoiceFilter extends WebObject
{
  //condition that is used for making checks
  var $check_condition = '';

  function init()
  {
    $last_month = WebApp::getSVar('last_month');
    $this->addSVars(array(
			  'month'   => $last_month,
			  'type'    => 'customer',
			  'payable' => 'checked'
			  ));
    $this->addSVars($this->get_initial_vars());
  }

  function get_initial_vars()
  {
    $arr_vars = array(
		      'amount'      => '',
		      'serial_nr'   => '',
		      'address'     => '',
		      'customer'    => '',
		      'NIPT'        => '',
		      'MSISDN'      => '',
		      'name'        => '',
		      'cos'         => '',
		      'passport_nr' => '',
		      'check'       => 'none'
		      );
    return $arr_vars;
  }

  function on_change_type($event_args)
  {
    $type = $event_args['type'];
    $this->setSVar('type', $type);
    WebApp::setSVar('invoiceList->type', $type);

    if ($type=='customer')
      $order_by = 'customer_name ASC';
    else
      $order_by = 'MSISDN ASC';
    WebApp::setSVar('invoiceList->order_by', $order_by);
  }

  function onParse()
    {
      WebApp::setSVar('invoiceList->filter', $this->get_filter());
    }

  function get_filter()
    {
      //get the state vars as local variables
      $vars = $this->getSVars();
      array_walk($vars, 'trim');
      extract($vars);

      //get the filter conditions
      $conditions = array();

      $conditions[] = "(month = '$month')";

      if ($payable=='checked')
	    $conditions[] = "payable = 'true'";

      if ($amount!='')
	$conditions[] = "TotalCharge $amount";

      if ($serial_nr!='')
	$conditions[] = "SerialNumber LIKE '%$serial_nr%'";
      
      if ($type=='customer')
	{
	  if ($address!='')
	    $conditions[] = "Address LIKE '%$address%'";

	  if ($customer!='')
	    $conditions[] = "customer_name LIKE '%$customer%'";

	  if ($NIPT!='')
	    $conditions[] = "NIPT like '%$NIPT%'";
	}
      else  // ($type=='subscriber')
	{
	  if ($address!='')
	    $conditions[] = "(Address1+Address2+City) LIKE '%$address%'";

	  if ($customer!='')
	    {
	      $params = compact('name', 'customer');
	      $rs = WebApp::openRS('get_customer_list', $params);
	      $arr_inv_id = $rs->getColumn('InvoiceID');
	      $inv_id_list = implode(',', $arr_inv_id);
	      $conditions[] = "CustomerInvoiceID IN ($inv_id_list)";
	    }

	  if ($MSISDN!='')
	    $conditions[] = "MSISDN LIKE '%$MSISDN%'";

	  if ($name!='')  
	    $conditions[] = "(FirstName+' '+LastName) LIKE '%$name%'";

	  if ($cos!='')
	    $conditions[] = "COSID='$cos'";

	  if ($passport_nr!='')
	    $conditions[] = "PassportNr LIKE '%$passport_nr%'";
	}

      //add also the check condition
      $check_condition = $this->get_check_condition($check);
      if ($check_condition!='')
	$conditions[] = $check_condition;

      $filter = implode(' AND ', $conditions);
      if ($filter=='')  $filter = '1=1';

      return $filter;
    }

  function get_check_condition($check)
  {
    switch ($check)
      {
      case 'none':
	$condition = '';
	break;
      case 'amount':
	$condition = 'TotalCharge < 10';
	break;
      case 'payable':
	$condition = ("((payable!='true' AND payable!='false') "
		      . "OR payable IS NULL)");
	break;
      case 'inv_date':
        $month = $this->getSVar('month');
        list($Y, $m) = split('-', $month, 2);
	$invoice_date = date('Y-m-d', mktime(0, 0, 0, $m+1, 1, $Y));
	$condition = "InvoiceDate != '$invoice_date'";
	break;
      case 'inv_period':
        $month = $this->getSVar('month');
        list($Y, $m) = split('-', $month, 2);
	$from_date = date('Y-m-d', mktime(0, 0, 0, $m, 1, $Y));
	$to_date = date('Y-m-d', mktime(0, 0, 0, $m+1, 1, $Y));
	$condition = "(FromDate < '$from_date' OR ToDate > '$to_date' OR DATEDIFF(day, FromDate, ToDate) < 3)";
	break;
      case 'msisdn':
	$condition = ("( len(MSISDN)!=12 OR (MSISDN NOT LIKE '35567%') "
		      . " OR (MSISDN IS NULL) )");
	break;
      case 'multiple_inv':
	$rs = WebApp::openRS('get_subs_with_multiple_invoices');
	$subs_arr = $rs->getColumn('SubsId');
	$subs_list = implode(',', $subs_arr);
	$condition = "SubsId IN ($subs_list)";
	break;
      }

    return $condition;
  }

  function onRender()
  {
    //get the type of the invoices
    $type = $this->getSVar('type');

    //add the recordset of the month listbox
    $last_month = WebApp::getSVar('last_month');
    list($Y, $m) = split('-', $last_month, 2);
    for ($i=0; $i < 6; $i++) 
      {
	$month = date('Y-m', mktime(0, 0, 0, $m-$i, 1, $Y));
	$arr_months[] = $month;
      }
    WebApp::add_listbox_rs('rs_months', $arr_months);

    //add the recordset for the invoice type
    $arr_types = array(
		       'customer'   => T_("Customer"),
		       'subscriber' => T_("Subscriber")
		       );
    WebApp::add_listbox_rs('rs_types', $arr_types);

    //add the recordset for the checks
    $arr_checks['none'] = T_("No Checks");
    $arr_checks['amount']  = T_("Amount not OK");
    $arr_checks['payable'] = T_("Payable not OK");
    $arr_checks['inv_date'] = T_("InvoiceDate not OK");
    $arr_checks['inv_period'] = T_("InvoicePeriod not OK");
    if ($type=='customer')
      {
      }
    else  // ($type=='subscriber')
      {
	$arr_checks['msisdn']  = T_("MSISDN not OK");
	$arr_checks['multiple_inv']  = T_("Multiple Invoices");
      }

    WebApp::add_listbox_rs('rs_checks', $arr_checks);    
  }
}
?>