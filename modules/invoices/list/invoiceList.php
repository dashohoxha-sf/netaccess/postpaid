<?php
  /*
   This file  is part of NetAccess.   NetAccess is a  web application for
   managing/administrating the  network connections of the  clients of an
   ISP.

   Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   NetAccess is free  software; you can redistribute it  and/or modify it
   under the terms of the GNU  General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.

   NetAccess  is distributed  in the  hope that  it will  be  useful, but
   WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
   MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
   General Public License for more details.

   You  should have received  a copy  of the  GNU General  Public License
   along with NetAccess;  if not, write to the  Free Software Foundation,
   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
  */


  /**
   * @package    customers
   * @subpackage reports
   */

class invoiceList extends WebObject
{
  function init()
  {
    $this->addSVar('type', 'customer');   // (customer|subscriber)
    $this->addSVar('filter', '(1=1)');
    $this->addSVar('order_by', 'customer_name ASC');
  }

  function on_reorder($event_args)
  {
    $order_field = $event_args['order_field'];
    $order_by = $this->getSVar('order_by');
    list($field, $direction) = split(' ', $order_by, 2);

    if ($field==$order_field)
      {
	//change the direction
	$direction = ($direction=='ASC' ? 'DESC' : 'ASC');
      }
    else
      {
	//change the field
	$field = $order_field;
      }

    //set the new order_by variable
    $this->setSVar('order_by', "$field $direction");
  }

  function on_next($event_args)
  {
    $page = $event_args['page'];
    WebApp::setSVar('invoiceList_rs->current_page', $page);
  }

  function on_export($event_args)
  {
    WebApp::setSVar('invoiceList_rs->recount', 'true');
    $type = $this->getSVar('type');
    $inv_table = ($type=='subscriber' ? 'inv_subscribers' : 'inv_customers');
    $rs = WebApp::openRS('invoiceList_all', compact('inv_table'));
    $rs->toExcelFile('invoice_list');
    exit;
  }

  function on_bulk($event_args)
  {
    WebApp::setSVar('invoiceBulk->visible', 'true');
  }

  function onRender()
  {
    WebApp::setSVar('invoiceList_rs->recount', 'true');
    $type = $this->getSVar('type');
    $inv_table = ($type=='subscriber' ? 'inv_subscribers' : 'inv_customers');
    $rs = WebApp::openRS('invoiceList_rs', compact('inv_table'));
    while (!$rs->EOF())
      {
	extract($rs->Fields());
	//list($year,$mon) = split('-', $month, 2);

	$InvoiceDate = substr($InvoiceDate, 0,10);
	$rs->setFld('InvoiceDate', $InvoiceDate);
	$FromDate = substr($FromDate, 0,10);
	$rs->setFld('FromDate', $FromDate);
	$ToDate = substr($ToDate, 0,10);
	$rs->setFld('ToDate', $ToDate);
	$TotalCharge = number_format($TotalCharge, 2);
	$rs->setFld('TotalCharge', $TotalCharge);

	if ($type=='customer')
	  {
	    $rs->setFld('customer_name', substr($customer_name, 0, 30));
	  }
	else // ($type=='subscriber')
	  {
	  }

	$rs->MoveNext();
      }
  }
}
?>