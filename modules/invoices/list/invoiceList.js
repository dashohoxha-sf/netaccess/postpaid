// -*-C-*- //tell emacs to use C mode
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function invoiceList_order_by(order_field)
{
  SendEvent('invoiceList', 'reorder', 'order_field='+order_field);
}

function view_cust_invoice(InvoiceID, month)
{
  var win_features = 'width=850,height=900,resizeable=1,scrollbars=1,menubar=1';
  window.open('', 'view_cust_invoice', win_features);

  var target = 'reports/invoices/invoice.html';
  var event_args = 'module=customer;InvoiceID='+InvoiceID+';month='+month;
  wGoTo('view_cust_invoice', target+'?event=invoice.view('+event_args+')');
}

function view_cust_envelope(customer_id, month)
{
  var target = 'reports/envelopes/envelope.html';
  var event_args = 'module=customer;id='+customer_id+';month='+month;

  var win_features = 'width=850,height=900,resizeable=1,scrollbars=1,menubar=1';
  window.open('', 'view_cust_envelope', win_features);

  wGoTo('view_cust_envelope', target+'?event=envelope.view('+event_args+')');
}

function view_subs_invoice(InvoiceID, month)
{
  var win_features = 'width=850,height=900,resizeable=1,scrollbars=1,menubar=1';
  window.open('', 'view_subs_invoice', win_features);

  var target = 'reports/invoices/invoice.html';
  var event_args = 'module=subscriber;InvoiceID='+InvoiceID+';month='+month;
  wGoTo('view_subs_invoice', target+'?event=invoice.view('+event_args+')');
}

function view_subs_envelope(subs_id, month)
{
  var target = 'reports/envelopes/envelope.html';
  var event_args = 'module=subscriber;id='+subs_id+';month='+month;

  var win_features = 'width=850,height=900,resizeable=1,scrollbars=1,menubar=1';
  window.open('', 'view_subs_envelope', win_features);

  wGoTo('view_subs_envelope', target+'?event=envelope.view('+event_args+')');
}

function view_calldetails(InvoiceID, month)
{
  var target = 'reports/calldetails/calldetails.html';
  var event_args = 'InvoiceID='+InvoiceID+';month='+month;

  var win_features = 'width=850,height=900,resizeable=1,scrollbars=1,menubar=1';
  window.open('', 'view_calldetails', win_features);

  wGoTo('view_calldetails', target+'?event=calldetails.view('+event_args+')');
}

function export_invoices()
{
  SendEvent('invoiceList', 'export');
}
