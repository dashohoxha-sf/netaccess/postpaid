<?php
/**
 * Create the html files of invoices, expenses and envelopes.
 */
class GenerateInvoices
{
  var $month = '2008-11';

  /** 'customer' or 'subscriber' */
  var $type;

  /** if true, then subscribers of each customer are processed */
  var $recursive;

  /** if true, then invoices will be generated */
  var $invoices;

  /** if true, then call details will be generated */
  var $calldetails;

  /** if true, than the envelopes will be generated */
  var $envelopes;

  /** if true, then the envelope labels will be generated */
  var $labels;

  /** the output format of the invoices, calldetails, etc. */
  var $output_format = 'html';   // text|html

  /** the templates of the invoices, call details, envelopes, labels, etc. */
  var $tpl_invoice     = 'modules/reports/invoices/invoice.html';
  var $tpl_calldetails = 'modules/reports/calldetails/calldetails.html';
  var $tpl_envelope    = 'modules/reports/envelopes/envelope.html';
  var $tpl_labels      = 'modules/reports/labels/labels.html';

  /** the number of labels in one label page */
  var $label_page_size = 12;

  /** The output directory of the html files. */
  var $output_dir;

  /** The current subdirectory where the html files are output. */
  var $subdir = 'A001';

  /** The number of files that are created in the current directory. */
  var $file_cnt = 0;

  /** If a subdirectory contains more than this many files, 
   *  we should create a new one. */
  var $max_file_nr = 1000;

  /**
   * Keep the text data of a customer and its subscribers 
   * invoices and call details.
   */
  var $customer_text_data = '';

  function GenerateInvoices($month, $type, $recursive, $invoices,
			    $calldetails, $envelopes, $labels, $format ='html')
  {
    $this->output_format = $format;

    if ($format=='html')
    {
      $this->tpl_invoice     = 'modules/reports/invoices/invoice.html';
      $this->tpl_calldetails = 'modules/reports/calldetails/calldetails.html';
      $this->tpl_envelope    = 'modules/reports/envelopes/envelope.html';
      $this->tpl_labels      = 'modules/reports/labels/labels.html';
    }
    else //if ($format=='text')
    {
      $this->tpl_invoice     = 'modules/reports/invoices/invoice_text.html';
      $this->tpl_calldetails = 'modules/reports/calldetails/calldetails_text.html';
    }

    $this->month     = $month;
    $this->type      = $type;
    $this->recursive = $recursive;
    $this->invoices     = $invoices;
    $this->calldetails  = $calldetails;
    $this->envelopes    = $envelopes;
    $this->labels       = $labels;


    $this->output_dir = 'k:/html/invoices/';
    $this->subdir = 'A001';
    $this->max_file_nr = 1000;
    $this->file_cnt = 0;
  }

  function generate_invoices($rs)
  {
    $dir = $this->output_dir . $this->subdir;
    shell("rm -rf $dir");
    shell("mkdir -p $dir");

    if ($this->type=='customer')
      {
	$this->generate_customer_invoices($rs);
      }
    else  // ($this->type=='subscriber')
      {
	$this->generate_subscriber_invoices($rs, 'chunk');
      }
  }
  
  function write_file($fname, &$fcontent, $customer_name =UNDEFINED)
  {
    //get the filename
    if ($customer_name==UNDEFINED)
    {
      $file_name = $this->output_dir.$this->subdir.'/'.$fname;
    }
    else
    {
      $customer_name = str_replace('/', '-', $customer_name);
      $dir = $this->output_dir.$this->subdir.'/'.$customer_name.'/';
      shell("mkdir -p '$dir'");
      $file_name = $this->output_dir.$this->subdir.'/'.$customer_name.'/'.$fname;
    }

    if (file_exists($file_name))
    {
      shell("mv $file_name ${file_name}.1");
    }

    //open the file and write the content
    $fp = fopen($file_name, 'w');
    fputs($fp, $fcontent);
    fclose($fp);

    //increment the file counter
    $this->file_cnt++;
  }

  function change_dir()
  {
    $this->subdir++;
    $dir = $this->output_dir.$this->subdir;
    shell("rm -rf $dir");
    shell("mkdir -p $dir");

    $this->file_cnt = 0;
  }

  function generate_customer_invoices($rs)
  {
    $format = $this->output_format;    // text, html, etc.
    print "<h2>Generating customer invoices </h2> \n";
    print "<ol> \n";

    $customer_cnt = 0;
    while (!$rs->EOF())
      {
	//get the data of the invoice as variables
	$invoice_data = $rs->Fields();
	extract($invoice_data);

	$write_customer = "write_customer_$format";
	$this->$write_customer($invoice_data);

	if ($this->recursive)
	  {
	    //create the invoices of each subscriber of this customer
	    $query = "SELECT * FROM inv_subscribers 
                      WHERE customer_id = '$customer_id'
                      AND month = '$this->month'
                      ORDER BY MSISDN, subs_id";
	    $subs_rs = WebApp::sqlQuery($query);
	    $this->generate_subscriber_invoices($subs_rs);
	  }

	//write also an end-page
	$write_endcustomer = "write_endcustomer_$format";
	$this->$write_endcustomer($invoice_data);

	//chunk output into directories of no more than 500 customers
	  /*
	if (++$customer_cnt >= 500) 
	{
	  $this->change_dir();
	  $customer_cnt = 0;
	}
	  */

	//get the next record
	$rs->MoveNext();
      }

    print "</ol> \n\n";
  }

  function generate_subscriber_invoices($rs, $chunk =false)
  {
    $format = $this->output_format;    // text, html, etc.

    print "<h4>Generating subscriber invoices</h4>\n";
    print "<ol> \n";

    while (!$rs->EOF())
      {
	//get the data of the invoice as variables
	$invoice_data = $rs->Fields();
	extract($invoice_data);

        $write_subscriber = "write_subscriber_$format";
	$this->$write_subscriber($invoice_data);

	//if there are too many files, change the output subdirectory
	if ($chunk)
	  {
	    if (($this->file_cnt > $this->max_file_nr)) $this->change_dir();
	  }

	//get the next record
	$rs->MoveNext();
      }

    print "</ol> \n <br/> \n";
  }

  /*-------------------------- html output ----------------------------*/

  function write_customer_html($customer_invoice)
  {
    extract($customer_invoice);

    //output a log item
    print "  <li> ($InvoiceID, $customer_id) $customer_name";  flush();

    //generate the html page of the invoice
    if ($this->invoices)
    {
      global $event;
      $event->target = 'invoice';
      $event->name = 'view';
      $event->args['month'] = $this->month;
      $event->args['module'] = 'customer';
      $event->args['InvoiceID'] = $InvoiceID;
      $html_page = WebApp::getHtmlPage($this->tpl_invoice);

      //write the html invoice to a file
      $ext = ($payable=='true' ? 'invoice' : 'expenses');
      $fname = "${InvoiceID}_000000000000_${InvoiceID}.${ext}.html";
      $this->write_file($fname, $html_page, $customer_name);

      //output the filename on the log
      print "\n   <br/><strong>$fname</strong>";  flush();
    }


    //handle envelopes and labels
    $this->write_envelope_html($customer_invoice, 'customer');
    $this->write_labels_html($customer_invoice, 'customer');

    //close the log itrm
    print "</li>\n";
  }

  function write_endcustomer_html($customer_invoice)
  {
    if (!$this->invoices)  return;

    extract($customer_invoice);

    $end_page = "<h1>-----</h1><h1>END of $customer_name</h1><h1>-----</h1>";
    $ext = ($payable=='true' ? 'invoice' : 'expenses');
    $fname = "${InvoiceID}_zzzzzzzzzzzzzzzzzzzzzz_end.${ext}.html";
    $this->write_file($fname, $end_page, $customer_name);
  }

  function write_subscriber_html($subscriber_invoice)
  {
    extract($subscriber_invoice);

    //get customer_name
    $query = ("SELECT customer FROM em_customers "
	      . " WHERE customer_id = '$customer_id'");
    $rs = WebApp::sqlQuery($query);
    $customer_name = $rs->Field('customer');
    $subscriber_invoice['customer_name'] = $customer_name;

    //open a log item
    print "  <li> ($InvoiceID, $subs_id) $MSISDN $customer_name $FirstName $LastName ";
    flush();

    //generate the html page of the invoice
    if ($this->invoices)
    {
      global $event;
      $event->target = 'invoice';
      $event->name = 'view';
      $event->args['month'] = $this->month;
      $event->args['module'] = 'subscriber';
      $event->args['InvoiceID'] = $InvoiceID;
      $html_page = WebApp::getHtmlPage($this->tpl_invoice);

      //write the html invoice to a file
      if (trim($CustomerInvoiceID)=='')  $CustomerInvoiceID = 'S';
      $ext = ($payable=='true' ? '1.invoice' : '1.expenses');
      $fname = "${CustomerInvoiceID}_${MSISDN}_${InvoiceID}.${ext}.html";
      $this->write_file($fname, $html_page, $customer_name);

      //output the filename to the log
      print "\n  <br/> <strong>$fname</strong>";  flush();
    }

    //handle calldetails, envelopes and labels
    $this->write_calldetails_html($subscriber_invoice);
    if ($payable=='true')
    {
      $this->write_envelope_html($subscriber_invoice, 'subscriber');
      $this->write_labels_html($subscriber_invoice, 'subscriber');
    }

    //close the log item
    print "  </li>\n";   flush();
  }

  function write_calldetails_html($subscriber_invoice)
  {
    if (!$this->calldetails)  return;

    extract($subscriber_invoice);

    global $event;
    $event->target = 'calldetails';
    $event->name = 'view';
    $event->args['month'] = $this->month;
    $event->args['InvoiceID'] = $InvoiceID;
    $html_page = WebApp::getHtmlPage($this->tpl_calldetails);

    //write the html invoice to a file
    if (trim($CustomerInvoiceID)=='')  $CustomerInvoiceID = 'S';
    $ext = '2.calldetails';
    $fname = "${CustomerInvoiceID}_${MSISDN}_${InvoiceID}.${ext}.html";
    $this->write_file($fname, $html_page, $customer_name);

    //output the filename to the log
    print "\n  <br/> <strong>$fname</strong>";  flush();
  }

  function write_envelope_html($invoice_data, $inv_type)
  {
    if (!$this->envelopes)  return;

    extract($invoice_data);

    //there is an envelope only for each customer invoice
    //and for each payable subscriber invoice
    if ($inv_type=='subscriber' and $payable=='false')  return;

    //generate the html page of the invoice
    global $event;
    $event->target = 'envelope';
    $event->name = 'view';
    $event->args['month'] = $this->month;
    $event->args['module'] = $inv_type;  // 'customer' | 'subscriber'
    $event->args['id'] = ($inv_type=='customer' ? $customer_id : $subs_id);
    $html_page = WebApp::getHtmlPage($this->tpl_envelope);

    //write the html invoice to a file
    $ext = 'envelope.C4';
    if ($inv_type=='customer')
    {
      $fname = "${InvoiceID}_000000000000_${InvoiceID}.$ext.html";
    }
    else
    {
      if (trim($CustomerInvoiceID)=='')  $CustomerInvoiceID = 'S';
      $fname = "${CustomerInvoiceID}_${MSISDN}_${InvoiceID}.${ext}.html";
    }
    $this->write_file($fname, $html_page, $customer_name);

    //output a log line
    print "\n  <br/> <strong>$fname</strong>";   flush();
  }

  function write_labels_html($invoice_data, $inv_type)
  {
    if (!$this->labels)  return;

    static $arr_addresses = array();
    static $file_cnt = 0;

    extract($invoice_data);

    //there is an envelope only for each customer invoice
    //and for each payable subscriber invoice
    if ($inv_type=='subscriber' and $payable=='false')  return;

    //get the address of the current customer/subscriber
    if ($inv_type=='customer')
    {
      $address = "$customer_name<br />\n$Address";
    }
    else
    {
      $address = "$FirstName $LastName<br />\n$Address1 $Address2<br />\n$City";
    }

    $arr_addresses[] = $address;
    if (sizeof($arr_addresses)==$this->label_page_size)
    {
      //put the labels into a recordset with two columns
      global $rs_labels;
      $rs_labels = new EditableRS('labels');
      for ($i=0; $i < sizeof($arr_addresses); $i+=2)
      {
	$label1 = $arr_addresses[$i];
	$label2 = $arr_addresses[$i+1];
	$rs_labels->addRec(compact('label1', 'label2'));
      }

      //set the propper values to the $event object
      global $event;
      $event->target = 'labels';
      $event->name = 'view';
      $event->args['page_size'] = $this->label_page_size;
      $event->args['file_cnt'] = ++$file_cnt;

      //generate the HTML page of the labels
      $html_page = WebApp::getHtmlPage($this->tpl_labels);

      //write the html page to a file
      $fname = "labels_${file_cnt}.html";
      $this->write_file($fname, $html_page, $customer_name);

      //output a log line
      print "\n  <br/> <strong>$customer_name/$fname</strong>";
      print $rs_labels->toHtmlTable('no-headers');  //debug
      flush();

      //empty the address page
      $arr_addresses = array();
    }
  }

  /*-------------------------- text output ----------------------------*/

  function write_customer_text($customer_invoice)
  {
    extract($customer_invoice);

    //generate the text page of the invoice
    global $event;
    $event->target = 'invoice_text';
    $event->name = 'view';
    $event->args['month'] = $this->month;
    $event->args['module'] = 'customer';
    $event->args['InvoiceID'] = $InvoiceID;
    $event->args['payable'] = $payable;
    $text_invoice = WebApp::fill_template($this->tpl_invoice);

    //save the data of the invoice
    $this->customer_text_data = $text_invoice;

    //output a log line
    print ("  <li> ($InvoiceID, $customer_id) $customer_name <br/>");
    flush();
  }

  function write_endcustomer_text($customer_invoice)
  {
    //write the customer_data to a file
    $customer_name = $customer_invoice['customer_name'];
    $fname = $customer_name.'.txt';
    $this->write_file($fname, $this->customer_text_data);

    //output the filename to the log
    print "\n  <strong>$fname</strong> <br/><br/></li>";  flush();
  }

  function write_subscriber_text($subscriber_invoice)
  {
    extract($subscriber_invoice);

    //print a log item
    print "  <li> ($InvoiceID, $subs_id) $MSISDN $customer_name $FirstName $LastName </li>\n";
    flush();

    //generate the html page of the invoice
    global $event;
    $event->target = 'invoice_text';
    $event->name = 'view';
    $event->args['month'] = $this->month;
    $event->args['module'] = 'subscriber';
    $event->args['InvoiceID'] = $InvoiceID;
    $event->args['payable'] = $payable;
    $text_invoice = WebApp::fill_template($this->tpl_invoice);

    //save the data of the invoice
    $this->customer_text_data .= $text_invoice;

    //handle calldetails
    $this->write_calldetails_text($subscriber_invoice);
  }

  function write_calldetails_text($subscriber_invoice)
  {
    extract($subscriber_invoice);

    global $event;
    $event->target = 'calldetails_text';
    $event->name = 'view';
    $event->args['month'] = $this->month;
    $event->args['InvoiceID'] = $InvoiceID;
    $text_calldetails = WebApp::fill_template($this->tpl_calldetails);

    //save the data of the invoice
    $this->customer_text_data .= $text_calldetails;
  }
}
?>