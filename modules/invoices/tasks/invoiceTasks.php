<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package invoices
   */
class invoiceTasks extends WebObject
{
  function init()
  {
    $this->addSVars(array(
			  'recursive'   => 'checked',
			  'invoices'    => 'checked',
			  'calldetails' => '',
			  'envelopes'   => '',
			  'labels'      => 'checked'
			  ));
  }

  function on_generate_invoices($event_args)
  {
    $format = $event_args['format'];
    $month = WebApp::getSVar('invoiceFilter->month');
    $type = WebApp::getSVar('invoiceFilter->type');

    extract($this->getSVars());
    $rec = ($recursive=='checked'? true : false);
    $inv = ($invoices=='checked'? true : false);
    $calls = ($calldetails=='checked'? true : false);
    $env = ($envelopes=='checked'? true : false);
    $lbl = ($labels=='checked'? true : false);

    if ($format=='text')
    {
      $rec = true;
      $inv = true;
      $calls = true;
      $env = false;
      $lbl = false;
    }

    //open the html document and print a title
    $title = T_("Generating Invoices");
    $css_file = 'css/print_inv.css';
    print "<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'
          'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' lang='EN'>
<head>
  <title>$title</title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  <link  type='text/css' rel='stylesheet' href='$css_file' />
</head>

<body>
";
    flush();

    //create the object that generates the invoices
    include dirname(__FILE__).'/class.GenerateInvoices.php';
    $rinv = new GenerateInvoices($month, $type, $rec, $inv, $calls, $env, $lbl, $format);
    //$rinv = new GenerateInvoices($month, 'customer', true, false, true, false, false, 'text');

    //generation may take a long time
    set_time_limit(0);

    //get the recordset of the selected invoices
    $inv_table = ($type=='subscriber' ? 'inv_subscribers' : 'inv_customers');
    $rs = WebApp::openRS('invoice_list', compact('inv_table'));

    //$query = "SELECT * FROM inv_customers WHERE payable='true' and month='2008-11' order by customer_name";
    //$rs = WebApp::sqlQuery($query);

    //start the generation
    $rinv->generate_invoices($rs);

    //close the html document
    print "\n</body></html>";

    //stop proccessing
    exit;
  }
}
?>