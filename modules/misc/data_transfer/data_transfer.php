<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package misc
   */
class data_transfer extends WebObject
{
  function init()
  {
    $dsn1 = "mssql://sa:eagle@10.234.252.121/Invoice08?new_link=true";
    $dsn2 = "mysqli://root@127.0.0.1/invoice6_dbo?new_link=true";
    $this->addSVars(array(
			  'src_DSN'   => $dsn1,
			  'src_table' => '',
			  'dst_DSN'   => $dsn2,
			  'dst_table' => '',
			  ));
  }

  function on_transfer($event_args)
  {
    extract($this->getSVars());
    //WebApp::message($src_table);  //debug
    $cnn1 = new Connection($src_DSN);
    $cnn2 = new Connection($dst_DSN);
    $cnn1->mdb2->loadModule('Manager');
    $cnn1->mdb2->loadModule('Reverse', null, true);
    //$table_info = $cnn1->mdb2->tableInfo($src_table, MDB2_TABLEINFO_ORDERTABLE);
    //$table_info = $cnn1->mdb2->tableInfo($src_table);
    $table_fields = $cnn1->mdb2->listTableFields($src_table);
    print '<xmp>';  print_r($table_fields); print '</xmp>';
    for ($i=0; $i < sizeof($table_fields); $i++)
      {
	$field = $table_fields[$i];
	$definition = $cnn1->mdb2->getTableFieldDefinition($src_table, $field);
	print '<xmp>';  print_r($definition); print '</xmp>';
      }
  }
}
?>