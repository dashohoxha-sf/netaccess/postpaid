<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

class update extends WebObject
{
  function update()
  {
    global $MDB2_OPTIONS;
    $mdb2_dsn = "mssql://cc:eagle_cc@10.1.21.201:1433/postpaid_scratch?new_link=true";
    $this->setConnection($mdb2_dsn, $MDB2_OPTIONS);
  }

  function init()
  {
    $this->addSVar('status', 'get');  // (get|update)
  }

  function on_update_system_tables($event_args)
  {
    set_time_limit(0); //don't interrupt until it is all done
    WebApp::sqlExec('set ansi_nulls ON;');
    WebApp::sqlExec('set ansi_warnings ON;');
    WebApp::sqlExec('exec emsp_get_system_data;');
    WebApp::message(T_("System tables update finished."));
  }

  function on_get_latest_subscribers($event_args)
  {
    WebApp::message(T_("Please try it later. Right now application is in maintenance. Thank you."));
      return;
	/*
    set_time_limit(0); //don't interrupt until it is all done
    WebApp::sqlExec('set ansi_nulls ON;');
    WebApp::sqlExec('set ansi_warnings ON;');
    WebApp::sqlExec('exec emsp_get_latest_subscribers;');

    $this->setSVar('status', 'update');
	*/
  }

  function on_update_subscribers($event_args)
  {
    WebApp::message(T_("Please try it later. Right now application is in maintenance. Thank you."));
      return;
    set_time_limit(0); //don't interrupt until it is all done
    WebApp::sqlExec('set ansi_nulls ON;');
    WebApp::sqlExec('set ansi_warnings ON;');
    WebApp::sqlQuery('exec emsp_update_subscribers;');
    $this->setSVar('status', 'get');
  }

  function on_cancel_update($event_args)
  {
    WebApp::sqlQuery('exec emsp_update_subscribers_clean;');
    $this->setSVar('status', 'get');
  }

  function onRender()
  {
    $status = $this->getSVar('status');
    if ($status=='update')
      {
	$rs = WebApp::openRS('update_interval');
	$fromTime = $rs->Field('fromTime');
	$toTime = $rs->Field('toTime');
	WebApp::addVars(compact('fromTime', 'toTime'));

	$rs = WebApp::openRS('subscriber_nr');
	$subs_nr = $rs->Field('nr');
	$rs = WebApp::openRS('pps_x_nr');
	$pps_x_nr = $rs->Field('nr');
	$rs = WebApp::openRS('crm_x_nr');
	$crm_x_nr = $rs->Field('nr');
	$rs = WebApp::openRS('em_x_nr');
	$em_x_nr = $rs->Field('nr');
	$rs = WebApp::openRS('sdr_nr');
	$sdr_nr = $rs->Field('nr');
	WebApp::addVars(compact('subs_nr','pps_x_nr','crm_x_nr','em_x_nr', 'sdr_nr'));
      }
  }
}
?>
