<?php
class envelope extends WebObject
{
  function init()
  {
    $this->addSVar('module', 'customer');  // customer|subscriber
    $this->addSVar('id', '1');
    $this->addSVar('month', '2008-09');
  }

  function on_view($event_args)
  {
    $this->setSVar('module', $event_args['module']);
    $this->setSVar('id', $event_args['id']);
    $this->setSVar('month', $event_args['month']);
  }

  function onParse()
  {
    $module = $this->getSVar('module');
    $rs = WebApp::openRS("get_${module}_invoice");

    if ($module=='subscriber')
      {
	$InvoiceID = $rs->Field('InvoiceID');
	$CustomerInvoiceID = $rs->Field('CustomerInvoiceID');
	if (trim($CustomerInvoiceID)=='')  $CustomerInvoiceID = 'S';
	$MSISDN = $rs->Field('MSISDN');
	$payable = $rs->Field('payable');
	$ext = 'envelope.C4';
	$title = "${CustomerInvoiceID}_${MSISDN}_${InvoiceID}.${ext}";
      }
    else  //($module=='customer')
      {
	$InvoiceID = $rs->Field('InvoiceID');
	$MSISDN = '000000000000';
	$payable = $rs->Field('payable');
	$ext = 'envelope.C4';
	$title = "${InvoiceID}_${MSISDN}_${InvoiceID}.${ext}";
      }

    WebApp::addGlobalVar('envelope_title', $title);
  }

  function onRender()
  {
    //get from DB the name, address, etc. and add them as variables
    $this->add_address_vars();
  }

  /** Get from DB the name, address, etc. and add them as variables. */
  function add_address_vars()
  {
    $module = $this->getSVar('module');
    $rs = WebApp::openRS("get_${module}_invoice");
    WebApp::addVars($rs->Fields());
  }
}
?>