<?php
include dirname(__FILE__).'/calldetails.php';
class calldetails_text extends calldetails
{
  function onRender()
  {
    parent::onRender();

    mb_internal_encoding('UTF-8');
    //mb_detect_order('iso-8859-2,iso-8859-1,utf-8,ascii');

    $this->format_header();
    $this->format_rows();
  }

  //convert the given string to correct encoding
  function enc($string)
  {
    return mb_convert_encoding($string, 'ISO-8859-1', 'UTF-8');
  }

  function format_header()
  {
    $InvoiceID = $this->getSVar('InvoiceID');
    $MSISDN = WebApp::getVar('MSISDN');
    $SubsId = WebApp::getVar('SubsId');
    $FromDate = WebApp::getVar('FromDate');
    $ToDate = WebApp::getVar('ToDate');

    $cdr_header = 'HD'; 
    $cdr_header .= sprintf("%'010.10s", $InvoiceID);
    $cdr_header .= ' ';
    $cdr_header .= sprintf('%15.15s', $MSISDN);

    WebApp::addVar('CDR_HEADER', $cdr_header);
  }

  function format_rows()
  {
    $InvoiceID = $this->getSVar('InvoiceID');
    $rs = WebApp::openRS('calldetails_rs');
    $rs->addCol('CDR_ROW', '');
    while (!$rs->EOF())
    {
      extract($rs->Fields());

      $CallClassType = $this->enc($CallClassType);

      $cdr = 'D' . sprintf("%'010.10s", $InvoiceID);
      $format = '%12.12s'.'%10.10s'.'%15.15s'.'%15.15s'.'%10.10s'.'%10.10s';
      $cdr .= sprintf($format, $Date, $Time, $Destination, $CallClassType, $ChargeableDuration, $ChargeableAmount);

      $rs->setFld('CDR_ROW', $cdr);
      $rs->MoveNext();
    }
  }
}
?>