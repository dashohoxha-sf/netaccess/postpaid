<?php
class calldetails extends WebObject
{
  function calldetails()
  {
    /*
    global $MDB2_OPTIONS;
    $mdb2_dsn = "mssql://magnolia:hcfxku2111@10.234.252.63/Invoice?new_link=true";
    $this->setConnection($mdb2_dsn, $MDB2_OPTIONS);
    */
  }

  function init()
  {
    $this->addSVar('InvoiceID', UNDEFINED);
    $this->addSVar('month', UNDEFINED);
  }

  function on_view($event_args)
  {
    $this->setSVar('InvoiceID', $event_args['InvoiceID']);
    $this->setSVar('month', $event_args['month']);
  }

  function onParse()
  {
    $InvoiceID = $this->getSVar('InvoiceID');

    $rs = WebApp::openRS("get_subs_invoice");

    $CustomerInvoiceID = $rs->Field('CustomerInvoiceID');
    if (trim($CustomerInvoiceID)=='')  $CustomerInvoiceID = 'S';
    $msisdn = $rs->Field('MSISDN');
    $payable = $rs->Field('payable');
    $ext = '2.calldetails';
    $title = "${CustomerInvoiceID}_${msisdn}_${InvoiceID}.${ext}";

    WebApp::addGlobalVar('calldetails_title', $title);
  }

  function onRender()
  {
    $rs = WebApp::openRS("get_subs_invoice");
    $MSISDN = $rs->Field('MSISDN');
    $SubsId = $rs->Field('SubsId');
    $FromDate = $rs->Field('FromDate');
    $ToDate = $rs->Field('ToDate');

    WebApp::addVars(compact('MSISDN','SubsId','FromDate','ToDate'));
  }
}
?>