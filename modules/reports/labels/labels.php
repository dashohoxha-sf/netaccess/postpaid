<?php
class labels extends WebObject
{
  function init()
  {
    $this->addSVar('page_size', '12');  //nr of labels in one page
    $this->addSVar('file_cnt', '0');    //count of the label file
  }

  function on_view($event_args)
  {
    $this->setSVar('page_size', $event_args['page_size']);
    $this->setSVar('file_cnt', $event_args['file_cnt']);
  }

  function onParse()
  {
    $page_size = $this->getSVar('page_size');
    $file_cnt = $this->getSVar('file_cnt');

    WebApp::addGlobalVar('title', 'labels_'.$file_cnt);
    WebApp::addGlobalVar('size', $page_size);
  }  
  
  function onRender()
  {
    global $rs_labels, $webPage;

    $page_size = $this->getSVar('page_size');
    if ($page_size==12)
    {
      $rs_labels->ID = 'labels';
      $webPage->addRecordset($rs_labels);
    }
    else
    {
      $rs_labels->MoveFirst();
      $first_label1 = $rs_labels->Field('label1');
      $first_label2 = $rs_labels->Field('label2');

      $rs_labels->MoveLast();
      $last_label1 = $rs_labels->Field('label1');
      $last_label2 = $rs_labels->Field('label2');
      WebApp::addVars(compact('first_label1', 'first_label2', 'last_label1', 'last_label2'));

      $rs = $rs_labels->slice(1, $rs->count - 1);
      $rs->ID = 'labels';
      $webPage->addRecordset($rs);
    }
  }
}
?>