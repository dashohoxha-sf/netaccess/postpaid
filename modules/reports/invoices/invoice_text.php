<?php
include dirname(__FILE__).'/invoice.php';
class invoice_text extends invoice
{
  function init()
  {
    parent::init();
    $this->addSVar('payable', 'false');
  }

  function on_view($event_args)
  {
    parent::on_view($event_args);
    $this->setSVar('payable', $event_args['payable']);
  }

  function onParse()
  {
  }

  function onRender()
  {
    parent::onRender();

    mb_internal_encoding('ISO-8859-1');
    //mb_detect_order('iso-8859-2,iso-8859-1,utf-8,ascii');

    $this->format_header_vars();
    $this->format_invoice_details();
    $this->format_totals();
  }

  //convert the given string to correct encoding
  function enc($string)
  {
    return mb_convert_encoding($string, 'ISO-8859-1', 'UTF-8');
  }

  function format_header_vars()
  {
    $type = $this->getSVar('type');
    $payable = $this->getSVar('payable');
    $InvoiceID = $this->getSVar('InvoiceID');
    //print "type='$type' payable='$payable' InvoiceID='$InvoiceID' <br/>\n";

    $header = 'H';
    $header .= ($type=='customer' ? 'C' : 'S');
    $header .= ($payable=='true' ? 'I' : 'E');
    $header .= sprintf("%'010.10s", $InvoiceID);
    $header .= ' ';

    $header .= ( $payable=='true' ? $this->get_invoice_data()
		 : $this->get_expense_data() );
    $header .= ( $type=='customer' ? $this->get_customer_info()
		 :$this->get_subscriber_info() );

    WebApp::addVar('INV_HEADER', $header);
  }

  function get_invoice_data()
  {
    $serial_number = WebApp::getVar('SerialNumber');
    $invoice_date = WebApp::getVar('InvoiceDate');
    $from_date = WebApp::getVar('FromDate');
    $to_date = WebApp::getVar('ToDate');
    $invoice_period = "$from_date - $to_date";
    $last_payment_date = WebApp::getVar('last_payment_date');
    $total_charge = WebApp::getVar('SUM_TOTAL');

    $format = '%20.20s'.'%12.12s'.'%25.25s'.'%12.12s'.'%15.15s';
    $invoice_data = sprintf($format, 
			    $serial_number, $invoice_date, $invoice_period, 
			    $last_payment_date, $total_charge);
    return $invoice_data;
  }

  function get_expense_data()
  {
    $calculation_date = WebApp::getVar('InvoiceDate');
    $from_date = WebApp::getVar('FromDate');
    $to_date = WebApp::getVar('ToDate');
    $calculation_period = "$from_date - $to_date";
    $total_charge = WebApp::getVar('SUM_TOTAL');

    $expense_data = sprintf('%12.12s'.'%25.25s'.'%15.15s', 
			    $calculation_date, $calculation_period, 
			    $total_charge);
    return $expense_data;
  }

  function get_customer_info()
  {
    $customer_name = WebApp::getVar('customer_name');
    $customer_name = $this->enc($customer_name);

    $nipt = WebApp::getVar('NIPT');
    $nipt = $this->enc($nipt);

    $address = WebApp::getVar('Address');
    $address = trim($address);
    $address = preg_replace("/\r?\n/", '\\n', $address);
    $address = $this->enc($address);

    $customer_info = sprintf('%-50.50s'.'%-12.12s'.'%-200.200s',
			     $customer_name, $nipt, $address);
    return $customer_info;
  }

  function get_subscriber_info()
  {
    $msisdn = WebApp::getVar('MSISDN');

    $cos_name = WebApp::getVar('cos_name');
    $cos_name = $this->enc($cos_name);

    $first_name = WebApp::getVar('FirstName');
    $last_name = WebApp::getVar('LastName');
    $name = trim($first_name . ' ' . $last_name);
    $name = $this->enc($name);

    $passport_nr = WebApp::getVar('PassportNr');
    $passport_nr = $this->enc($passport_nr);

    $account_id = WebApp::getVar('AccountID');

    $address1 = WebApp::getVar('Address1');
    $address2 = WebApp::getVar('Address2');
    $address = trim($address1 . ' ' . $address2);
    $address = $this->enc($address);

    $city = WebApp::getVar('City');
    $city = $this->enc($city);

    $format = '%15.15s'.'%-50.50s'.'%-60.60s'.'%-20.20s'.'%-20.20s'.'%-100.100s'.'%-20.20s';
    $subscriber_info = sprintf($format, $msisdn, $cos_name, $name, $passport_nr, $account_id, $address, $city);

    return $subscriber_info;
  }

  function format_invoice_details()
  {
    //$type = $this->getSVar('type');
    $InvoiceID = $this->getSVar('InvoiceID');
    $rs = WebApp::openRS('invoice_details');
    $rs->addCol('INV_SUMMARY', '');
    while (!$rs->EOF())
    {
      extract($rs->Fields());

      $DetailName = $this->enc($DetailName);
      $Unit = $this->enc($Unit);

      $inv_summary = 'B' . sprintf("%'010.10s", $InvoiceID) . ' '; 
      $format = '%-50.50s'.'%15.15s'.'%10.10s'.'%15.15s'.'%15.15s'.'%15.15s';
      $inv_summary .= sprintf($format, $DetailName, $Usage, $Unit, $NetValue, $VAT, $TotalValue);
      $rs->setFld('INV_SUMMARY', $inv_summary);

      $rs->MoveNext();
    }
  }

  function format_totals()
  {
    $InvoiceID = $this->getSVar('InvoiceID');

    $sum_net = WebApp::getVar('SUM_NET');
    $sum_vat = WebApp::getVar('SUM_VAT');
    $sum_total = WebApp::getVar('SUM_TOTAL');

    $inv_totals = 'T' . sprintf("%'010.10s", $InvoiceID) . ' ';
    $format = '%20.20s'.'%20.20s'.'%20.20s';
    $inv_totals .= sprintf($format, $sum_net, $sum_vat, $sum_total);

    WebApp::addVar('INV_TOTALS', $inv_totals);
  }
}
?>