<?php
class invoice extends WebObject
{
  function init()
  {
    $this->addSVar('type', 'customer');  // customer|subscriber
    $this->addSVar('InvoiceID', '1');
    $this->addSVar('month', '2008-09');  // month of the invoice
    $this->addSVar('payable', 'false');  // false|true
  }

  function on_view($event_args)
  {
    $this->setSVar('type', $event_args['module']);
    $this->setSVar('InvoiceID', $event_args['InvoiceID']);
    $this->setSVar('month', $event_args['month']);
  }

  function onParse()
  { 
    $type = $this->getSVar('type');
    $InvoiceID = $this->getSVar('InvoiceID');
    $month = $this->getSVar('month');

    //add the variable {{payable}}
    $rs = WebApp::openRS("get_${type}_invoice", compact('InvoiceID', 'month'));
    $payable = $rs->Field('payable');
    $payable = ($payable=='true' ? 'true' : 'false');
    $this->setSVar('payable', $payable);

    //add the title of the invoice
    WebApp::addGlobalVar('invoice_title', $this->get_invoice_title());
  }

  function get_invoice_title()
  {
    $type = $this->getSVar('type');
    $InvoiceID = $this->getSVar('InvoiceID');
    $month = $this->getSVar('month');
    $payable = $this->getSVar('payable');

    $rs = WebApp::openRS("get_${type}_invoice", compact('InvoiceID', 'month'));
    if ($type=='subscriber')
      {
	$customer_id = $rs->Field('customer_id');
	$InvoiceID = $rs->Field('InvoiceID');
	$CustomerInvoiceID = $rs->Field('CustomerInvoiceID');
	if (trim($CustomerInvoiceID)=='')  $CustomerInvoiceID = 'S';
	$MSISDN = $rs->Field('MSISDN');
	$payable = $rs->Field('payable');
	$ext = ($payable=='true' ? '1.invoice' : '1.expenses');
	$title = "${CustomerInvoiceID}_${MSISDN}_${InvoiceID}.${ext}";
      }
    else  // ($type=='customer')
      {
	$customer_id = $rs->Field('customer_id');
	$InvoiceID = $rs->Field('InvoiceID');
	$MSISDN = '000000000000';
	$payable = $rs->Field('payable');
	$ext = ($payable=='true' ? 'invoice' : 'expenses');
	$title = "${InvoiceID}_${MSISDN}_${InvoiceID}.${ext}";
      }

    return $title;
  }

  function onRender()
  {
    //get from DB the values that fill the header 
    //of the template and add them as variables
    $this->add_header_vars();

    //get from DB the values that fill the details
    //of the template and add them as variables
    $this->add_detail_vars();
  }

  /**
   * Get from DB the values that fill the header 
   * of the template and add them as variables.
   */
  function add_header_vars()
  {
    $type = $this->getSVar('type');
    $rs = WebApp::openRS("get_${type}_invoice");
    //WebApp::debug_msg($rs->toHtmlTable());  //debug
    $vars = $rs->Fields();
    $inv_date = $vars['InvoiceDate'];
    $vars['InvoiceDate'] = date('d/m/Y', strtotime($inv_date));
    $vars['FromDate'] = date('d/m/Y', strtotime($vars['FromDate']));
    $vars['ToDate'] = date('d/m/Y', strtotime($vars['ToDate']));
    $vars['TotalCharge'] = number_format($vars['TotalCharge'], 2, '.', ',');
    $vars['InvoiceID'] = $this->getSVar('InvoiceID');
    $vars['payable'] = $this->getSVar('payable');
    WebApp::addVars($vars);

    //add the variable {{last_payment_date}}
    $last_payment_date = date('d/m/Y', strtotime($inv_date) + 22*24*3600);
    WebApp::addVar('last_payment_date', $last_payment_date);
  }

  /**
   * Get from DB the values that fill the details
   * of the template and add them as variables.
   */
  function add_detail_vars()
  {
    //create an array for keeping the data that will be displayed
    //in the report, and initialize it with some default values
    $arr_details = array();

    $arr_details[0]['DetailName'] = 'Thirrjet në Grup';
    $arr_details[1]['DetailName'] = 'Eagle - Eagle';
    $arr_details[2]['DetailName'] = 'Eagle - GSM të tjera';
    $arr_details[3]['DetailName'] = 'Eagle - Albtelecom';
    $arr_details[4]['DetailName'] = 'Ndërkombëtare';
    $arr_details[5]['DetailName'] = 'Roaming';
    $arr_details[6]['DetailName'] = 'SMS në Grup';
    $arr_details[7]['DetailName'] = 'Eagle SMS';
    $arr_details[8]['DetailName'] = 'SMS të tjera';
    $arr_details[9]['DetailName'] = 'SMS Roaming';
    $arr_details[10]['DetailName'] = 'GPRS';
    $arr_details[11]['DetailName'] = 'GPRS Roaming';
    //$arr_details[12]['DetailName'] = 'Zbritje dhe Rimbursim';

    for ($i=0; $i < 12; $i++)
      {
	$arr_details[$i]['Usage'] = '-';
	$arr_details[$i]['Unit'] = '';
	$arr_details[$i]['NetValue'] = '0.00';
	$arr_details[$i]['VAT'] = '0.00';
	$arr_details[$i]['TotalValue'] = '0.00';
      }

    foreach (array(0,1,2,3,4,5) as $i)
      {
	$arr_details[$i]['Usage'] = '0:00';
	$arr_details[$i]['Unit'] = 'Min';
      }
    foreach (array(6,7,8,9) as $i)
      {
	$arr_details[$i]['Usage'] = '0';
	$arr_details[$i]['Unit'] = 'Njesi';
      }
    foreach (array(10,11) as $i)
      {
	$arr_details[$i]['Usage'] = '0.0';
	$arr_details[$i]['Unit'] = 'KB';
      }

    //unset($arr_details[9]);
    //unset($arr_details[10]);
    //unset($arr_details[11]);
    unset($arr_details[12]);

    //get the invoice details and put the data into the array
    $rs = WebApp::openRS('get_invoice_details');
    //WebApp::debug_msg($rs->toHtmlTable());  //debug

    $SUM_NET = $SUM_VAT = $SUM_TOTAL = 0;
    while (!$rs->EOF())
      {
	$fields = $rs->Fields();
	$order       = $fields['DetailOrder'];
	$category    = $fields['DetailCategory'];
	$usage       = $fields['Usage'];
	$count       = $fields['UnitCount'];
	$net_value   = $fields['NetValue'];
	$vat         = $fields['VAT'];
	$total_value = $fields['TotalValue'];

	if ($order >= 20)  continue;

	if ($category=='call')
	  {
	    $min = floor($usage / 60);
	    $sec = sprintf('%02d', $usage%60);
	    $arr_details[$order]['Usage'] = "$min:$sec"; 
	    $arr_details[$order]['Unit'] = "Min"; 
	  }
	else if ($category=='sms')
	  {
	    if (trim($usage)=='')  $usage = '0';
	    $arr_details[$order]['Usage'] = $count;
	    $arr_details[$order]['Unit'] = "Njesi";
	  }
	else if ($category=='gprs')
	  {
	    if (trim($usage)=='')  $usage = '0';
	    $arr_details[$order]['Usage'] = $usage;
	    $arr_details[$order]['Unit'] = "KB";
	  }
	else
	  {
	    $arr_details[$order]['Usage'] = "-";
	    $arr_details[$order]['Unit'] = '-';
	  }

	$arr_details[$order]['NetValue']   = number_format($net_value, 2, '.', ',');
	$arr_details[$order]['VAT']        = number_format($vat, 2, '.', ',');
	$arr_details[$order]['TotalValue'] = number_format($total_value, 2, '.', ',');

	$SUM_NET   += $net_value;
	$SUM_VAT   += $vat;
	$SUM_TOTAL += $total_value;

	$rs->MoveNext();
      }


    //get the services and append them to the report
    $rs = WebApp::openRS('get_invoice_services');
    //WebApp::debug_msg($rs->toHtmlTable());  //debug

    $i = 12;
    while (!$rs->EOF())
      {
	$fields = $rs->Fields();

	$type        = $fields['DetailType'];
	$unit_count  = $fields['UnitCount'];
	$detail_name = $fields['DetailName'];
	$net_value   = $fields['NetValue'];
	$vat         = $fields['VAT'];
	$total_value = $fields['TotalValue'];

	$arr_details[$i]['DetailName'] = $detail_name;
	$arr_details[$i]['Usage']      = $unit_count;
	$arr_details[$i]['Unit']      = 'Muaj';
	$arr_details[$i]['NetValue']   = number_format($net_value, 2, '.', ',');
	$arr_details[$i]['VAT']        = number_format($vat, 2, '.', ',');
	$arr_details[$i]['TotalValue'] = number_format($total_value, 2, '.', ',');
	if ($type=='discount') $arr_details[$i]['Unit'] = '';

	$SUM_NET   += $net_value;
	$SUM_VAT   += $vat;
	$SUM_TOTAL += $total_value;
	
	$rs->MoveNext();
	$i++;
      }

    //add the sum variables
    WebApp::addVars(array(
			  'SUM_NET'   => number_format($SUM_NET, 2, '.', ','),
			  'SUM_VAT'   => number_format($SUM_VAT, 2, '.', ','),
			  'SUM_TOTAL' => number_format($SUM_TOTAL, 2, '.', ','),
			  )
		    );

    //convert the array of details to a recordset
    $rs_details = new EditableRS('invoice_details');
    foreach ($arr_details as $i => $detail)  $rs_details->addRec($detail); 

    //add this recordsets to the webPage
    global $webPage;
    $webPage->addRecordset($rs_details);
  }
}
?>